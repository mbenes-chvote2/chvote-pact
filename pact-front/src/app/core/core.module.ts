/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MissingTranslationHandler, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CustomMissingTranslationHandler } from './custom-missing-translation-handler';
import { BaseUrlService } from './base-url-service/base-url.service';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { MaterialModule } from '../material/material.module';
import { WindowRefService } from './window-ref-service/window-ref.service';
import { SnackBarAlertComponent } from './snack-bar-alert/snack-bar-alert.component';
import { LoadingSpinnerService } from './loading-spinner.service';

// AoT requires an exported function for factories
export function TranslateHttpLoaderFactory(httpClient: HttpClient, baseUrlService: BaseUrlService) {
  return new TranslateHttpLoader(httpClient, baseUrlService.frontendBaseUrl + 'assets/i18n/');
}

@NgModule({
  imports: [
    HttpClientModule,
    MaterialModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: TranslateHttpLoaderFactory,
        deps: [HttpClient, BaseUrlService]
      },
      missingTranslationHandler: {provide: MissingTranslationHandler, useClass: CustomMissingTranslationHandler}
    })
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    RouterModule,
    ConfirmationDialogComponent
  ],
  providers: [
    ConfirmationDialogComponent,
    SnackBarAlertComponent,
    WindowRefService,
    BaseUrlService,
    LoadingSpinnerService
  ],
  declarations: [
    ConfirmationDialogComponent,
    SnackBarAlertComponent
  ],
  entryComponents: [ConfirmationDialogComponent, SnackBarAlertComponent]
})
export class CoreModule {
}
