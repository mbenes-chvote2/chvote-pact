/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/


import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { VotingMaterialsInvalidation } from '../model/voting-materials-invalidation';
import { VotingMaterialsInvalidationService } from '../voting-materials-invalidation.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { SnackBarAlertComponent } from '../../core/snack-bar-alert/snack-bar-alert.component';
import { MatSnackBar } from '@angular/material';
import { ActionMetadata } from '../../action-detail/model/action-metadata';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';
import { ActionConverterService } from '../../action-detail/action-converter.service';

@Component({
  templateUrl: './voting-materials-invalidation-detail.component.html',
  styleUrls: ['./voting-materials-invalidation-detail.component.scss']
})
export class VotingMaterialsInvalidationDetailComponent implements OnInit {
  action: VotingMaterialsInvalidation;
  actionMetadata: ActionMetadata;
  votersCreationStatsMetaData: ActionMetadata[];


  constructor(private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private votingMaterialsInvalidationService: VotingMaterialsInvalidationService,
              private loadingSpinnerService: LoadingSpinnerService,
              private actionConverter: ActionConverterService) {
  }


  ngOnInit() {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    const actionObservable: Observable<VotingMaterialsInvalidation> = this.route.paramMap.pipe(switchMap(
      (params: ParamMap) => {
        const id = parseInt(params.get('id'));

        if (this.router.url.startsWith('/pending-actions')) {
          return this.votingMaterialsInvalidationService.getById(id);
        } else {
          return this.votingMaterialsInvalidationService.getByBusinessId(id);
        }
      }));


    actionObservable.subscribe(action => {
      this.action = action;
      this.actionMetadata = this.actionConverter.convertVotingMaterialsInvalidation(action);
      this.votersCreationStatsMetaData = this.action.votersCreationStats.map(stats => (
          {
            "printer": {value: stats.printer},
            "testingCardCount": {
              value: stats.printableTestingCards + stats.nonPrintableTestingCards + stats.printerTestingCards +
                     stats.controllerTestingCards
            },
            "realCardCount": {value: stats.statByCountingCircle.reduce((acc, value) => acc + value.numberOfVoters, 0)},
            "stats": {value: stats}
          }
        )
      );


      this.loadingSpinnerService.broadcastLoadingStatus(false);
    });


  }

  private onSuccess(messageLabel: string): void {
    this.loadingSpinnerService.broadcastLoadingStatus(false);
    this.router.navigate(['/voting-materials-invalidation']);
    this.snackBar.openFromComponent(SnackBarAlertComponent, {
      duration: 2500,
      data: {
        message: 'voting-materials-invalidation.detail.snack-bar.' + messageLabel,
        messageParams: {operationName: this.action.operationName}
      }
    });
  }

  private onError(): void {
    this.loadingSpinnerService.broadcastLoadingStatus(false);
    this.snackBar.openFromComponent(SnackBarAlertComponent, {
      duration: 2500,
      data: {
        message: 'Error while saving.'
      }
    });
  }

  public onCreateAction(): void {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this.votingMaterialsInvalidationService.createAction(this.action.votingMaterialsConfigurationId).subscribe(
      this.onSuccess.bind(this, 'created'),
      this.onError.bind(this));
  }

  public onApproveAction(): void {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this.votingMaterialsInvalidationService.approve(this.action.id).subscribe(
      this.onSuccess.bind(this, 'approved'),
      this.onError.bind(this));
  }

  public onRejectAction(reason: string): void {
    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this.votingMaterialsInvalidationService.reject(this.action.id, reason).subscribe(
      this.onSuccess.bind(this, 'rejected'),
      this.onError.bind(this));
  }
}
