/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VotingMaterialsInvalidationDetailComponent } from './voting-materials-invalidation-detail/voting-materials-invalidation-detail.component';
import { VotingMaterialsInvalidationTableComponent } from './voting-materials-invalidation-table/voting-materials-invalidation-table.component';

const routes: Routes = [
  {path: 'voting-materials-invalidation', component: VotingMaterialsInvalidationTableComponent},
  {path: 'voting-materials-invalidation/:id', component: VotingMaterialsInvalidationDetailComponent},
  {path: 'pending-actions/voting-materials-invalidation/:id', component: VotingMaterialsInvalidationDetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VotingMaterialsInvalidationRoutingModule {
}

