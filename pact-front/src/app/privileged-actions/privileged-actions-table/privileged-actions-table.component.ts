/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/


import {merge as observableMerge,  Observable ,  Subscription ,  BehaviorSubject } from 'rxjs';

import {map} from 'rxjs/operators';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { PrivilegedAction } from '../model/privileged-action';
import { PrivilegedActionsService } from '../privileged-actions.service';
import { DataSource } from '@angular/cdk/collections';
import { AuthenticationService } from '../../authentication/authentication.service';
import { Router } from '@angular/router';



import { MatSort } from '@angular/material';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';

@Component({
  templateUrl: './privileged-actions-table.component.html',
  styleUrls: ['./privileged-actions-table.component.scss']
})
export class PrivilegedActionsTableComponent implements OnInit, OnDestroy, AfterViewInit {

  static readonly FILTER_ALL: number = 0;
  static readonly FILTER_MINE: number = 1;
  static readonly FILTER_OTHER: number = 2;

  @ViewChild(MatSort) sort: MatSort;

  constructor(private router: Router,
              private privilegedActionsService: PrivilegedActionsService,
              private loadingSpinnerService: LoadingSpinnerService,
              private authService: AuthenticationService,
              private cd: ChangeDetectorRef) {
  }

  private _showOwner = true;
  private _currentFilter = PrivilegedActionsTableComponent.FILTER_ALL;

  get showOwner(): boolean {
    return this._showOwner;
  }

  get currentFilter(): number {
    return this._currentFilter;
  }

  private _privilegedActionsDataSource: PrivilegedActionsDataSource;
  private _dataSourceSubscription: Subscription;

  get privilegedActionsDataSource(): PrivilegedActionsDataSource {
    return this._privilegedActionsDataSource;
  }

  private _displayedColumns = ['operationName', 'operationDate', 'statusDate', 'ownerName', 'type', 'action'];

  private _displayedColumnsWithoutOwner = ['operationName', 'operationDate', 'statusDate', 'type', 'action'];

  get displayedColumns(): Array<string> {
    return this.showOwner ? this._displayedColumns : this._displayedColumnsWithoutOwner;
  }

  ngOnInit(): void {
    let hasInitialized = false;
    this._privilegedActionsDataSource = new PrivilegedActionsDataSource(this.authService, this.sort);

    this.loadingSpinnerService.broadcastLoadingStatus(true);
    this._dataSourceSubscription = this.privilegedActionsService.pendingActions
      .subscribe(pendingActions => {
        const applyFilter = !hasInitialized && pendingActions.slice().filter(action =>
          action.ownerName !== this.authService.currentUser
        ).length > 0;
        this.privilegedActionsDataSource.pendingActions = pendingActions;
        this.loadingSpinnerService.broadcastLoadingStatus(false);

        if (applyFilter) {
          this.filterChanged(PrivilegedActionsTableComponent.FILTER_OTHER);
          hasInitialized = true;
        }
      });
  }

  ngAfterViewInit() {
    this.cd.detectChanges();
  }

  ngOnDestroy(): void {
    this._dataSourceSubscription.unsubscribe();
  }

  filterChanged(index: number) {

    // hide column 'owner' for 'Mes demandes'
    this._showOwner = (index !== PrivilegedActionsTableComponent.FILTER_MINE);
    this._currentFilter = index;

    this._privilegedActionsDataSource.filter = index;
  }

  viewDetails(privilegedAction: PrivilegedAction) {
    let type = privilegedAction.type;
    let id = privilegedAction.id;
    if (type === 'CONFIGURATION_DEPLOYMENT') {
      this.router.navigate([`/pending-actions/configuration-deployments/${id}`]);
    } else if (type === 'VOTING_MATERIALS_CREATION') {
      this.router.navigate([`/pending-actions/voting-materials-creation/${id}`]);
    } else if (type === 'VOTING_PERIOD_INITIALIZATION') {
      this.router.navigate([`/pending-actions/voting-period-initialization/${id}`]);
    } else if (type === 'VOTING_MATERIALS_INVALIDATION') {
      this.router.navigate([`/pending-actions/voting-materials-invalidation/${id}`]);
    }
  }
}

class PrivilegedActionsDataSource extends DataSource<PrivilegedAction> {
  private _empty = false;
  private _filterChange = new BehaviorSubject(PrivilegedActionsTableComponent.FILTER_ALL);
  private _pendingActionsChange = new BehaviorSubject({});

  constructor(private authService: AuthenticationService,
              private sort: MatSort) {
    super();
  }

  private _pendingActions: Array<PrivilegedAction> = [];

  set pendingActions(value: Array<PrivilegedAction>) {
    this._pendingActions = value;
    this._pendingActionsChange.next(value);
  }

  get filter(): number {
    return this._filterChange.value;
  }

  set filter(filter: number) {
    this._filterChange.next(filter);
  }

  get empty(): boolean {
    return this._empty;
  }

  connect(): Observable<Array<PrivilegedAction>> {
    const displayDataChanges = [
      this._pendingActionsChange,
      this._filterChange,
      this.sort.sortChange
    ];

    return observableMerge(...displayDataChanges).pipe(map(() => {
      const data = this.getSortedData();
      this._empty = !data.length;
      return data;
    }));
  }

  disconnect(): void {
  }

  getSortedData(): Array<PrivilegedAction> {
    const data = this._pendingActions.slice().filter((pendingAction: PrivilegedAction) => {
      switch (this.filter) {
        case PrivilegedActionsTableComponent.FILTER_ALL:
          return true;
        case PrivilegedActionsTableComponent.FILTER_MINE:
          return pendingAction.ownerName === this.authService.currentUser;
        case PrivilegedActionsTableComponent.FILTER_OTHER:
          return pendingAction.ownerName !== this.authService.currentUser;
      }
    });

    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: Date | string = '';
      let propertyB: Date | string = '';

      switch (this.sort.active) {
        case 'type':
          [propertyA, propertyB] = [a.type, b.type];
          break;
        case 'operationName':
          [propertyA, propertyB] = [a.operationName, b.operationName];
          break;
        case 'operationDate':
          [propertyA, propertyB] = [a.operationDate, b.operationDate];
          break;
        case 'statusDate':
          [propertyA, propertyB] = [a.statusDate, b.statusDate];
          break;
        case 'ownerName':
          [propertyA, propertyB] = [a.ownerName, b.ownerName];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this.sort.direction === 'asc' ? 1 : -1);
    });
  }
}
