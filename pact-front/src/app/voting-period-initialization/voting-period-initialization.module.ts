/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { VotingPeriodInitializationRoutingModule } from './voting-period-initialization-routing.module';
import { VotingPeriodInitializationService } from './voting-period-initialization.service';
import { CoreModule } from '../core/core.module';
import { PollingModule } from '../polling/polling.module';
import { AuthenticationModule } from '../authentication/authentication.module';
import { MaterialModule } from '../material/material.module';
import { LayoutModule } from '../layout/layout.module';
import { VotingPeriodInitializationDetailComponent } from './voting-period-initialization-detail/voting-period-initialization-detail.component';
import { VotingPeriodInitializationTableComponent } from './voting-period-initialization-table/voting-period-initialization-table.component';
import { ActionDetailModule } from '../action-detail/action-detail.module';

@NgModule({
  imports: [
    CoreModule,
    PollingModule,
    MaterialModule,
    AuthenticationModule,
    LayoutModule,
    ActionDetailModule,
    VotingPeriodInitializationRoutingModule,
    FlexLayoutModule
  ],
  providers: [VotingPeriodInitializationService],
  declarations: [VotingPeriodInitializationTableComponent, VotingPeriodInitializationDetailComponent]
})
export class VotingPeriodInitializationModule {
}
