/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { VotingPeriodInitializationDetailComponent } from './voting-period-initialization-detail.component';
import { ActivatedRoute, convertToParamMap, Params, Router } from '@angular/router';
import { of } from 'rxjs';
import { VotingPeriodInitializationService } from '../voting-period-initialization.service';
import { ActionConverterService } from '../../action-detail/action-converter.service';
import { VotingPeriodInitialization } from '../model/voting-period-initialization';
import { VotingPeriodConfiguration } from '../model/voting-period-configuration';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';
import { MatSnackBar } from '@angular/material';
import Spy = jasmine.Spy;

describe('VotingPeriodInitializationDetailComponent', () => {
  let component: VotingPeriodInitializationDetailComponent;

  const action1: VotingPeriodInitialization = {
    id: null,
    ownerName: 'user1',
    operationDate: new Date(Date.now()),
    creationDate: null,
    statusDate: null,
    operationName: '201709VP',
    status: 'NOT_CREATED',
    type: 'PERIOD_CREATION',
    votingPeriodConfiguration: <VotingPeriodConfiguration>{
      id: 1,
      configuration: {}
    }
  };
  const action2: VotingPeriodInitialization = {
    id: 1,
    ownerName: 'user1',
    operationDate: new Date(Date.now()),
    creationDate: new Date(Date.now()),
    statusDate: new Date(Date.now()),
    operationName: '201709VP',
    status: 'PENDING',
    type: 'PERIOD_CREATION',
    votingPeriodConfiguration: <VotingPeriodConfiguration>{
      id: 1,
      configuration: {}
    }
  };

  const route: ActivatedRoute = <any> {
    paramMap: of(convertToParamMap(<Params> {id: '1'}))
  };
  const snackBar: MatSnackBar = <any> {
    openFromComponent: jasmine.createSpy('openFromComponent')
  };
  const votingPeriodCreationService: VotingPeriodInitializationService = <any> {
    getById: jasmine.createSpy('getById').and.returnValue(of(action2)),
    getByBusinessId: jasmine.createSpy('getByBusinessId').and
      .returnValue(of(action1)),
    createAction: jasmine.createSpy('createAction').and.returnValue(of(true)),
    approve: jasmine.createSpy('approve').and.returnValue(of(true)),
    reject: jasmine.createSpy('reject').and.returnValue(of(true))
  };

  beforeEach(() => {
    (votingPeriodCreationService.getById as Spy).calls.reset();
    (votingPeriodCreationService.getByBusinessId as Spy).calls.reset();
    (votingPeriodCreationService.createAction as Spy).calls.reset();
    (votingPeriodCreationService.approve as Spy).calls.reset();
    (votingPeriodCreationService.reject as Spy).calls.reset();
  });

  it('should retrieve and approve a pending action', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/pending-actions/1'
    };

    component = new VotingPeriodInitializationDetailComponent(router, route, snackBar,
      votingPeriodCreationService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(votingPeriodCreationService.getById).toHaveBeenCalledTimes(1);
    expect(votingPeriodCreationService.getById).toHaveBeenCalledWith(1);
    expect(votingPeriodCreationService.getByBusinessId).not.toHaveBeenCalled();
    expect(component.action.id).toBe(1);
    expect(component.action.status).toBe('PENDING');

    component.onApproveAction();

    expect(votingPeriodCreationService.approve).toHaveBeenCalledWith(1);
    expect(votingPeriodCreationService.approve).toHaveBeenCalledTimes(1);
    expect(votingPeriodCreationService.reject).not.toHaveBeenCalled();
    expect(votingPeriodCreationService.createAction).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/voting-period-initialization']);
  });

  it('should retrieve and reject a pending action', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/pending-actions/1'
    };

    component = new VotingPeriodInitializationDetailComponent(router, route, snackBar,
      votingPeriodCreationService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(votingPeriodCreationService.getById).toHaveBeenCalledTimes(1);
    expect(votingPeriodCreationService.getById).toHaveBeenCalledWith(1);
    expect(votingPeriodCreationService.getByBusinessId).not.toHaveBeenCalled();
    expect(component.action.id).toBe(1);
    expect(component.action.status).toBe('PENDING');

    component.onRejectAction('reason');

    expect(votingPeriodCreationService.reject).toHaveBeenCalledWith(1, 'reason');
    expect(votingPeriodCreationService.reject).toHaveBeenCalledTimes(1);
    expect(votingPeriodCreationService.approve).not.toHaveBeenCalled();
    expect(votingPeriodCreationService.createAction).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/voting-period-initialization']);
  });

  it('should retrieve and create an voting period configuration', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/voting-period-initialization/1'
    };

    component = new VotingPeriodInitializationDetailComponent(router, route, snackBar,
      votingPeriodCreationService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(votingPeriodCreationService.getByBusinessId).toHaveBeenCalledTimes(1);
    expect(votingPeriodCreationService.getByBusinessId).toHaveBeenCalledWith(1);
    expect(votingPeriodCreationService.getById).not.toHaveBeenCalled();
    expect(component.action.id).toBeNull();
    expect(component.action.status).toBe('NOT_CREATED');

    component.onCreateAction();

    expect(votingPeriodCreationService.createAction).toHaveBeenCalledWith(1);
    expect(votingPeriodCreationService.createAction).toHaveBeenCalledTimes(1);
    expect(votingPeriodCreationService.reject).not.toHaveBeenCalled();
    expect(votingPeriodCreationService.approve).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/voting-period-initialization']);
  });
});
