/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { ConfigurationDeploymentDetailComponent } from './configuration-deployment-detail.component';
import { ActivatedRoute, convertToParamMap, Params, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ConfigurationDeploymentService } from '../configuration-deployment.service';
import { of } from 'rxjs';
import { ConfigurationDeployment } from '../model/configuration-deployment';
import { OperationConfiguration } from '../model/operation-configuration';
import { ActionConverterService } from '../../action-detail/action-converter.service';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';
import Spy = jasmine.Spy;

describe('ConfigurationDeploymentDetailComponent', () => {
  let component: ConfigurationDeploymentDetailComponent;

  const operationConfiguration: OperationConfiguration = {
    id: 1,
    version: 1,
    operationDate: new Date(Date.now()),
    lastChangeDate: new Date(Date.now()),
    siteOpeningDate: new Date(Date.now()),
    siteClosingDate: new Date(Date.now()),
    gracePeriod: 200,
    lastChangeUser: 'user',
    operationName: '201709VP',
    siteTextHash: '123',
    deploymentFiles: []
  };
  const action1: ConfigurationDeployment = {
    id: null,
    ownerName: 'user1',
    operationDate: new Date(Date.now()),
    creationDate: null,
    statusDate: null,
    operationName: '201709VP',
    status: 'NOT_CREATED',
    type: 'CONFIGURATION_DEPLOYMENT',
    operationConfiguration: operationConfiguration
  };
  const action2: ConfigurationDeployment = {
    id: 1,
    ownerName: 'user1',
    operationDate: new Date(Date.now()),
    creationDate: new Date(Date.now()),
    statusDate: new Date(Date.now()),
    operationName: '201709VP',
    status: 'PENDING',
    type: 'CONFIGURATION_DEPLOYMENT',
    operationConfiguration: operationConfiguration
  };

  const route: ActivatedRoute = <any> {
    paramMap: of(convertToParamMap(<Params> {id: '1'}))
  };
  const snackBar: MatSnackBar = <any> {
    openFromComponent: jasmine.createSpy('openFromComponent')
  };
  const configurationDeploymentService: ConfigurationDeploymentService = <any> {
    getById: jasmine.createSpy('getById').and.returnValue(of(action2)),
    getByBusinessId: jasmine.createSpy('getByBusinessId').and
      .returnValue(of(action1)),
    createAction: jasmine.createSpy('createAction').and.returnValue(of(true)),
    approve: jasmine.createSpy('approve').and.returnValue(of(true)),
    reject: jasmine.createSpy('reject').and.returnValue(of(true))
  };

  beforeEach(() => {
    (configurationDeploymentService.getById as Spy).calls.reset();
    (configurationDeploymentService.getByBusinessId as Spy).calls.reset();
    (configurationDeploymentService.createAction as Spy).calls.reset();
    (configurationDeploymentService.approve as Spy).calls.reset();
    (configurationDeploymentService.reject as Spy).calls.reset();
  });

  it('should retrieve and approve a pending action', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/pending-actions/1'
    };

    component = new ConfigurationDeploymentDetailComponent(router, route, snackBar,
      configurationDeploymentService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(configurationDeploymentService.getById).toHaveBeenCalledTimes(1);
    expect(configurationDeploymentService.getById).toHaveBeenCalledWith(1);
    expect(configurationDeploymentService.getByBusinessId).not.toHaveBeenCalled();
    expect(component.action.id).toBe(1);
    expect(component.action.status).toBe('PENDING');
    expect(component.action.operationConfiguration.deploymentFiles.length).toBe(0);

    component.onApproveAction();

    expect(configurationDeploymentService.approve).toHaveBeenCalledWith(1);
    expect(configurationDeploymentService.approve).toHaveBeenCalledTimes(1);
    expect(configurationDeploymentService.reject).not.toHaveBeenCalled();
    expect(configurationDeploymentService.createAction).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/configuration-deployments']);
  });

  it('should retrieve and reject a pending action', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/pending-actions/1'
    };

    component = new ConfigurationDeploymentDetailComponent(router, route, snackBar,
      configurationDeploymentService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(configurationDeploymentService.getById).toHaveBeenCalledTimes(1);
    expect(configurationDeploymentService.getById).toHaveBeenCalledWith(1);
    expect(configurationDeploymentService.getByBusinessId).not.toHaveBeenCalled();
    expect(component.action.id).toBe(1);
    expect(component.action.status).toBe('PENDING');
    expect(component.action.operationConfiguration.deploymentFiles.length).toBe(0);

    component.onRejectAction('reason');

    expect(configurationDeploymentService.reject).toHaveBeenCalledWith(1, 'reason');
    expect(configurationDeploymentService.reject).toHaveBeenCalledTimes(1);
    expect(configurationDeploymentService.approve).not.toHaveBeenCalled();
    expect(configurationDeploymentService.createAction).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/configuration-deployments']);
  });

  it('should retrieve and deploy an operation configuration', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/configuration-deployments/1'
    };

    component = new ConfigurationDeploymentDetailComponent(router, route, snackBar,
      configurationDeploymentService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(configurationDeploymentService.getByBusinessId).toHaveBeenCalledTimes(1);
    expect(configurationDeploymentService.getByBusinessId).toHaveBeenCalledWith(1);
    expect(configurationDeploymentService.getById).not.toHaveBeenCalled();
    expect(component.action.id).toBeNull();
    expect(component.action.status).toBe('NOT_CREATED');
    expect(component.action.operationConfiguration.deploymentFiles.length).toBe(0);

    component.onCreateAction();

    expect(configurationDeploymentService.createAction).toHaveBeenCalledWith(1);
    expect(configurationDeploymentService.createAction).toHaveBeenCalledTimes(1);
    expect(configurationDeploymentService.reject).not.toHaveBeenCalled();
    expect(configurationDeploymentService.approve).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/configuration-deployments']);
  });

});
