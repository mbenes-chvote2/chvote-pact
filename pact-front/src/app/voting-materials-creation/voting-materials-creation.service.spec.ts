/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { VotingMaterialsCreationService } from './voting-materials-creation.service';
import { of } from 'rxjs';
import { PollingService } from '../polling/polling.service';
import { HttpClient } from '@angular/common/http';
import { BaseUrlService } from '../core/base-url-service/base-url.service';

describe('VotingMaterialsCreationService', () => {
  let service: VotingMaterialsCreationService;
  const httpClientMock: HttpClient = <any> {};
  const pollingService: PollingService = <any> {
    poll: jasmine.createSpy('poll').and.returnValue(of([]))
  };
  const baseUrlServiceMock: BaseUrlService = <any> {
    backendBaseUrl: '/api',
  };

  it('should call the materials creation endpoint', () => {
    service = new VotingMaterialsCreationService(httpClientMock, baseUrlServiceMock, pollingService);
    service.newOrPending.subscribe(actions => expect(actions.length).toBe(0));
    expect(pollingService.poll)
      .toHaveBeenCalledWith('/api/privileged-actions/voting-materials-creation/new-or-pending/');
  });
});
