/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { VersionService } from './version.service';
import { HttpClient } from '@angular/common/http';
import { BaseUrlService } from '../core/base-url-service/base-url.service';
import { of } from 'rxjs';

describe('VersionService', () => {
  const timestamp = new Date();

  const httpClientMock: HttpClient = <any> {
    get: jasmine.createSpy('get').and.returnValue(of({buildNumber: 'a5bb23c2', buildTimestamp: timestamp}))
  };
  const baseUrlServiceMock: BaseUrlService = <any> {
    backendBaseUrl: '/api',
  };

  let service: VersionService;

  beforeEach(() => {
    service = new VersionService(httpClientMock, baseUrlServiceMock);
  });

  it('should call the server to retrieve the current version', () => {
    service.currentVersion.subscribe(version => {
      expect(version.buildNumber).toBe('a5bb23c2');
      expect(version.buildTimestamp).toBe(timestamp);
    });
    expect(httpClientMock.get).toHaveBeenCalledWith('/api/version');
  });
});
