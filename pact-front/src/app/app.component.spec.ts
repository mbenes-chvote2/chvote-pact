/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { AppComponent } from './app.component';
import { TranslateService } from '@ngx-translate/core';

describe('AppComponent', () => {
  let availableLangs = [];
  let defaultLang = '';
  let appComponent: AppComponent;
  const translateServiceMock: TranslateService = <any> {
    use: jasmine.createSpy('use'),
    addLangs: (langs: Array<string>) => availableLangs = availableLangs.concat(langs),
    getLangs: () => availableLangs,
    setDefaultLang: (lang: string) => defaultLang = lang,
    getDefaultLang: () => defaultLang,
    getBrowserLang: () => availableLangs[0]
  };

  beforeEach(() => {
    appComponent = new AppComponent(translateServiceMock);
    appComponent.ngOnInit();
  });

  it('should use the browser lang by default', () => {
    expect(translateServiceMock.use).toHaveBeenCalledWith('fr');
  });
});
