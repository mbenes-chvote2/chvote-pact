/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { EventEmitter, Injectable } from '@angular/core';
import { LoginEvent } from './login.event';
import { Observable } from 'rxjs';
import { UserService } from '../user/user.service';
import { isNullOrUndefined } from 'util';

const AUTHORIZATION_KEY = 'authorization';

@Injectable()
export class AuthenticationService {
  private _login: EventEmitter<LoginEvent> = new EventEmitter();
  private _logout: EventEmitter<void> = new EventEmitter();

  constructor(private userService: UserService) {
    const authorization = AuthenticationService.getAuthorization();
    if (!isNullOrUndefined(authorization)) {
      this._currentUser = atob(authorization).split(':')[0];
    }
  }

  private _currentUser: string | undefined;

  get currentUser(): string | undefined {
    return this._currentUser;
  }

  static getAuthorization(): string | null {
    return localStorage.getItem(AUTHORIZATION_KEY);
  }

  login(username: string, password: string): void {
    localStorage.setItem(AUTHORIZATION_KEY, btoa(`${username}:${password}`));
    this.userService.currentUser.subscribe(
      () => {
        this._currentUser = username;
        this._login.emit({username: username});
      },
      () => this._currentUser = undefined
    );
  }

  logout(): void {
    localStorage.removeItem(AUTHORIZATION_KEY);
    this._currentUser = undefined;
    this._logout.emit();
  }

  get loginEvents(): Observable<LoginEvent> {
    return this._login;
  }

  get logoutEvents(): Observable<void> {
    return this._logout;
  }
}
