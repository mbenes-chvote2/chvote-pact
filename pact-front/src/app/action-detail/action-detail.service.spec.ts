/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { ActionDetailService } from './action-detail.service';
import { PollingService } from '../polling/polling.service';
import { of } from 'rxjs';
import { PrivilegedAction } from '../privileged-actions/model/privileged-action';
import { HttpClient, HttpParams } from '@angular/common/http';

describe('ActionDetailService', () => {
  let service: ActionDetailService<PrivilegedAction>;

  const pollingService: PollingService = <any> {
    broadcast: jasmine.createSpy('broadcast')
  };

  const action1: PrivilegedAction = {
    id: 1,
    ownerName: 'toto',
    operationDate: new Date(Date.now()),
    creationDate: new Date(Date.now()),
    statusDate: new Date(Date.now()),
    operationName: '201709VP',
    status: 'PENDING',
    type: 'CONFIGURATION_DEPLOYMENT'
  };
  const action2: PrivilegedAction = {
    id: 2,
    ownerName: 'titi',
    operationDate: new Date(Date.now()),
    creationDate: new Date(Date.now()),
    statusDate: new Date(Date.now()),
    operationName: '201709VP',
    status: 'PENDING',
    type: 'VOTER_REGISTRY_PUBLICATION'
  };

  it('should call the service to retrieve new or pending actions', () => {
    const httpClientMock: HttpClient = <any> {};
    pollingService.poll = jasmine.createSpy('poll').and.returnValue(of([action1, action2]));

    service = new ActionDetailService(httpClientMock, pollingService, '/api/actions');
    service.newOrPending.subscribe(actions => expect(actions.length).toBe(2));
    expect(pollingService.poll).toHaveBeenCalledWith('/api/actions/new-or-pending/');
  });

  it('should call the service to create a new action', () => {
    const httpClientMock: HttpClient = <any> {
      put: jasmine.createSpy('put').and.returnValue(of(true))
    };

    service = new ActionDetailService(httpClientMock, pollingService, '/api/actions');
    service.createAction(1).subscribe(resp => expect(resp).toBeTruthy());
    expect(httpClientMock.put)
      .toHaveBeenCalledWith('/api/actions', 'businessId=1',
        jasmine.any(Object));
  });

  it('should retrieve a configuration deployment by its operation configuration id', () => {
    const httpClientMock: HttpClient = <any> {
      get: jasmine.createSpy('get').and.returnValue(of(action2))
    };
    const params = new HttpParams().set('businessId', (2).toString());

    service = new ActionDetailService(httpClientMock, pollingService, '/api/actions');
    service.getByBusinessId(2).subscribe(action => expect(action.id).toBe(2));
    expect(httpClientMock.get)
      .toHaveBeenCalledWith('/api/actions', {params: params});
  });

  it('should retrieve a configuration deployment by id', () => {
    const httpClientMock: HttpClient = <any> {
      get: jasmine.createSpy('get').and.returnValue(of(action1))
    };

    service = new ActionDetailService(httpClientMock, pollingService, '/api/actions');
    service.getById(1).subscribe(action => expect(action.id).toBe(1));
    expect(httpClientMock.get).toHaveBeenCalledWith('/api/actions/1');
  });

  it('should call the service to approve a pending action by id', () => {
    const action: PrivilegedAction = {
      id: 1,
      ownerName: 'toto',
      operationDate: new Date(Date.now()),
      creationDate: new Date(Date.now()),
      statusDate: new Date(Date.now()),
      operationName: '201709VP',
      status: 'PENDING',
      type: 'CONFIGURATION_DEPLOYMENT'
    };
    const httpClientMock: HttpClient = <any> {
      put: jasmine.createSpy('put').and.returnValue(of(true))
    };

    service = new ActionDetailService(httpClientMock, pollingService, '/api/actions');
    service.approve(1).subscribe(resp => expect(resp).toBeTruthy());
    expect(httpClientMock.put)
      .toHaveBeenCalledWith('/api/actions/1', 'status=APPROVED', jasmine.any(Object));
  });

  it('should call the service to reject a pending action by id', () => {
    const action: PrivilegedAction = {
      id: 1,
      ownerName: 'toto',
      operationDate: new Date(Date.now()),
      creationDate: new Date(Date.now()),
      statusDate: new Date(Date.now()),
      operationName: '201709VP',
      status: 'PENDING',
      type: 'CONFIGURATION_DEPLOYMENT'
    };
    const httpClientMock: HttpClient = <any> {
      put: jasmine.createSpy('put').and.returnValue(of(true))
    };

    service = new ActionDetailService(httpClientMock, pollingService, '/api/actions');
    service.reject(1, 'NotGood').subscribe(resp => expect(resp).toBeTruthy());
    expect(httpClientMock.put).toHaveBeenCalledWith('/api/actions/1',
      'status=REJECTED&rejectionReason=NotGood', jasmine.any(Object));
  });
});
