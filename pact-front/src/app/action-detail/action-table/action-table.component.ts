/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/


import { BehaviorSubject, merge as observableMerge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { ActionMetadata } from '../model/action-metadata';

@Component({
  selector: 'pact-action-table',
  templateUrl: './action-table.component.html',
  styleUrls: ['./action-table.component.scss']
})
export class ActionTableComponent implements OnInit, OnChanges {

  @Input() i18nLabel: string;
  @Input() actions: ActionMetadata[] = [];
  @Input() columns: string[] = [];

  constructor() {
    this._dataSource = new ActionsDataSource();
  }

  private _dataSource: ActionsDataSource;

  get dataSource(): ActionsDataSource {
    return this._dataSource;
  }

  ngOnChanges(): void {
    this._dataSource.actions = this.actions;
  }

  ngOnInit(): void {
    this._dataSource.actions = this.actions;
  }
}

class ActionsDataSource extends DataSource<ActionMetadata> {

  private _empty = false;
  private _actionsChange = new BehaviorSubject({});

  constructor() {
    super();
  }

  private _actions: ActionMetadata[] = [];

  set actions(value: ActionMetadata[]) {
    this._actions = value;
    this._empty = !value.length;
    this._actionsChange.next(value);
  }

  get empty(): boolean {
    return this._empty;
  }

  connect(): Observable<ActionMetadata[]> {
    return observableMerge(this._actionsChange).pipe(map(() => {
      return this._actions;
    }));
  }

  disconnect(): void {
  }

}
