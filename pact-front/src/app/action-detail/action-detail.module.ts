/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionTableComponent } from './action-table/action-table.component';
import { CoreModule } from '../core/core.module';
import { MaterialModule } from '../material/material.module';
import { ActionDetailAccordionRowComponent } from './action-detail-accordion-row/action-detail-accordion-row.component';
import { ActionDetailRowComponent } from './action-detail-row/action-detail-row.component';
import { ActionDetailSubmitRowComponent } from './action-detail-submit-row/action-detail-submit-row.component';
import { AuthenticationModule } from '../authentication/authentication.module';
import { ActionDetailValueComponent } from './action-detail-value/action-detail-value.component';
import { ActionConverterService } from "./action-converter.service";

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    MaterialModule,
    AuthenticationModule
  ],
  declarations: [
    ActionTableComponent,
    ActionDetailRowComponent,
    ActionDetailAccordionRowComponent,
    ActionDetailSubmitRowComponent,
    ActionDetailValueComponent
  ],
  providers: [
    ActionTableComponent,
    ActionDetailRowComponent,
    ActionDetailAccordionRowComponent,
    ActionDetailSubmitRowComponent,
    ActionDetailValueComponent,
    ActionConverterService
  ],
  exports: [
    ActionTableComponent,
    ActionDetailRowComponent,
    ActionDetailValueComponent,
    ActionDetailAccordionRowComponent,
    ActionDetailSubmitRowComponent
  ]
})
export class ActionDetailModule {
}

