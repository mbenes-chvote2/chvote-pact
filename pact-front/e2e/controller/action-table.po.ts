/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, by, element, ExpectedConditions } from 'protractor';
import { SidenavController } from './sidenav.po';

export class ActionTableController {



  static get actionTable() {
    return element(by.css('.action-table'));
  }




  static get emptyTableIsDisplayed() {
    return element(by.css('.pact-empty-table-msg')).isPresent();
  }


  private static get actionsRows() {
    return element.all(by.css('.action-table mat-row'));
  }

  static get numberOfRows() {
    return ActionTableController.actionsRows.count();
  }

  private static getActionByPosition(pos: number) {
    return element.all(by.css('.action-table mat-row button')).get(pos);
  }

  static openActionByPosition(pos: number) {
    return browser.wait(ExpectedConditions.elementToBeClickable(ActionTableController.getActionByPosition(pos)))
      .then(() => ActionTableController.getActionByPosition(pos).click());
  }
}
