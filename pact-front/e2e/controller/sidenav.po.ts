/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, by, element, ExpectedConditions } from 'protractor';

export class SidenavController {
  private static get menuToggleBtn() {
    return element(by.css('#pact-sidenav-toggle-btn'));
  }

  static toggleSidenav() {
    return SidenavController.menuToggleBtn.click();
  }

  private static get pendingCountChip() {
    return element(by.css('#pact-pending-count-chip'));
  }

  static get pendingActionCount() {
    return this.openSideNav()
      .then(() => SidenavController.pendingCountChip.getText())
      .then(txt => parseInt(txt));
  }

  private static get sideNavElmt() {
    return element(by.css('#pact-sidenav'));
  }

  private static get drawerElmt() {
    return element(by.className('mat-drawer-shown'));
  }

  static get isSidenavOpened() {
    return this.sideNavElmt.isDisplayed();
  }

  static get isMenuToggleBtnDisplayed() {
    return this.menuToggleBtn.isDisplayed();
  }

  static openSideNav() {
    return SidenavController.isSidenavOpened.then(isOpened => {
      if (!isOpened) {
        return this.toggleSidenav()
          .then(() => browser.wait(ExpectedConditions.presenceOf(SidenavController.drawerElmt)));
      }
    });
  }

}
