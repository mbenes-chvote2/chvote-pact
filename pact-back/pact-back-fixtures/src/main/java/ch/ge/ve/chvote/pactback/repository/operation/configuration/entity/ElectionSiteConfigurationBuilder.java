/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A builder for {@link ElectionSiteConfiguration} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class ElectionSiteConfigurationBuilder {
  private Long         id;
  private String       ballot;
  private boolean      displayCandidateSearchForm;
  private boolean      allowChangeOfElectoralList;
  private boolean      displayEmptyPosition;
  private boolean      displayCandidatePositionOnACompactBallotPaper;
  private boolean      displayCandidatePositionOnAModifiedBallotPaper;
  private boolean      displaySuffrageCount;
  private boolean      displayListVerificationCode;
  private boolean      allowOpenCandidature;
  private boolean      allowMultipleMandates;
  private boolean      displayVoidOnEmptyBallotPaper;
  private String       candidateInformationDisplayModel;
  private List<String> displayedColumnsOnVerificationTable;
  private List<String> columnsOrderOnVerificationTable;

  private ElectionSiteConfigurationBuilder() {
  }

  public static ElectionSiteConfigurationBuilder anElectionSiteConfiguration() {
    return new ElectionSiteConfigurationBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static ElectionSiteConfigurationBuilder copyOf(ElectionSiteConfiguration prototype) {
    return new ElectionSiteConfigurationBuilder()
        .withId(prototype.getId())
        .withBallot(prototype.getBallot())
        .withDisplayCandidateSearchForm(prototype.isDisplayCandidateSearchForm())
        .withAllowChangeOfElectoralList(prototype.isAllowChangeOfElectoralList())
        .withDisplayEmptyPosition(prototype.isDisplayEmptyPosition())
        .withDisplayCandidatePositionOnACompactBallotPaper(prototype.isDisplayCandidatePositionOnACompactBallotPaper())
        .withDisplayCandidatePositionOnAModifiedBallotPaper(prototype.isDisplayCandidatePositionOnAModifiedBallotPaper())
        .withDisplaySuffrageCount(prototype.isDisplaySuffrageCount())
        .withDisplayListVerificationCode(prototype.isDisplayListVerificationCode())
        .withAllowOpenCandidature(prototype.isAllowOpenCandidature())
        .withAllowMultipleMandates(prototype.isAllowMultipleMandates())
        .withDisplayVoidOnEmptyBallotPaper(prototype.isDisplayVoidOnEmptyBallotPaper())
        .withCandidateInformationDisplayModel(prototype.getCandidateInformationDisplayModel())
        .withDisplayedColumnsOnVerificationTable(prototype.getDisplayedColumnsOnVerificationTable())
        .withColumnsOrderOnVerificationTable(prototype.getColumnsOrderOnVerificationTable());
  }

  public ElectionSiteConfigurationBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public ElectionSiteConfigurationBuilder withBallot(String ballot) {
    this.ballot = ballot;
    return this;
  }

  public ElectionSiteConfigurationBuilder withDisplayCandidateSearchForm(boolean displayCandidateSearchForm) {
    this.displayCandidateSearchForm = displayCandidateSearchForm;
    return this;
  }

  public ElectionSiteConfigurationBuilder withAllowChangeOfElectoralList(boolean allowChangeOfElectoralList) {
    this.allowChangeOfElectoralList = allowChangeOfElectoralList;
    return this;
  }

  public ElectionSiteConfigurationBuilder withDisplayEmptyPosition(boolean displayEmptyPosition) {
    this.displayEmptyPosition = displayEmptyPosition;
    return this;
  }

  public ElectionSiteConfigurationBuilder withDisplayCandidatePositionOnACompactBallotPaper(
      boolean displayCandidatePositionOnACompactBallotPaper) {
    this.displayCandidatePositionOnACompactBallotPaper = displayCandidatePositionOnACompactBallotPaper;
    return this;
  }

  public ElectionSiteConfigurationBuilder withDisplayCandidatePositionOnAModifiedBallotPaper(
      boolean displayCandidatePositionOnAModifiedBallotPaper) {
    this.displayCandidatePositionOnAModifiedBallotPaper = displayCandidatePositionOnAModifiedBallotPaper;
    return this;
  }

  public ElectionSiteConfigurationBuilder withDisplaySuffrageCount(boolean displaySuffrageCount) {
    this.displaySuffrageCount = displaySuffrageCount;
    return this;
  }

  public ElectionSiteConfigurationBuilder withDisplayListVerificationCode(boolean displayListVerificationCode) {
    this.displayListVerificationCode = displayListVerificationCode;
    return this;
  }

  public ElectionSiteConfigurationBuilder withAllowOpenCandidature(boolean allowOpenCandidature) {
    this.allowOpenCandidature = allowOpenCandidature;
    return this;
  }

  public ElectionSiteConfigurationBuilder withAllowMultipleMandates(boolean allowMultipleMandates) {
    this.allowMultipleMandates = allowMultipleMandates;
    return this;
  }

  public ElectionSiteConfigurationBuilder withDisplayVoidOnEmptyBallotPaper(boolean displayVoidOnEmptyBallotPaper) {
    this.displayVoidOnEmptyBallotPaper = displayVoidOnEmptyBallotPaper;
    return this;
  }

  public ElectionSiteConfigurationBuilder withCandidateInformationDisplayModel(
      String candidateInformationDisplayModel) {
    this.candidateInformationDisplayModel = candidateInformationDisplayModel;
    return this;
  }

  public ElectionSiteConfigurationBuilder withDisplayedColumnsOnVerificationTable(Collection<String> values) {
    this.displayedColumnsOnVerificationTable = new ArrayList<>(values);
    return this;
  }

  public ElectionSiteConfigurationBuilder withColumnsOrderOnVerificationTable(Collection<String> values) {
    this.columnsOrderOnVerificationTable = new ArrayList<>(values);
    return this;
  }

  public ElectionSiteConfiguration build() {
    ElectionSiteConfiguration electionSiteConfiguration = new ElectionSiteConfiguration();
    electionSiteConfiguration.setId(id);
    electionSiteConfiguration.setBallot(ballot);
    electionSiteConfiguration.setDisplayCandidateSearchForm(displayCandidateSearchForm);
    electionSiteConfiguration.setAllowChangeOfElectoralList(allowChangeOfElectoralList);
    electionSiteConfiguration.setDisplayEmptyPosition(displayEmptyPosition);
    electionSiteConfiguration
        .setDisplayCandidatePositionOnACompactBallotPaper(displayCandidatePositionOnACompactBallotPaper);
    electionSiteConfiguration
        .setDisplayCandidatePositionOnAModifiedBallotPaper(displayCandidatePositionOnAModifiedBallotPaper);
    electionSiteConfiguration.setDisplaySuffrageCount(displaySuffrageCount);
    electionSiteConfiguration.setDisplayListVerificationCode(displayListVerificationCode);
    electionSiteConfiguration.setAllowOpenCandidature(allowOpenCandidature);
    electionSiteConfiguration.setAllowMultipleMandates(allowMultipleMandates);
    electionSiteConfiguration.setDisplayVoidOnEmptyBallotPaper(displayVoidOnEmptyBallotPaper);
    electionSiteConfiguration.setCandidateInformationDisplayModel(candidateInformationDisplayModel);
    electionSiteConfiguration.setDisplayedColumnsOnVerificationTable(displayedColumnsOnVerificationTable);
    electionSiteConfiguration.setColumnsOrderOnVerificationTable(columnsOrderOnVerificationTable);
    return electionSiteConfiguration;
  }
}
