/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.voter;

import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.DateOfBirth;
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterCountingCircle;
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterEntity;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import java.util.List;

/**
 * A builder for {@link VoterEntityBuilder} objects.
 * <p>
 * Get a new builder with one of its static factory method.
 * </p>
 */
public final class VoterEntityBuilder {
  private Long                id;
  private int                 protocolVoterIndex;
  private String              localPersonId;
  private DateOfBirth         dateOfBirth;
  private VoterCountingCircle voterCountingCircle;
  private String              printingAuthorityName;
  private List<String>        allowedDoiIds;
  private ProtocolInstance    protocolInstance;

  private VoterEntityBuilder() {
  }

  public static VoterEntityBuilder aVoterEntity() {
    return new VoterEntityBuilder();
  }

  public VoterEntityBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public VoterEntityBuilder withProtocolVoterIndex(int protocolVoterIndex) {
    this.protocolVoterIndex = protocolVoterIndex;
    return this;
  }

  public VoterEntityBuilder withLocalPersonId(String localPersonId) {
    this.localPersonId = localPersonId;
    return this;
  }

  public VoterEntityBuilder withDateOfBirth(DateOfBirth dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
    return this;
  }

  public VoterEntityBuilder withVoterCountingCircle(VoterCountingCircle voterCountingCircle) {
    this.voterCountingCircle = voterCountingCircle;
    return this;
  }

  public VoterEntityBuilder withPrintingAuthorityName(String printingAuthorityName) {
    this.printingAuthorityName = printingAuthorityName;
    return this;
  }

  public VoterEntityBuilder withAllowedDoiIds(List<String> allowedDoiIds) {
    this.allowedDoiIds = allowedDoiIds;
    return this;
  }

  public VoterEntityBuilder withProtocolInstance(ProtocolInstance protocolInstance) {
    this.protocolInstance = protocolInstance;
    return this;
  }

  public VoterEntity build() {
    VoterEntity voterEntity = new VoterEntity();
    voterEntity.setId(id);
    voterEntity.setProtocolVoterIndex(protocolVoterIndex);
    voterEntity.setLocalPersonId(localPersonId);
    voterEntity.setDateOfBirth(dateOfBirth);
    voterEntity.setVoterCountingCircle(voterCountingCircle);
    voterEntity.setPrintingAuthorityName(printingAuthorityName);
    voterEntity.setAllowedDoiIds(allowedDoiIds);
    voterEntity.setProtocolInstance(protocolInstance);
    return voterEntity;
  }
}
