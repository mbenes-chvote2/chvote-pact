/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.fixtures.repository;

import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgress;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgressBuilder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ProtocolStages {

  private ProtocolStages() {
    throw new AssertionError("Not instantiable");
  }

  public static List<ProtocolStageProgress> upToElectionOfficerStage(int nbCC, LocalDateTime lastModified) {
    final List<ProtocolStageProgress> list = new ArrayList<>();

    AtomicReference<LocalDateTime> timestamp = new AtomicReference<>(lastModified);

    list.add(ProtocolStageProgressBuilder.aProtocolStageProgress()
                                         .withStage(ProtocolStage.SEND_ELECTION_OFFICER_KEY).withTotal(1L)
                                         .withDone(1L)
                                         .withLastModificationDate(lastModified)
                                         .withStartDate(timestamp.updateAndGet(t -> t.minusSeconds(7)))
                                         .build());

    ProtocolStageProgressBuilder builder =
        ProtocolStageProgressBuilder.aProtocolStageProgress()
                                    .withStage(ProtocolStage.REQUEST_PRIVATE_CREDENTIALS)
                                    .withTotal(100L).withDone(100L)
                                    .withLastModificationDate(timestamp.updateAndGet(t -> t.minusSeconds(2)))
                                    .withStartDate(timestamp.updateAndGet(t -> t.minusSeconds(23)));

    for (int i = 0; i < nbCC; i++) {
      list.add(builder.withCcIndex(i).build());
    }

    list.add(ProtocolStageProgressBuilder.aProtocolStageProgress()
                                     .withStage(ProtocolStage.BUILD_PUBLIC_CREDENTIALS)
                                     .withTotal((long) nbCC).withDone((long) nbCC)
                                     .withLastModificationDate(timestamp.updateAndGet(t -> t.minusSeconds(1)))
                                     .withStartDate(timestamp.updateAndGet(t -> t.minusSeconds(9))).build());

    long nbVoters = 100L;
    ProtocolStageProgressBuilder builder2 =
        ProtocolStageProgressBuilder.aProtocolStageProgress()
                                    .withStage(ProtocolStage.SEND_VOTERS)
                                    .withTotal(nbVoters).withDone(nbVoters)
                                    .withLastModificationDate(timestamp.updateAndGet(t -> t.minusSeconds(2)))
                                    .withStartDate(timestamp.updateAndGet(t -> t.minusSeconds(28)));

    for (int i = 0; i < nbCC; i++) {
      list.add(builder2.withCcIndex(i).build());
    }

    list.add(ProtocolStageProgressBuilder.aProtocolStageProgress()
                                         .withStage(ProtocolStage.PUBLISH_PRIMES)
                                         .withTotal((long) nbCC).withDone((long) nbCC)
                                         .withLastModificationDate(timestamp.updateAndGet(t -> t.minusSeconds(1)))
                                         .withStartDate(timestamp.updateAndGet(t -> t.minusSeconds(4))).build());

    list.add(ProtocolStageProgressBuilder.aProtocolStageProgress()
                                         .withStage(ProtocolStage.SEND_ELECTION_SET)
                                         .withTotal(1L).withDone(1L)
                                         .withLastModificationDate(timestamp.updateAndGet(t -> t.minusSeconds(2)))
                                         .withStartDate(timestamp.updateAndGet(t -> t.minusSeconds(6))).build());

    list.add(ProtocolStageProgressBuilder.aProtocolStageProgress()
                                         .withStage(ProtocolStage.STORE_PUBLIC_KEY_PARTS)
                                         .withTotal((long) nbCC).withDone((long) nbCC)
                                         .withLastModificationDate(timestamp.updateAndGet(t -> t.minusSeconds(1)))
                                         .withStartDate(timestamp.updateAndGet(t -> t.minusSeconds(2))).build());

    list.add(ProtocolStageProgressBuilder.aProtocolStageProgress()
                                         .withStage(ProtocolStage.PUBLISH_PUBLIC_KEY_PARTS)
                                         .withTotal((long) nbCC).withDone((long) nbCC)
                                         .withLastModificationDate(timestamp.updateAndGet(t -> t.minusSeconds(1)))
                                         .withStartDate(timestamp.updateAndGet(t -> t.minusSeconds(2))).build());

    list.add(ProtocolStageProgressBuilder.aProtocolStageProgress()
                                         .withStage(ProtocolStage.SEND_PUBLIC_PARAMETERS)
                                         .withTotal(1L).withDone(1L)
                                         .withLastModificationDate(timestamp.updateAndGet(t -> t.minusSeconds(2)))
                                         .withStartDate(timestamp.updateAndGet(t -> t.minusSeconds(6))).build());

    Collections.reverse(list);
    return Collections.unmodifiableList(list);
  }

}
