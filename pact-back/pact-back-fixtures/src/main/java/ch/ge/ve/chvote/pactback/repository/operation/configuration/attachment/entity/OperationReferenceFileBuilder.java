/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity;

import ch.ge.ve.chvote.pactback.fixtures.ResourceLoadingException;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import com.google.common.base.Preconditions;
import com.google.common.base.Verify;
import com.google.common.hash.Hashing;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * A builder for {@link OperationReferenceFile} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class OperationReferenceFileBuilder {
  private String                                 messageId;
  private LocalDateTime                          generationDate;
  private String                                 sourceApplicationManufacturer;
  private String                                 sourceApplicationProduct;
  private String                                 name;
  private String                                 sourceApplicationProductVersion;
  private AttachmentType                         type;
  private String                                 signatureAuthor;
  private LocalDateTime                          importDate;
  private LocalDateTime                          signatureExpiryDate;
  private String                                 fileHash;
  private OperationReferenceFile.SignatureStatus signatureStatus;
  private byte[]                                 file;

  private OperationReferenceFileBuilder() {
  }

  private OperationReferenceFileBuilder(String name) {
    this.name = name;
  }

  public static OperationReferenceFileBuilder aVotationRepository(String name) {
    return new OperationReferenceFileBuilder(name).withType(AttachmentType.OPERATION_REPOSITORY_VOTATION);
  }

  public static OperationReferenceFileBuilder anElectionRepository(String name) {
    return new OperationReferenceFileBuilder(name).withType(AttachmentType.OPERATION_REPOSITORY_ELECTION);
  }

  public static OperationReferenceFileBuilder aDomainOfInfluence(String name) {
    return new OperationReferenceFileBuilder(name).withType(AttachmentType.DOMAIN_OF_INFLUENCE);
  }

  public static OperationReferenceFileBuilder aRegister(String name) {
    return new OperationReferenceFileBuilder(name).withType(AttachmentType.OPERATION_REGISTER);
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static OperationReferenceFileBuilder copyOf(OperationReferenceFile prototype) {
    return new OperationReferenceFileBuilder()
        .withMessageId(prototype.getMessageId())
        .withGenerationDate(prototype.getGenerationDate())
        .withSourceApplicationManufacturer(prototype.getSourceApplicationManufacturer())
        .withSourceApplicationProduct(prototype.getSourceApplicationProduct())
        .withName(prototype.getName())
        .withSourceApplicationProductVersion(prototype.getSourceApplicationProductVersion())
        .withType(prototype.getType())
        .withSignatureAuthor(prototype.getSignatureAuthor())
        .withImportDate(prototype.getImportDate())
        .withSignatureExpiryDate(prototype.getSignatureExpiryDate())
        .withFileHash(prototype.getFileHash())
        .withSignatureStatus(prototype.getSignatureStatus())
        .withFile(prototype.getFile());
  }

  public OperationReferenceFileBuilder withMessageId(String messageId) {
    this.messageId = messageId;
    return this;
  }

  public OperationReferenceFileBuilder withGenerationDate(LocalDateTime generationDate) {
    this.generationDate = generationDate;
    return this;
  }

  public OperationReferenceFileBuilder withSourceApplicationManufacturer(String sourceApplicationManufacturer) {
    this.sourceApplicationManufacturer = sourceApplicationManufacturer;
    return this;
  }

  public OperationReferenceFileBuilder withSourceApplicationProduct(String sourceApplicationProduct) {
    this.sourceApplicationProduct = sourceApplicationProduct;
    return this;
  }

  public OperationReferenceFileBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public OperationReferenceFileBuilder withSourceApplicationProductVersion(String sourceApplicationProductVersion) {
    this.sourceApplicationProductVersion = sourceApplicationProductVersion;
    return this;
  }

  // private because chosen at construction time
  private OperationReferenceFileBuilder withType(AttachmentType type) {
    Verify.verify(type.isOperationReferenceFile(), "%s is not an OperationReference attachment", type);
    this.type = type;
    return this;
  }

  public OperationReferenceFileBuilder withSignatureAuthor(String signatureAuthor) {
    this.signatureAuthor = signatureAuthor;
    return this;
  }

  public OperationReferenceFileBuilder withImportDate(LocalDateTime importDate) {
    this.importDate = importDate;
    return this;
  }

  public OperationReferenceFileBuilder withSignatureExpiryDate(LocalDateTime signatureExpiryDate) {
    this.signatureExpiryDate = signatureExpiryDate;
    return this;
  }

  public OperationReferenceFileBuilder withSignatureStatus(OperationReferenceFile.SignatureStatus signatureStatus) {
    this.signatureStatus = signatureStatus;
    return this;
  }

  /**
   * Sets the file's content. Methods exist to set both content and hash consistently.
   *
   * @see #withRealFile(Path)
   * @see #withCalculatedFileHash()
   */
  public OperationReferenceFileBuilder withFile(byte[] content) {
    this.file = Arrays.copyOf(content, content.length);
    return this;
  }

  /**
   * Sets the file hash - independent of the file's content. Methods exist to set both content and hash consistently.
   *
   * @see #withRealFile(Path)
   * @see #withCalculatedFileHash()
   */
  public OperationReferenceFileBuilder withFileHash(String fileHash) {
    this.fileHash = fileHash;
    return this;
  }

  /**
   * Sets the hash for the file's content - content MUST have been set before calling this method !
   */
  public OperationReferenceFileBuilder withCalculatedFileHash() {
    Preconditions.checkNotNull(file, "File's content must have been set prior to hash calculation");
    return withFileHash( Hashing.sha256().hashBytes(file).toString() );
  }

  public OperationReferenceFileBuilder withRealFile(Path path) {
    try {
      byte[] bytes = Files.readAllBytes(path);
      return withFile(bytes).withCalculatedFileHash();
    } catch (Exception e) {
      throw new ResourceLoadingException("Failed reading file " + path, e);
    }
  }

  public OperationReferenceFile build() {
    OperationReferenceFile operationReferenceFile = new OperationReferenceFile();
    operationReferenceFile.setMessageId(messageId);
    operationReferenceFile.setGenerationDate(generationDate);
    operationReferenceFile.setSourceApplicationManufacturer(sourceApplicationManufacturer);
    operationReferenceFile.setSourceApplicationProduct(sourceApplicationProduct);
    operationReferenceFile.setName(name);
    operationReferenceFile.setSourceApplicationProductVersion(sourceApplicationProductVersion);
    operationReferenceFile.setType(type);
    operationReferenceFile.setSignatureAuthor(signatureAuthor);
    operationReferenceFile.setImportDate(importDate);
    operationReferenceFile.setSignatureExpiryDate(signatureExpiryDate);
    operationReferenceFile.setFileHash(fileHash);
    operationReferenceFile.setSignatureStatus(signatureStatus);
    operationReferenceFile.setFile(file);
    return operationReferenceFile;
  }
}
