/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import java.util.ArrayList;
import java.util.List;

/**
 * A builder for {@link ElectionSiteConfigurationVo} objects.
 * <p>
 * Get a new builder with one of its static factory method.
 * </p>
 */
public final class ElectionSiteConfigurationVoBuilder {
  private String       ballot;
  private boolean      displayCandidateSearchForm;
  private boolean      allowChangeOfElectoralList;
  private boolean      displayEmptyPosition;
  private boolean      displayCandidatePositionOnACompactBallotPaper;
  private boolean      displayCandidatePositionOnAModifiedBallotPaper;
  private boolean      displaySuffrageCount;
  private boolean      displayListVerificationCode;
  private boolean      allowOpenCandidature;
  private boolean      allowMultipleMandates;
  private boolean      displayVoidOnEmptyBallotPaper;
  private String       candidateInformationDisplayModel;
  private List<String> displayedColumnsOnVerificationTable;
  private List<String> columnsOrderOnVerificationTable;

  private ElectionSiteConfigurationVoBuilder() {
  }

  public static ElectionSiteConfigurationVoBuilder anElectionSiteConfigurationVo() {
    return new ElectionSiteConfigurationVoBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   * All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   *
   * @return a new builder instance
   */
  public static ElectionSiteConfigurationVoBuilder copyOf(ElectionSiteConfigurationVo prototype) {
    return new ElectionSiteConfigurationVoBuilder()
        .withBallot(prototype.getBallot())
        .withDisplayCandidateSearchForm(prototype.isDisplayCandidateSearchForm())
        .withAllowChangeOfElectoralList(prototype.isAllowChangeOfElectoralList())
        .withDisplayEmptyPosition(prototype.isDisplayEmptyPosition())
        .withDisplayCandidatePositionOnACompactBallotPaper(prototype.isDisplayCandidatePositionOnACompactBallotPaper())
        .withDisplayCandidatePositionOnAModifiedBallotPaper(
            prototype.isDisplayCandidatePositionOnAModifiedBallotPaper())
        .withDisplaySuffrageCount(prototype.isDisplaySuffrageCount())
        .withDisplayListVerificationCode(prototype.isDisplayListVerificationCode())
        .withAllowOpenCandidature(prototype.isAllowOpenCandidature())
        .withAllowMultipleMandates(prototype.isAllowMultipleMandates())
        .withDisplayVoidOnEmptyBallotPaper(prototype.isDisplayVoidOnEmptyBallotPaper())
        .withCandidateInformationDisplayModel(prototype.getCandidateInformationDisplayModel())
        .withDisplayedColumnsOnVerificationTable(prototype.getDisplayedColumnsOnVerificationTable())
        .withColumnsOrderOnVerificationTable(prototype.getColumnsOrderOnVerificationTable());
  }

  public ElectionSiteConfigurationVoBuilder withBallot(String ballot) {
    this.ballot = ballot;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withDisplayCandidateSearchForm(boolean displayCandidateSearchForm) {
    this.displayCandidateSearchForm = displayCandidateSearchForm;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withAllowChangeOfElectoralList(boolean allowChangeOfElectoralList) {
    this.allowChangeOfElectoralList = allowChangeOfElectoralList;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withDisplayEmptyPosition(boolean displayEmptyPosition) {
    this.displayEmptyPosition = displayEmptyPosition;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withDisplayCandidatePositionOnACompactBallotPaper(
      boolean displayCandidatePositionOnACompactBallotPaper) {
    this.displayCandidatePositionOnACompactBallotPaper = displayCandidatePositionOnACompactBallotPaper;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withDisplayCandidatePositionOnAModifiedBallotPaper(
      boolean displayCandidatePositionOnAModifiedBallotPaper) {
    this.displayCandidatePositionOnAModifiedBallotPaper = displayCandidatePositionOnAModifiedBallotPaper;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withDisplaySuffrageCount(boolean displaySuffrageCount) {
    this.displaySuffrageCount = displaySuffrageCount;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withDisplayListVerificationCode(boolean displayListVerificationCode) {
    this.displayListVerificationCode = displayListVerificationCode;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withAllowOpenCandidature(boolean allowOpenCandidature) {
    this.allowOpenCandidature = allowOpenCandidature;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withAllowMultipleMandates(boolean allowMultipleMandates) {
    this.allowMultipleMandates = allowMultipleMandates;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withDisplayVoidOnEmptyBallotPaper(boolean displayVoidOnEmptyBallotPaper) {
    this.displayVoidOnEmptyBallotPaper = displayVoidOnEmptyBallotPaper;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withCandidateInformationDisplayModel(
      String candidateInformationDisplayModel) {
    this.candidateInformationDisplayModel = candidateInformationDisplayModel;
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withDisplayedColumnsOnVerificationTable(List<String> values) {
    displayedColumnsOnVerificationTable = new ArrayList<>(values);
    return this;
  }

  public ElectionSiteConfigurationVoBuilder withColumnsOrderOnVerificationTable(List<String> values) {
    columnsOrderOnVerificationTable = new ArrayList<>(values);
    return this;
  }

  public ElectionSiteConfigurationVo build() {
    ElectionSiteConfigurationVo electionSiteConfigurationVo = new ElectionSiteConfigurationVo();
    electionSiteConfigurationVo.setBallot(ballot);
    electionSiteConfigurationVo.setDisplayCandidateSearchForm(displayCandidateSearchForm);
    electionSiteConfigurationVo.setAllowChangeOfElectoralList(allowChangeOfElectoralList);
    electionSiteConfigurationVo.setDisplayEmptyPosition(displayEmptyPosition);
    electionSiteConfigurationVo
        .setDisplayCandidatePositionOnACompactBallotPaper(displayCandidatePositionOnACompactBallotPaper);
    electionSiteConfigurationVo
        .setDisplayCandidatePositionOnAModifiedBallotPaper(displayCandidatePositionOnAModifiedBallotPaper);
    electionSiteConfigurationVo.setDisplaySuffrageCount(displaySuffrageCount);
    electionSiteConfigurationVo.setDisplayListVerificationCode(displayListVerificationCode);
    electionSiteConfigurationVo.setAllowOpenCandidature(allowOpenCandidature);
    electionSiteConfigurationVo.setAllowMultipleMandates(allowMultipleMandates);
    electionSiteConfigurationVo.setDisplayVoidOnEmptyBallotPaper(displayVoidOnEmptyBallotPaper);
    electionSiteConfigurationVo.setCandidateInformationDisplayModel(candidateInformationDisplayModel);
    electionSiteConfigurationVo.setDisplayedColumnsOnVerificationTable(displayedColumnsOnVerificationTable);
    electionSiteConfigurationVo.setColumnsOrderOnVerificationTable(columnsOrderOnVerificationTable);
    return electionSiteConfigurationVo;
  }
}
