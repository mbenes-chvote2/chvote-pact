/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.fixtures.repository;

import ch.ge.ve.chvote.pactback.fixtures.ResourceLoadingException;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.PublicKeyFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.PublicKeyFileBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfigurationBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.entity.OperationBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfigurationBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.periods.entity.VotingPeriodConfigurationBuilder;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstanceBuilder;
import com.google.common.io.ByteStreams;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.springframework.core.io.ClassPathResource;

public class Operations {

  private static final LocalDate FIRST_DAY = LocalDate.of(2017, 9, 15);
  private static final LocalDate JUNE_8_2018 = LocalDate.of(2018, 6, 8);

  private static final LocalTime START_OF_DAY = LocalTime.of(8, 0);
  private static final LocalTime END_OF_DAY = LocalTime.of(18, 0);

  private static final LocalDateTime INIT_DATE = FIRST_DAY.atTime(10, 30);

  private Operations() {
    throw new AssertionError("Not instantiable");
  }


  public static OperationBuilder failedOnResultsGenerationBuilder() {
    final String fixturesAuthor = "Fixtures module";
    final LocalDateTime opCreationDate = JUNE_8_2018.atTime(START_OF_DAY);
    final LocalDate opDate = opCreationDate.plusMonths(2).toLocalDate();

    final String simulationName = "Simulation of failedOnResultsGeneration fixture";

    final LocalDateTime statusDate = opCreationDate.plusDays(10).plusMinutes(78);
    final ProtocolInstance protocolInstance =
        ProtocolInstanceBuilder.aProtocolInstance()
                               .withEnvironment(ProtocolEnvironment.PRODUCTION)
                               .withNodeRepartitionKey("FixtureNode")
                               .withProtocolId("0123-4567-8987-6543")
                               .withStatus(ProtocolInstanceStatus.TALLY_ARCHIVE_GENERATION_FAILURE)
                               .withStatusDate(statusDate)
                               .withProgress(ProtocolStages.upToElectionOfficerStage(2, statusDate.minusMinutes(3)))
                               .build();


    OperationConfiguration operationConfiguration =
        OperationConfigurationBuilder.anOperationConfiguration()
                                     .withOperationName("Fixture that failed on results generation")
                                     .withOperationLabel("failedOnResultsGeneration")
                                     .withOperationDate(opDate.atTime(LocalTime.NOON))
                                     .withSiteTextHash("8f3899aead04b608c0d444a50714bc997fcc23028d4e71ce8e9323c17a798ba1")
                                     .withValidationStatus(ValidationStatus.VALIDATED)
                                     .withLastChange(Users.user1().getUsername(), opCreationDate)
                                     .withSiteOpeningDate(opDate.minusWeeks(2).atTime(LocalTime.MIDNIGHT))
                                     .withSiteClosingDate(opDate.atTime(END_OF_DAY))
                                     .withGracePeriod(30)
                                     .withCanton("GE")
                                     .withGroupVotation(true)
                                     .withProtocolInstance(protocolInstance)
                                     .withTestPrinter(Printers.bohVirtualPrinter())
                                     .build();

    VotingMaterialsConfiguration votingMaterialsConfiguration =
        VotingMaterialsConfigurationBuilder.aVotingMaterialsConfiguration()
                                           .withAuthor(fixturesAuthor)
                                           .withSubmissionDate(opCreationDate.plusMinutes(30))
                                           .withVotingCardLabel("Fixture cards")
                                           .withTarget(DeploymentTarget.SIMULATION)
                                           .withSimulationName(simulationName)
                                           .build();

    VotingPeriodConfiguration votingPeriodConfiguration =
        VotingPeriodConfigurationBuilder.aVotingPeriodConfiguration()
                                        .withTarget(DeploymentTarget.SIMULATION)
                                        .withSimulationName(simulationName)
                                        .withAuthor(fixturesAuthor)
                                        .withSiteOpeningDate(operationConfiguration.getSiteOpeningDate())
                                        .withSiteClosingDate(operationConfiguration.getSiteClosingDate())
                                        .withGracePeriod(operationConfiguration.getGracePeriod())
                                        .withElectoralAuthorityKey(electoralAuthorityKeyFile())
                                        .withSubmissionDate(opCreationDate.plusHours(2))
                                        .build();

    return OperationBuilder.anOperation().withClientId("failed-on-results-generation-fixture")
                           .withCreationDate(opCreationDate)
                           .withDeployedConfiguration(operationConfiguration)
                           .withVotingMaterialsConfiguration(votingMaterialsConfiguration)
                           .withVotingPeriodConfiguration(votingPeriodConfiguration);
  }

  public static Operation failedOnResultsGeneration() {
    return failedOnResultsGenerationBuilder().build();
  }


  public static Operation createdTestConfiguration() {
    final LocalDateTime opCreationDate = JUNE_8_2018.atTime(START_OF_DAY);
    final LocalDate opDate = opCreationDate.plusMonths(2).toLocalDate();
    final LocalDateTime statusDate = opCreationDate.plusDays(10).plusMinutes(78);
    final ProtocolInstance protocolInstance =
        ProtocolInstanceBuilder.aProtocolInstance()
                               .withEnvironment(ProtocolEnvironment.TEST)
                               .withNodeRepartitionKey("FixtureNode")
                               .withProtocolId("0123-4567-8987-6543")
                               .withStatus(ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES)
                               .withStatusDate(statusDate)
                               .withProgress(ProtocolStages.upToElectionOfficerStage(2, statusDate.minusMinutes(3)))
                               .build();

    OperationConfiguration operationConfiguration =
        OperationConfigurationBuilder.anOperationConfiguration()
                                     .withOperationName("Fixture that failed on results generation")
                                     .withOperationLabel("failedOnResultsGeneration")
                                     .withOperationDate(opDate.atTime(LocalTime.NOON))
                                     .withSiteTextHash(
                                         "8f3899aead04b608c0d444a50714bc997fcc23028d4e71ce8e9323c17a798ba1")
                                     .withValidationStatus(ValidationStatus.VALIDATED)
                                     .withLastChange(Users.user1().getUsername(), opCreationDate)
                                     .withSiteOpeningDate(opDate.minusWeeks(2).atTime(LocalTime.MIDNIGHT))
                                     .withSiteClosingDate(opDate.atTime(END_OF_DAY))
                                     .withGracePeriod(30)
                                     .withCanton("GE")
                                     .withGroupVotation(true)
                                     .withProtocolInstance(protocolInstance)
                                     .withTestPrinter(Printers.bohVirtualPrinter())
                                     .build();

    return OperationBuilder.anOperation().withClientId("created-test-configuration")
                           .withCreationDate(opCreationDate)
                           .withConfigurationInTest(operationConfiguration).build();

  }


  private static PublicKeyFile electoralAuthorityKeyFile() {
    final ClassPathResource keyFile = new ClassPathResource("fixtures/public-keys/election-officer-public-key.json");
    try {
      return PublicKeyFileBuilder.aPublicKeyFile()
          .withName("electoral-authority-pk.json")
          .withImportDate(INIT_DATE)
          .withExternalIdentifier(-2040773991572645938L)
          .withFile(ByteStreams.toByteArray(keyFile.getInputStream()))
          .withCalculatedFileHash()
          .build();
    } catch (IOException e) {
      throw new ResourceLoadingException("Failed to read " + keyFile, e);
    }
  }

}
