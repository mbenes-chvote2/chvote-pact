/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.scheduler;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 * Annotation to declare a {@link SpringBootTest} where we would like all
 * {@link org.springframework.scheduling.annotation.Scheduled @Scheduled} components to be excluded
 * from the container's context.
 * <p>
 *   This is equivalent to running the test with the following annotations :
 *   <pre>
 *     &#64;SpringBootTest
 *     &#64;ActiveProfiles("no-scheduler")
 *     public class MyComponentIntegrationTest { ... }
 *   </pre>
 *
 *   Which means that the scheduler components are expected to be declared with a {@code @Profile("!no-scheduler")}
 *   annotation : as the profile is activated by {@code @SpringBootTestWithoutScheduler} they will not
 *   be loaded by Spring.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@SpringBootTest
@ActiveProfiles("no-scheduler")
public @interface SpringBootTestWithoutScheduler {
}
