/*
 * #%L
 * chvote-pact
 * %%
 * Copyright (C) 2016 - 2018 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.chvote.pactback.scheduler.tasks

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.scheduler.SpringBootTestWithoutScheduler
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.transaction.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

/**
 * This REST controller manages quite directly the repository. Most of the complexity is to expose data
 * with a REST interface, in an idempotent manner and with consistent response codes.
 *
 * The simplest and most efficient way to test it is with REST tests using a fully integrated context.
 */
@SpringBootTestWithoutScheduler
@AutoConfigureMockMvc
@Transactional
class ScheduledTasksControllerTest extends Specification {

  private DateTimeFormatter jsdatef = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss.SSS")

  @Autowired
  private MockMvc mvc

  @Autowired
  private ScheduledTasksRepository repository

  def "should list existing tasks"() {
    given:
    def firstEndTime = LocalDate.now().plusWeeks(2).atTime(18, 30)
    def secondEndTime = LocalDate.now().plusWeeks(6).atTime(20, 0)
    def secondTriggeredTime = LocalDateTime.now().minusMinutes(10)
    repository.saveAll([
            new ScheduledTask(operationId: "first", votingPeriodEndTime: firstEndTime),
            new ScheduledTask(operationId: "second", votingPeriodEndTime: secondEndTime, triggeredTime: secondTriggeredTime, httpStatusReceived: 201)
    ])

    when:
    def httpResponse = mvc.perform(get("/tasks/operations")
            .with(httpBasic("test-user", "password")))

    then:
    httpResponse.andExpect(status().isOk())
            .andExpect(jsonPath('$.[0].operationId').value("first"))
            .andExpect(jsonPath('$.[0].votingPeriodEndTime').value("JSDate[${jsdatef.format(firstEndTime)}]".toString()))
            .andExpect(jsonPath('$.[0].triggeredTime').doesNotExist())
            .andExpect(jsonPath('$.[0].httpStatusReceived').doesNotExist())
            .andExpect(jsonPath('$.[1].operationId').value("second"))
            .andExpect(jsonPath('$.[1].votingPeriodEndTime').value("JSDate[${jsdatef.format(secondEndTime)}]".toString()))
            .andExpect(jsonPath('$.[1].triggeredTime').value("JSDate[${jsdatef.format(secondTriggeredTime)}]".toString()))
            .andExpect(jsonPath('$.[1].httpStatusReceived').value(201))
  }

  def "should register a new operation"() {
    given:
    // an empty repository
    def operationId = "test-add"
    def operationEndTime = "2018-12-10T16:45:00"

    when:
    def httpResponse = mvc.perform(put("/tasks/operations/$operationId").param("date", operationEndTime)
            .with(httpBasic("test-user", "password")))

    then:
    httpResponse.andExpect(status().isCreated())

    and:
    def allTasks = repository.findAll()
    allTasks.size() == 1
    allTasks[0].properties.subMap(["operationId", "votingPeriodEndTime", "triggeredTime", "httpStatusReceived"]) == [
            operationId: operationId, votingPeriodEndTime: LocalDateTime.of(2018, 12, 10, 16, 45),
            triggeredTime: null, httpStatusReceived: null
    ]
  }

  def "should have an idempotent behaviour / response when registering the same operation again"() {
    given:
    def operationId = "test-add"
    def operationEndTime = "2018-12-10T16:45:00"
    def existingEndTime = LocalDateTime.of(2018, 12, 10, 16, 45)
    def task = repository.save(new ScheduledTask(operationId: operationId, votingPeriodEndTime: existingEndTime))

    when:
    def httpResponse = mvc.perform(put("/tasks/operations/$operationId").param("date", operationEndTime)
            .with(httpBasic("test-user", "password")))

    then:
    httpResponse.andExpect(status().isOk())

    and:
    def allTasks = repository.findAll()
    allTasks.size() == 1
    allTasks[0] == task
  }

  def "should indicate a conflict - 409 - when registering the same operation with a different date"() {
    given:
    def operationId = "test-add"
    def existingEndTime = LocalDateTime.of(2018, 12, 10, 16, 45)
    def differentEndTime = "2018-12-15T19:30:00"
    repository.save(new ScheduledTask(operationId: operationId, votingPeriodEndTime: existingEndTime))

    when:
    def httpResponse = mvc.perform(put("/tasks/operations/$operationId").param("date", differentEndTime)
            .with(httpBasic("test-user", "password")))

    then:
    httpResponse.andExpect(status().isConflict())

    and:
    def allTasks = repository.findAll()
    allTasks.size() == 1
    allTasks[0].operationId == operationId
    allTasks[0].votingPeriodEndTime == existingEndTime
  }

  def "should delete an already-triggered operation"() {
    given:
    def operationEndTime = LocalDate.now().atTime(12, 0)
    def triggeredTime = LocalDateTime.now().minusMinutes(10)
    repository.save(new ScheduledTask(operationId: "test-delete", votingPeriodEndTime: operationEndTime,
            triggeredTime: triggeredTime))

    when:
    def httpResponse = mvc.perform(delete("/tasks/operations/test-delete")
            .with(httpBasic("test-user", "password")))

    then:
    httpResponse.andExpect(status().isOk())

    and:
    repository.findAll().isEmpty()
  }

  def "should delete a non-triggered operation"() {
    given:
    def operationEndTime = LocalDate.now().atTime(12, 0)
    repository.save(new ScheduledTask(operationId: "test-delete", votingPeriodEndTime: operationEndTime))

    when:
    def httpResponse = mvc.perform(delete("/tasks/operations/test-delete")
            .with(httpBasic("test-user", "password")))

    then:
    httpResponse.andExpect(status().isOk())

    and:
    repository.findAll().isEmpty()
  }

  def "should fail to delete a non-existing operation"() {
    given:
    def operationEndTime = LocalDate.now().atTime(12, 0)
    repository.save(new ScheduledTask(operationId: "sample", votingPeriodEndTime: operationEndTime))

    def requestClientId = "other-id"

    when:
    def httpResponse = mvc.perform(delete("/tasks/operations/$requestClientId")
            .with(httpBasic("test-user", "password")))

    then:
    httpResponse.andExpect(status().isNotFound())
  }
}
