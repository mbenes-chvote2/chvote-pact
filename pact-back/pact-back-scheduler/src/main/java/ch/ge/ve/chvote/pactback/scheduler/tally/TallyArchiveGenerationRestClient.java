/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.scheduler.tally;

import java.util.Collections;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * REST client to communicate with the endpoint for demanding the tally-archive generation to the PACT.
 */
@Component
public class TallyArchiveGenerationRestClient {

  private final RestTemplate restTemplate;

  public TallyArchiveGenerationRestClient(@Value("${chvote.pact.b2b-host-uri:}") String rootUri,
                                          RestTemplateBuilder restTemplateBuilder) {
    this.restTemplate = restTemplateBuilder.rootUri(rootUri)
                                           .basicAuthorization("pact-scheduler", "password")
                                           .build();
  }

  /**
   * PUT a generation demand for an operation to the PACT.
   *
   * @param operationId identifier of the operation in the "client" system
   * @return the status returned by the endpoint
   *
   * @throws org.springframework.web.client.HttpClientErrorException if status is 4xx
   * @throws org.springframework.web.client.HttpServerErrorException if status is 5xx
   */
  public HttpStatus demandArchiveGeneration(String operationId) {
    final String url = "/operation/{clientId}/tally-archive/demand";

    return restTemplate.execute(url, HttpMethod.PUT, this::addXRequestedWithHeader, ClientHttpResponse::getStatusCode,
                                Collections.singletonMap("clientId", operationId));
  }

  private void addXRequestedWithHeader(ClientHttpRequest request) {
    // Note : this might not be relevant for b2b requests - for now it is mandatory anyway
    request.getHeaders().set("X-Requested-With", "XMLHttpRequest");
  }
}
