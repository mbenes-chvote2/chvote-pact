/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.operation;

import ch.ge.ve.chvote.pact.b2b.client.model.OperationBaseConfiguration;
import ch.ge.ve.chvote.pact.b2b.client.model.Voter;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingSiteConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.Attachment;
import ch.ge.ve.chvote.pactback.service.operation.ProtocolEndpointSupportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLConnection;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A controller for retrieving the resources of a protocol instance.
 */
@RestController
@RequestMapping("/protocol/{protocolId}")
@Api(value = "PACT protocol resources provider",
     tags = "Protocol Instance")
public class ProtocolResourceController {
  private final ProtocolEndpointSupportService protocolEndpointSupportService;

  @Autowired
  public ProtocolResourceController(ProtocolEndpointSupportService protocolEndpointSupportService) {
    this.protocolEndpointSupportService = protocolEndpointSupportService;
  }

  /**
   * Retrieve all the voters associated with the given the protocol resource.
   *
   * @param protocolId the protocol resource identifier.
   *
   * @return the list of {@link Voter}s.
   */
  @GetMapping(value = "/voters", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Voters information",
      notes = "Retrieves the information of all the voters associated with the given protocol id.")
  public List<Voter> getVotersInformation(
      @ApiParam(value = "protocol identifier") @PathVariable("protocolId") String protocolId) {
    return protocolEndpointSupportService.getVotersByProtocolId(protocolId);
  }

  /**
   * Retrieves information about the operation associated with the given protocol id.
   *
   * @param protocolId the protocol resource identifier.
   *
   * @return an object containing information about the operation.
   */
  @GetMapping(value = "/operation", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Operation information",
      notes = "Retrieves information about the operation associated with the given protocol id.")
  @ApiResponses({
      @ApiResponse(code = 404, message = "Protocol instance not found")
  })
  public OperationBaseConfiguration getOperationConfiguration(
      @ApiParam(value = "protocol identifier") @PathVariable("protocolId") String protocolId) {
    return protocolEndpointSupportService.getOperationConfigurationByProtocolId(protocolId);
  }

  /**
   * Retrieves the voting site configuration for the operation denoted by the given protocol id.
   *
   * @param protocolId the protocol resource identifier.
   *
   * @return an object containing the voting site configuration.
   */
  @GetMapping(value = "/voting-site-configuration", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "voting site configuration for the operation denoted by the given protocol id.")
  @ApiResponses({
      @ApiResponse(code = 404, message = "Protocol instance not found")
  })
  public VotingSiteConfiguration getVotingSiteConfiguration(
      @ApiParam(value = "protocol identifier") @PathVariable("protocolId") String protocolId) {
    return protocolEndpointSupportService.getVotingSiteConfigurationByProtocolId(protocolId);
  }

  /**
   * Retrieves an attachment from the database.
   *
   * @param protocolId   the protocol resource identifier.
   * @param attachmentId attachment identifier.
   */
  @GetMapping("/attachment/{attachmentId}")
  @ApiOperation(value = "Retrieves an attachment from the database")
  @ApiResponses({
      @ApiResponse(code = 404, message = "Attachment not found")
  })
  public void getAttachment(HttpServletResponse response,
                            @ApiParam(value = "protocol identifier") @PathVariable("protocolId") String protocolId,
                            @ApiParam(value = "attachment identifier") @PathVariable("attachmentId") Long attachmentId)
      throws IOException {

    Attachment attachment =
        protocolEndpointSupportService.getAttachmentByProtocolIdAndAttachmentId(protocolId, attachmentId);
    response.setContentType(URLConnection.guessContentTypeFromName(attachment.getName()));
    response.setHeader("Content-Disposition", "attachment; filename=\"" + attachment.getName() + "\"");
    try (OutputStream os = response.getOutputStream()) {
      os.write(attachment.getFile());
    }
  }

}
