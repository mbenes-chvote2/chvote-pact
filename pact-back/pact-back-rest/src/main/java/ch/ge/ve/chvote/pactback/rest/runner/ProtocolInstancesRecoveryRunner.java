/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.runner;

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInitializationService;
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * This runner retrieves all suspicious {@code ProtocolInstance}s and continues the protocol process with them.
 * <p>
 * It is possible to disable this mechanism by setting the property "chvote.pact.node.onstart-recovery" to "false".
 * By default (ie if the property is not defined) the runner will be activated on start.
 * </p>
 *
 * @see ProtocolInitializationService#continueProtocol(ProtocolInstance)
 */
@Component
@ConditionalOnProperty(name = "chvote.pact.node.onstart-recovery", havingValue = "true", matchIfMissing = true)
public class ProtocolInstancesRecoveryRunner implements ApplicationRunner {

  private static final Logger logger = LoggerFactory.getLogger(ProtocolInstancesRecoveryRunner.class);

  private final ProtocolInstanceService       protocolInstanceService;
  private final ProtocolInitializationService initializationService;

  @Autowired
  public ProtocolInstancesRecoveryRunner(ProtocolInstanceService protocolInstanceService,
                                         ProtocolInitializationService initializationService) {
    this.protocolInstanceService = protocolInstanceService;
    this.initializationService = initializationService;
  }

  @Override
  public void run(ApplicationArguments args) {
    logger.debug("Run {} with nodeRepartitionKey = {}", getClass(), protocolInstanceService.getNodeRepartitionKey());
    List<ProtocolInstance> protocolInstances = protocolInstanceService.getProtocolInstancesStuckInGeneration();

    if (!protocolInstances.isEmpty()) {
      logger.info("Found {} ProtocolInstances that seem to have been stuck during the credentials generation phase",
                  protocolInstances.size());

      protocolInstances.forEach(this::recoverCredentialsGeneration);
    }
  }

  private void recoverCredentialsGeneration(ProtocolInstance protocolInstance) {
    logger.info("ProtocolInstance #{} recovery from status {}", protocolInstance.getId(), protocolInstance.getStatus());
    initializationService.continueProtocol(protocolInstance);
  }
}
