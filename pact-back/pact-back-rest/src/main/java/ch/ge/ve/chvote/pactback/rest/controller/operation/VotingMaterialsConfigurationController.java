/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.operation;

import ch.ge.ve.chvote.pactback.contract.operation.VotingMaterialsConfigurationSubmissionVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotersCreationStatisticsVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo;
import ch.ge.ve.chvote.pactback.service.action.VotingMaterialsCreationService;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.operation.OperationService;
import ch.ge.ve.chvote.pactback.service.operation.materials.VotingMaterialsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * A specialised controller for interacting with the Voting materials configuration of an Operation.
 */
@RestController
@RequestMapping("/operation")
@Api(
    value = "Voting material",
    tags = "Voting material")
public class VotingMaterialsConfigurationController {

  private final VotingMaterialsService service;
  private final OperationService       operationService;
  private final ObjectMapper           objectMapper;

  /**
   * Creates a new {@link VotingMaterialsConfigurationController} with the given service.
   *
   * @param operationService the {@link OperationService} service
   * @param service          the {@link VotingMaterialsCreationService} service
   * @param objectMapper     the {@link ObjectMapper} to be used
   */
  @Autowired
  public VotingMaterialsConfigurationController(
      OperationService operationService,
      VotingMaterialsService service,
      ObjectMapper objectMapper) {
    this.operationService = operationService;
    this.service = service;
    this.objectMapper = objectMapper;
  }

  /**
   * Creates the Voting material configuration for an Operation.
   *
   * @param clientId      the identifier of the operation in the client's system
   * @param files         the ZIP files (registers and printers) of the voting materials
   * @param configuration the voting materials metadata described by {@link VotingMaterialsConfigurationSubmissionVo}
   *
   * @throws IOException if there was a problem uploading the file.
   */
  @PostMapping(
      value = "{clientId}/voting-materials/configuration",
      consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Creates the voting material configuration of an operation",
      notes = "should send  ZIP files (registers and printers) of the voting materials in parts named data")
  @ApiImplicitParams(
      {
          @ApiImplicitParam(
              required = true, name = "configuration",
              value = "Voting material configuration  metadata as a json string in a separate part",
              dataType = "ch.ge.ve.chvote.pactback.contract.operation.VotingMaterialsConfigurationSubmissionVo",
              paramType = "body")
      })
  public void submitVotingMaterialsConfiguration(
      @ApiParam(value = "Identifier of the operation in the client's system")
      @PathVariable("clientId") String clientId,
      @ApiParam(hidden = true)
      @RequestPart("data") MultipartFile[] files,
      @ApiParam(hidden = true)
      @RequestPart("configuration") String configuration)
      throws IOException {
    VotingMaterialsConfigurationSubmissionVo submissionVo =
        objectMapper.readValue(configuration, VotingMaterialsConfigurationSubmissionVo.class);
    operationService.addVotingMaterialsConfiguration(clientId, submissionVo, Arrays.asList(files));
  }

  /**
   * Validate voting material after it has been generated
   *
   * @param clientId the operation identifier in the client's system
   * @param user     User that validate voting material
   */
  @PostMapping("{clientId}/voting-materials/validate")
  @ApiOperation(
      value = "Validate the voting material creation by client id",
      notes = "Set the current voting material configuration for this operation client id to validated")
  @ApiResponses({
      @ApiResponse(code = 404, message = "Voting material configuration not found"),
      @ApiResponse(code = 409, message = "The configuration does not need validation")
  })
  public void validateVotingMaterialsByClientId(
      @ApiParam("client ID of the operation")
      @PathVariable("clientId") String clientId,
      @RequestParam("user") String user) {
    service.validateConfigurationByClientId(clientId, user);
  }

  /**
   * Retrieves the status of the voting materials of the deployed configuration.
   *
   * @param clientId the operation identifier in the client's system
   *
   * @return an {@link VotingMaterialsStatusVo} for the matching operation.
   *
   * @throws EntityNotFoundException if either the operation or the operation's voting materials configuration was not
   *                                 found
   */
  @GetMapping(value = "{clientId}/voting-materials/status", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Get voting materials status by operation client id",
      notes = "Retrieve current voting materials status by operation client id.")
  @ApiResponses({
      @ApiResponse(code = 404, message = "Voting material configuration not found")
  })
  public VotingMaterialsStatusVo getVotingMaterialsStatusByClientId(
      @ApiParam(value = "Operation client identifier")
      @PathVariable("clientId") String clientId) {
    return service.getVotingMaterialsStatusByClientId(clientId);
  }

  /**
   * Retrieves the statistics for the deployed configuration : the number of voters created by printer
   * and counting circle.
   *
   * @param clientId the operation identifier in the client's system
   *
   * @return the list of statistic objects for the deployed configuration of this operation
   *
   * @throws EntityNotFoundException if no operation matches the clientId, or it has no associated statistics
   */
  @GetMapping(value = "{clientId}/voting-materials/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Get statistics about created voting materials",
      notes = "Retrieves the statistics for the deployed configuration of an operation")
  @ApiResponses({
      @ApiResponse(code = 404, message = "Voting material configuration statistics not found")
  })
  public List<VotersCreationStatisticsVo> getStatisticsByClientId(
      @ApiParam(value = "Operation client identifier")
      @PathVariable("clientId") String clientId) {

    return service.getDeployedMaterialsCreationStatistics(clientId);
  }

}
