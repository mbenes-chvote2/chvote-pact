/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.actuator;

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import java.time.LocalDateTime;

/**
 * A DTO used to expose tasks in the {@link ResultsTasksEndpoint}.
 */
public final class TaskDTO {

  private final String              operationId;
  private final ProtocolEnvironment protocolEnvironment;
  private final String              targetMode;
  private final StatusDTO           status;

  public TaskDTO(String operationId, ProtocolEnvironment protocolEnvironment, String targetMode,
                 ProtocolInstanceStatus status, LocalDateTime statusDate) {
    this.operationId = operationId;
    this.protocolEnvironment = protocolEnvironment;
    this.targetMode = targetMode;
    this.status = new StatusDTO(status, statusDate);
  }

  public String getOperationId() {
    return operationId;
  }

  public ProtocolEnvironment getProtocolEnvironment() {
    return protocolEnvironment;
  }

  public String getTargetMode() {
    return targetMode;
  }

  public StatusDTO getStatus() {
    return status;
  }

  /** Embedded DTO for the pair "status - date" */
  public static final class StatusDTO {

    private final ProtocolInstanceStatus value;
    private final LocalDateTime          since;

    private StatusDTO(ProtocolInstanceStatus value, LocalDateTime since) {
      this.value = value;
      this.since = since;
    }

    public ProtocolInstanceStatus getValue() {
      return value;
    }

    public LocalDateTime getSince() {
      return since;
    }
  }

}
