/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller;

import ch.ge.ve.chvote.pactback.rest.controller.operation.exception.InvalidOperationConfigurationException;
import ch.ge.ve.chvote.pactback.service.exception.CannotCreateOrUpdateVotingPeriodConfigurationException;
import ch.ge.ve.chvote.pactback.service.exception.CannotDeployOperationConfigurationException;
import ch.ge.ve.chvote.pactback.service.exception.CannotUpdateVotingMaterialConfigurationException;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.exception.IllegalTransitionException;
import ch.ge.ve.chvote.pactback.service.exception.InvalidVotingMaterialsConfigurationException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyExistsException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionNotFoundException;
import ch.ge.ve.chvote.pactback.service.exception.SelfActionResolutionException;
import ch.ge.ve.chvote.pactback.service.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A controller advice containing common exception to HTTP status mapping functionality.
 */
@ControllerAdvice
public class ExceptionHandlerController {

  private static final Logger logger = LoggerFactory.getLogger(ExceptionHandlerController.class);

  /**
   * Captures and sets HTTP Status to {@link HttpStatus#NOT_FOUND} for all suitable exceptions.
   *
   * @param ex the exception being handled
   *
   * @see PrivilegedActionNotFoundException
   * @see EntityNotFoundException
   */
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ExceptionHandler({
                        PrivilegedActionNotFoundException.class,
                        EntityNotFoundException.class
                    })
  public void notFound(RuntimeException ex) {
    logBusinessException(ex);
  }

  /**
   * Captures and sets HTTP Status to {@link HttpStatus#FORBIDDEN} for all suitable exceptions.
   *
   * @param ex the exception being handled
   *
   * @see PrivilegedActionAlreadyProcessedException
   * @see SelfActionResolutionException
   * @see SecurityException
   */
  @ResponseStatus(value = HttpStatus.FORBIDDEN)
  @ExceptionHandler({
                        PrivilegedActionAlreadyProcessedException.class,
                        SelfActionResolutionException.class,
                        SecurityException.class})
  public void forbidden(RuntimeException ex) {
    logBusinessException(ex);
  }

  /**
   * Captures and sets HTTP Status to {@link HttpStatus#BAD_REQUEST} for all suitable exceptions.
   *
   * @param ex the exception being handled
   *
   * @see IllegalTransitionException
   * @see UserNotFoundException
   * @see InvalidVotingMaterialsConfigurationException
   * @see InvalidOperationConfigurationException
   */
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ExceptionHandler({
                        IllegalTransitionException.class,
                        UserNotFoundException.class,
                        InvalidVotingMaterialsConfigurationException.class,
                        InvalidOperationConfigurationException.class})
  public void badRequest(RuntimeException ex) {
    logBusinessException(ex);
  }

  /**
   * Captures and sets HTTP Status to {@link HttpStatus#CONFLICT} for all suitable exceptions.
   *
   * @param ex the exception being handled
   *
   * @see PrivilegedActionAlreadyExistsException
   * @see CannotDeployOperationConfigurationException
   * @see CannotUpdateVotingMaterialConfigurationException
   */
  @ResponseStatus(value = HttpStatus.CONFLICT)
  @ExceptionHandler({
                        PrivilegedActionAlreadyExistsException.class,
                        CannotDeployOperationConfigurationException.class,
                        CannotUpdateVotingMaterialConfigurationException.class,
                        CannotCreateOrUpdateVotingPeriodConfigurationException.class})
  public void conflict(RuntimeException ex) {
    logBusinessException(ex);
  }

  /**
   * Captures and sets HTTP Status to {@link HttpStatus#INTERNAL_SERVER_ERROR} for all undefined exceptions.
   *
   * @param ex the exception being handled
   */
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(Exception.class)
  public void serverError(Exception ex) {
    logBusinessException(ex);
  }

  private void logBusinessException(Exception ex) {
    logger.error("Caught operation exception", ex);
  }
}
