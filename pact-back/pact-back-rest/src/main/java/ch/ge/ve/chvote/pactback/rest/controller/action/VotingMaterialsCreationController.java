/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.action;

import ch.ge.ve.chvote.pactback.service.action.VotingMaterialsCreationService;
import ch.ge.ve.chvote.pactback.service.action.vo.VotingMaterialsCreationVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A specialised {@link ActionCreationController} for accessing {@link VotingMaterialsCreationVo}.
 */
@RestController
@RequestMapping("/privileged-actions/voting-materials-creation")
@Api(
    value = "Voting materials creation",
    tags = "Voting materials creation")
public class VotingMaterialsCreationController extends ActionCreationController<VotingMaterialsCreationVo> {

  /**
   * Creates a new {@link VotingMaterialsCreationController}s.
   *
   * @param service the {@link VotingMaterialsCreationVo} service.
   */
  @Autowired
  public VotingMaterialsCreationController(VotingMaterialsCreationService service) {
    super(service);
  }

}
