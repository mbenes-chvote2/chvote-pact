/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.actuator

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.rest.MockedRabbitConfiguration
import ch.ge.ve.chvote.pactback.rest.RestEndpointTest
import ch.ge.ve.chvote.pactback.ManagementEndPointConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.MockMvc
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification

@Transactional
@RestEndpointTest(context = [MockedRabbitConfiguration, ManagementEndPointConfiguration])
class DeadLetterEventEndpointTest extends Specification {

  @Autowired
  MockMvc mvc

  @Sql(scripts = "classpath:sql/dead-letter-events.sql")
  def "should retrieve all dead letter events"() {
    when:
    def result = mvc.perform(get("/dlx/events").with(httpBasic("user1", "password")))

    then:
    result.andExpect(status().isOk()).andExpect(jsonPath('$.length()').value('2'))
  }

  @Sql(scripts = "classpath:sql/dead-letter-events.sql")
  def "should retrieve an event from its id"() {
    when:
    def result = mvc.perform(get("/dlx/events/1").with(httpBasic("user1", "password")))

    then:
    result.andExpect(status().isOk()).andExpect(jsonPath('$.id').value('1'))
  }

  def "should return 404 when an event is not found"() {
    when:
    def result = mvc.perform(get("/dlx/events/1").with(httpBasic("user1", "password")))

    then:
    result.andExpect(status().isNotFound())
  }

  @Sql(scripts = "classpath:sql/dead-letter-events.sql")
  def "should delete an event after having republished it"() {
    when:
    def result = mvc.perform(post("/dlx/events/1/publications").with(httpBasic("user1", "password")))

    then:
    result.andExpect(status().isOk())

    and:
    mvc.perform(get("/dlx/events").with(httpBasic("user1", "password")))
        .andExpect(status().isOk()).andExpect(jsonPath('$.length()').value('1'))
  }
}
