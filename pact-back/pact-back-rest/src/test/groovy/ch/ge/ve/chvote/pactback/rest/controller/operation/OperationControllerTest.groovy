/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.operation

import static ch.ge.ve.chvote.pactback.rest.TestUtils.XML_HTTP_REQUEST
import static ch.ge.ve.chvote.pactback.rest.TestUtils.X_REQUESTED_WITH
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.B2bEndPointConfiguration
import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo
import ch.ge.ve.chvote.pactback.contract.operation.BallotDocumentationVo
import ch.ge.ve.chvote.pactback.contract.operation.ElectionSiteConfigurationVo
import ch.ge.ve.chvote.pactback.contract.operation.HighlightedQuestionsVo
import ch.ge.ve.chvote.pactback.contract.operation.MunicipalityVo
import ch.ge.ve.chvote.pactback.contract.operation.OperationConfigurationSubmissionVo
import ch.ge.ve.chvote.pactback.contract.operation.PrinterConfigurationVo
import ch.ge.ve.chvote.pactback.contract.operation.TestSitePrinter
import ch.ge.ve.chvote.pactback.fixtures.PrintersPublicKeys
import ch.ge.ve.chvote.pactback.mock.server.OperationsCreationService
import ch.ge.ve.chvote.pactback.rest.H2DatabaseCleaner
import ch.ge.ve.chvote.pactback.rest.MockedRabbitConfiguration
import ch.ge.ve.chvote.pactback.rest.RestEndpointTest
import ch.ge.ve.chvote.pactback.rest.spring.AfterLastTest
import ch.ge.ve.chvote.pactback.rest.spring.BeforeFirstTest
import ch.ge.ve.chvote.pactback.rest.spring.ContextConfigTestExecutionListener
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.io.ByteStreams
import java.time.LocalDateTime
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import javax.transaction.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.lang.Stepwise

@RestEndpointTest(context = [MockedRabbitConfiguration, B2bEndPointConfiguration])
@TestExecutionListeners(listeners = ContextConfigTestExecutionListener, mergeMode = MERGE_WITH_DEFAULTS)
@Stepwise
class OperationControllerTest extends Specification {

  @Autowired
  MockMvc mvc

  @Autowired
  ObjectMapper objectMapper

  @BeforeFirstTest
  static void prepareOperations(OperationsCreationService service) {
    service.createAllOperations()
  }

  @AfterLastTest
  static void restoreDatabase(H2DatabaseCleaner cleaner) {
    cleaner.cleanupDatabase()
  }

  def "should retrieve an operation status VO for operations in test given the clientId of an operation"() {
    when:
    def operationStatus = mvc.perform(
            get("/operation/" + clientId + "/configuration/status")
                    .with(httpBasic("user1", "password"))
                    .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    operationStatus
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.inTest.state').value(expectedStatus))
            .andExpect(jsonPath('$.inTest.comment').value(expectedComment))

    where:
    clientId || expectedStatus         || expectedComment
    "1"      || "VALIDATED"            || null
    "3"      || "DEPLOYMENT_REQUESTED" || null
    "8"      || "DEPLOYMENT_REFUSED"   || "Bart Simpson is not a real person"
  }

  def "should retrieve an operation status VO for deployed operations given the clientId of an operation"() {
    when:
    def operationStatus = mvc.perform(
            get("/operation/5/configuration/status")
                    .with(httpBasic("user1", "password"))
                    .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    operationStatus
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.deployed.lastChangeUser').value("user3"))
  }

  def "should fail to retrieve a operation report VO, given a wrong clientId of an operation"() {
    given:
    def clientId = "SillyOperation"

    when:
    def operationStatus = mvc.perform(
            get("/operation/" + clientId + "/configuration/status")
                    .with(httpBasic("user1", "password"))
                    .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    operationStatus
            .andExpect(status().isNotFound())
  }

  def "should create an operation"() {
    given:
    def doiFile = new MockMultipartFile("data", "doi.zip", null,
            zip("doi.xml",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/doi.xml")))
    def doiEntry = new AttachmentFileEntryVo()
    doiEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOMAIN_OF_INFLUENCE)
    doiEntry.setImportDateTime(LocalDateTime.now())
    doiEntry.setZipFileName("doi.zip")

    def faqDEFile = new MockMultipartFile("data", "op_doc_faq.de.zip", null,
            zip("op_doc_faq.de.pdf",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/op_doc_faq.de.pdf")))
    def faqDEEntry = new AttachmentFileEntryVo()
    faqDEEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOCUMENT_FAQ)
    faqDEEntry.setLanguage("DE")
    faqDEEntry.setImportDateTime(LocalDateTime.now())
    faqDEEntry.setZipFileName("op_doc_faq.de.zip")

    def faqFRFile = new MockMultipartFile("data", "op_doc_faq.fr.zip", null,
            zip("op_doc_faq.fr.pdf",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/op_doc_faq.fr.pdf")))
    def faqFREntry = new AttachmentFileEntryVo()
    faqFREntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.DOCUMENT_FAQ)
    faqFREntry.setLanguage("FR")
    faqFREntry.setImportDateTime(LocalDateTime.now())
    faqFREntry.setZipFileName("op_doc_faq.fr.zip")

    def orepFile = new MockMultipartFile("data", "orep.zip", null,
            zip("orep.xml",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/orep.xml")))
    def orepEntry = new AttachmentFileEntryVo()
    orepEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REPOSITORY_VOTATION)
    orepEntry.setImportDateTime(LocalDateTime.now())
    orepEntry.setZipFileName("orep.zip")

    def orepFile2 = new MockMultipartFile("data", "orep2.zip", null,
            zip("orep.xml",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/orep.xml")))
    def orepEntry2 = new AttachmentFileEntryVo()
    orepEntry2.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REPOSITORY_ELECTION)
    orepEntry2.setImportDateTime(LocalDateTime.now())
    orepEntry2.setZipFileName("orep2.zip")

    def registerFile = new MockMultipartFile("data", "register.zip", null,
            zip("register.xml",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/register.xml")))
    def registerEntry = new AttachmentFileEntryVo()
    registerEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REGISTER)
    registerEntry.setImportDateTime(LocalDateTime.now())
    registerEntry.setZipFileName("register.zip")

    def translations = new MockMultipartFile("data", "translations.zip", null,
            zip("translations.json",
                    this.class.getClassLoader().getResourceAsStream("attachments/operation/translations.json")))
    def translationsEntry = new AttachmentFileEntryVo()
    translationsEntry.setAttachmentType(AttachmentFileEntryVo.AttachmentType.TRANSLATIONS)
    translationsEntry.setImportDateTime(LocalDateTime.now())
    translationsEntry.setZipFileName("translations.zip")

    def ballotDocFile = new MockMultipartFile("data", "ballotDoc.zip", null,
            zip("ballodDoc.pdf", this.class.getClassLoader().getResourceAsStream("attachments/operation/simple.pdf")))
    def ballotDocEntry = new AttachmentFileEntryVo()
    ballotDocEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.BALLOT_DOCUMENTATION
    ballotDocEntry.importDateTime = LocalDateTime.now()
    ballotDocEntry.zipFileName = "ballotDoc.zip"
    def ballotDocumentation = new BallotDocumentationVo()
    ballotDocumentation.ballot = "ballot"
    ballotDocumentation.label = "label"
    ballotDocumentation.documentation = ballotDocEntry

    def highlightedQuestionFile = new MockMultipartFile("data", "highlightedQuestion.zip", null,
            zip("highlightedQuestion.pdf", this.class.getClassLoader().getResourceAsStream("attachments/operation/simple.pdf")))
    def highlightedQuestionEntry = new AttachmentFileEntryVo()
    highlightedQuestionEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.HIGHLIGHTED_QUESTION
    highlightedQuestionEntry.importDateTime = LocalDateTime.now()
    highlightedQuestionEntry.zipFileName = "highlightedQuestion.zip"
    def highlightedQuestion = new HighlightedQuestionsVo()
    highlightedQuestion.answerFile = highlightedQuestionEntry
    highlightedQuestion.question = "Question ?"

    def electionSiteConfiguration = new ElectionSiteConfigurationVo()
    electionSiteConfiguration.ballot = "ballot"
    electionSiteConfiguration.candidateInformationDisplayModel = "model"
    electionSiteConfiguration.displayedColumnsOnVerificationTable = ["col", "col"]
    electionSiteConfiguration.columnsOrderOnVerificationTable = ["col"]
    electionSiteConfiguration.displayEmptyPosition = true

    def vo = new OperationConfigurationSubmissionVo()
    vo.user = "Fred"
    vo.gracePeriod = 50
    vo.canton = "GE"
    vo.groupVotation = true
    vo.operationName = "201706VP"
    vo.operationLabel = "201706VP"
    vo.operationDate = LocalDateTime.MAX
    vo.clientId = "1000"
    vo.milestones = [:]
    vo.testSitePrinter = new TestSitePrinter()
    vo.testSitePrinter.municipality = new MunicipalityVo()
    vo.testSitePrinter.municipality.ofsId = 6601
    vo.testSitePrinter.municipality.name = "Municipality"
    vo.testSitePrinter.printer = new PrinterConfigurationVo()
    vo.testSitePrinter.printer.id = "printer-0"
    vo.testSitePrinter.printer.name = "Printer 0"
    vo.testSitePrinter.printer.publicKey = PrintersPublicKeys.publicKeyPa0()
    vo.milestones[OperationConfigurationSubmissionVo.Milestone.SITE_OPEN] = LocalDateTime.MAX
    vo.milestones[OperationConfigurationSubmissionVo.Milestone.SITE_CLOSE] = LocalDateTime.MAX
    vo.attachments = Arrays.asList(doiEntry, faqDEEntry, faqFREntry, orepEntry, registerEntry,
            highlightedQuestionEntry, ballotDocEntry, orepEntry2, translationsEntry)

    vo.ballotDocumentations = [ballotDocumentation]
    vo.highlightedQuestions = [highlightedQuestion]
    vo.electionSiteConfigurations = [electionSiteConfiguration]

    def operation = new MockMultipartFile("operation", "", "application/json",
            objectMapper.writeValueAsBytes(vo))

    when:
    def resultActions = mvc.perform(fileUpload("/operation/1000/configuration")
            .file(operation).file(doiFile).file(faqDEFile).file(faqFRFile).file(orepFile).file(registerFile)
            .file(orepFile2).file(translations).file(ballotDocFile).file(highlightedQuestionFile)
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isOk())
  }

  def "should fail to create an operation if there are missing fields"() {
    given:

    def vo = new OperationConfigurationSubmissionVo()
    vo.user = "Fred"
    vo.gracePeriod = 500
    vo.canton = "GE "
    vo.groupVotation = true
    vo.operationName = "201706VP"
    vo.operationDate = LocalDateTime.MAX
    vo.milestones = [:]
    vo.testSitePrinter = new TestSitePrinter()
    vo.testSitePrinter.municipality = new MunicipalityVo()
    vo.testSitePrinter.municipality.ofsId = 6601
    vo.testSitePrinter.municipality.name = "Municipality 6601"
    vo.testSitePrinter.printer = new PrinterConfigurationVo()
    vo.testSitePrinter.printer.id = "printer-0"
    vo.testSitePrinter.printer.name = "Printer 0"
    vo.testSitePrinter.printer.publicKey = PrintersPublicKeys.publicKeyPa0()
    vo.milestones[OperationConfigurationSubmissionVo.Milestone.SITE_CLOSE] = LocalDateTime.MAX
    vo.attachments = Arrays.asList()
    def operation = new MockMultipartFile("operation", "", "application/json",
            objectMapper.writeValueAsBytes(vo))

    when:
    def resultActions = mvc.perform(fileUpload("/operation/1000/configuration")
            .file(operation)
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isBadRequest())
  }

  @Transactional
  def "should invalidate an operation configuration"() {
    when:
    def resultActions = mvc.perform(post("/operation/7/configuration/invalidate")
            .param("user", "johndoe")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then: "the call is successful"
    resultActions.andExpect(status().isOk())

    and: "the status is modified as expected"
    mvc.perform(get("/operation/7/configuration/status")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))
            .andExpect(jsonPath('$.inTest.state').value("INVALIDATED"))
  }

  @Transactional
  def "should validate an operation configuration"() {
    when:
    def resultActions = mvc.perform(post("/operation/7/configuration/validate")
            .param("user", "johndoe")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then: "the call is successful"
    resultActions.andExpect(status().isOk())

    and: "the status is modified as expected"
    mvc.perform(get("/operation/7/configuration/status")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))
            .andExpect(jsonPath('$.inTest.state').value("VALIDATED"))
  }

  static zip(String filename, InputStream input) {
    def bInput = ByteStreams.toByteArray(input)
    def baos = new ByteArrayOutputStream()
    def zos = new ZipOutputStream(baos)
    def entry = new ZipEntry(filename)

    entry.setSize(bInput.length)
    zos.putNextEntry(entry)
    zos.write(bInput)
    zos.closeEntry()
    zos.close()

    return new ByteArrayInputStream(baos.toByteArray())
  }

}
