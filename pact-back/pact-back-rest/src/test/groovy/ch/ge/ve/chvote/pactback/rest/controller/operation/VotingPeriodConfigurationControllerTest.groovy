/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.operation

import static ch.ge.ve.chvote.pactback.rest.TestUtils.XML_HTTP_REQUEST
import static ch.ge.ve.chvote.pactback.rest.TestUtils.X_REQUESTED_WITH
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.B2bEndPointConfiguration
import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo
import ch.ge.ve.chvote.pactback.contract.operation.VotingPeriodConfigurationSubmissionVo
import ch.ge.ve.chvote.pactback.mock.server.OperationsCreationService
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.rest.H2DatabaseCleaner
import ch.ge.ve.chvote.pactback.rest.MockedRabbitConfiguration
import ch.ge.ve.chvote.pactback.rest.RestEndpointTest
import ch.ge.ve.chvote.pactback.rest.spring.AfterLastTest
import ch.ge.ve.chvote.pactback.rest.spring.ContextConfigTestExecutionListener
import com.fasterxml.jackson.databind.ObjectMapper
import java.time.LocalDateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

@RestEndpointTest(context = [MockedRabbitConfiguration, B2bEndPointConfiguration])
@TestExecutionListeners(listeners = ContextConfigTestExecutionListener, mergeMode = MERGE_WITH_DEFAULTS)
class VotingPeriodConfigurationControllerTest extends Specification {

  @Autowired
  MockMvc mvc

  @Autowired
  OperationsCreationService operationsCreationService

  @Autowired
  ObjectMapper objectMapper

  @AfterLastTest
  static void restoreDatabase(H2DatabaseCleaner cleaner) {
    cleaner.cleanupDatabase()
  }

  def "should create a voting period configuration"() {
    given:
    def id = operationsCreationService.testCase
            {
              it.withConfigurationDeployed()
                      .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                      .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
                      .validateVotingMaterial()
            }


    def vo = new VotingPeriodConfigurationSubmissionVo()
    vo.user = "some user"
    vo.gracePeriod = 10
    vo.siteOpeningDate = LocalDateTime.of(2010, 1, 1, 10, 0)
    vo.siteClosingDate = LocalDateTime.of(2010, 1, 5, 11, 0)
    vo.target = ch.ge.ve.chvote.pactback.contract.operation.DeploymentTarget.SIMULATION
    vo.simulationName = "simulation-1"

    AttachmentFileEntryVo attachmentEntryVo = new AttachmentFileEntryVo()
    attachmentEntryVo.setImportDateTime(LocalDateTime.now())
    vo.electoralAuthorityKey = attachmentEntryVo

    def configuration = new MockMultipartFile("configuration", "", "application/json",
            objectMapper.writeValueAsBytes(vo))

    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    def publicKey = new MockMultipartFile("data", "publicKey.txt", null, is)

    when:
    def resultActions = mvc.perform(multipart("/operation/${id.clientId}/voting-period/configuration")
            .file(configuration)
            .file(publicKey)
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST)
    )

    then:
    resultActions.andExpect(status().isOk())
  }
}
