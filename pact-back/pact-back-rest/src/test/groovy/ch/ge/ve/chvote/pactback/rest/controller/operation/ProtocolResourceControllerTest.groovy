/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.operation

import static ch.ge.ve.chvote.pactback.rest.TestUtils.XML_HTTP_REQUEST
import static ch.ge.ve.chvote.pactback.rest.TestUtils.X_REQUESTED_WITH
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.B2bEndPointConfiguration
import ch.ge.ve.chvote.pactback.mock.server.IdHolder
import ch.ge.ve.chvote.pactback.mock.server.OperationsCreationService
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.rest.H2DatabaseCleaner
import ch.ge.ve.chvote.pactback.rest.MockedRabbitConfiguration
import ch.ge.ve.chvote.pactback.rest.RestEndpointTest
import ch.ge.ve.chvote.pactback.rest.spring.AfterLastTest
import ch.ge.ve.chvote.pactback.rest.spring.ContextConfigTestExecutionListener
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.lang.Stepwise

@RestEndpointTest(context = [MockedRabbitConfiguration, B2bEndPointConfiguration])
@TestExecutionListeners(listeners = ContextConfigTestExecutionListener, mergeMode = MERGE_WITH_DEFAULTS)
@Stepwise
class ProtocolResourceControllerTest extends Specification {

  @Autowired
  MockMvc mvc

  @Autowired
  OperationsCreationService operationsCreationService

  @Autowired
  ObjectMapper objectMapper

  @AfterLastTest
  static void restoreDatabase(H2DatabaseCleaner cleaner) {
    cleaner.cleanupDatabase()
  }


  def "getAttachment should retrieve operation files"() {
    given:
    def operation = operationsCreationService.testCaseWithOperation({
      builder ->
        builder.withConfigurationDeployed()
                .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
                .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
    })
    def ids = new IdHolder(operation);



    when:
    def operationStatus = mvc.perform(
            get("/protocol/$ids.protocolId/attachment/${operation.fetchDeployedConfiguration().highlightedQuestions[0].id}")
                    .with(httpBasic("user1", "password"))
                    .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    operationStatus
            .andExpect(status().isOk())
            .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, 'attachment; filename="question.pdf"'))
            .andExpect(header().string(HttpHeaders.CONTENT_TYPE, 'application/pdf'))
  }

  def "getVotingSiteConfiguration should retrieve voting site relative information"() {
    given:
    def ids = operationsCreationService.testCase({
      it.withConfigurationDeployed()
              .withVotingMaterialConfiguration(DeploymentTarget.SIMULATION)
              .acceptVotingMaterialCreation(ProtocolInstanceStatus.CREDENTIALS_GENERATED)
              .validateVotingMaterial()
    })

    when:
    def operationStatus = mvc.perform(
            get("/protocol/${ids.protocolId}/voting-site-configuration")
                    .with(httpBasic("user1", "password"))
                    .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    operationStatus
            .andExpect(status().isOk())

            .andExpect(jsonPath('$.documentations[*]')
            .value(Matchers.hasSize(3)))

            .andExpect(jsonPath('$.votationBallotConfigList[0].ballotDocumentations[0].label')
            .value("votation ballot doc"))

  }


}
