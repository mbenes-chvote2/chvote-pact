/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.spring;

import com.google.common.collect.ImmutableSet;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.Set;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListener;
import org.springframework.util.ReflectionUtils;

/**
 * A {@link TestExecutionListener} that enhances the behaviour of {@code setupSpec} and {@code cleanupSpec}
 * static test classes wrapper methods.
 * <p>
 *   It is not straightforward in JUnit / Spock test classes lifecycle to take advantage of the Spring context
 *   and its registered beans to initialize / clean a test class ("feature" in Spock's language). <br/>
 *
 *   This listener aims at leveraging this "weakness", by detecting <code>@BeforeFirstTest</code> and
 *   <code>@AfterLastTest</code> annotated <strong>static methods</strong>, as BeforeClass / AfterClass
 *   replacements, and provide them with parameters requested from Spring context.
 * </p>
 * <p>
 *   Example :
 *   <pre>
 *     &#64;SpringBootTest
 *     &#64;TestExecutionListeners(listeners = ContextConfigTestExecutionListener, mergeMode = MERGE_WITH_DEFAULTS)
 *     public class SomeTest {
 *
 *       &#64;BeforeFirstTest
 *       public static void prepareTestsEnv(MyService service) {
 *         service.doSomething();  // service will be fetched in Spring context
 *       }
 *
 *       &#64;AfterLastTest
 *       public static void closeTestEnv(MyService service) {
 *         service.tearDown();     // service will be fetched in Spring context
 *       }
 *
 *     }
 *   </pre>
 * </p>
 */
public class ContextConfigTestExecutionListener implements TestExecutionListener {

  private static final String KEY = ContextConfigTestExecutionListener.class.getName() + ".TEST_INIT";

  @Override
  public void prepareTestInstance(TestContext testContext) {
    final Object attribute = testContext.getAttribute(KEY);
    if (attribute == null) {
      executeMethods(testContext, BeforeFirstTest.class);
      testContext.setAttribute(KEY, Boolean.TRUE);
    }
  }

  private void executeMethods(TestContext testContext, Class<? extends Annotation> annotationClass) {
    ReflectionUtils.doWithMethods(testContext.getTestClass(),
                                  m -> doExecute(m, testContext),
                                  m -> isAnnotatedStaticMethod(m, annotationClass));
  }

  private void doExecute(Method method, TestContext testContext) {
    final ApplicationContext applicationContext = testContext.getApplicationContext();
    if (method.getParameterCount() == 0) {
      ReflectionUtils.invokeMethod(method, null); // null cause static method
    } else {
      final Object[] parameters = Stream.of(method.getParameters())
                                        .map(p -> findBean(p, method, applicationContext))
                                        .toArray();

      ReflectionUtils.invokeMethod(method, null, parameters); // null cause static method
    }
  }

  private static Object findBean(Parameter param, Method method, ApplicationContext applicationContext) {
    final Set<String> namesForType = ImmutableSet.copyOf(applicationContext.getBeanNamesForType(param.getType()));
    if (namesForType.isEmpty()) {
      throw noBeanFoundException(param.getType(), method);
    }
    else if (namesForType.size() == 1) {
      return applicationContext.getBean(param.getType());
    }
    else if (param.isNamePresent() && namesForType.contains(param.getName())) {
      return applicationContext.getBean(param.getName(), param.getType());
    } else {
      Qualifier qualifier = param.getAnnotation(Qualifier.class);
      if (qualifier != null) {
        return applicationContext.getBean(qualifier.value(), param.getType());
      }
      throw noBeanFoundException(param.getType(), method);
    }
  }

  private static IllegalStateException noBeanFoundException(Class beanType, Method method) {
    return new IllegalStateException(String.format("No bean found for type %s for method %s",
                                                   beanType.getName(), method.getName()));
  }

  private static boolean isAnnotatedStaticMethod(Method method, Class<? extends Annotation> annotationType) {
    final int mods = method.getModifiers();
    return Modifier.isStatic(mods) && method.isAnnotationPresent(annotationType);
  }

  @Override
  public void afterTestClass(TestContext testContext) {
    executeMethods(testContext, AfterLastTest.class);
  }
}
