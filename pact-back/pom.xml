<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ chvote-pact
  ~ %%
  ~ Copyright (C) 2016 - 2018 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.0.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <groupId>ch.ge.ve</groupId>
    <artifactId>pact-back</artifactId>
    <version>0.0.5</version>
    <packaging>pom</packaging>

    <name>pact-back</name>
    <description>Backend for the Privileged Actions Confirmation Tool (PACT)</description>
    <organization>
        <name>OCSIN-SIDP</name>
    </organization>

    <modules>
        <module>pact-back-contract</module>
        <module>pact-back-repository</module>
        <module>pact-back-fixtures</module>
        <module>pact-back-service</module>
        <module>pact-back-mock-server</module>
        <module>pact-back-rest</module>
        <module>pact-back-scheduler</module>
        <module>pact-b2b-client</module>
        <module>pact-notification-events</module>
    </modules>

    <distributionManagement>
        <snapshotRepository>
            <id>chvote-snapshots</id>
            <name>CHVote Snapshots</name>
            <url>${env.MVN_DIST_SNAPSHOTS_URL}</url>
        </snapshotRepository>
        <repository>
            <id>chvote</id>
            <name>CHVote Releases</name>
            <url>${env.MVN_DIST_RELEASES_URL}</url>
        </repository>
    </distributionManagement>

    <properties>
        <!-- Build parameters -->
        <java.version>11</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <build.timestamp>${maven.build.timestamp}</build.timestamp>

        <!-- SonarQube parameters -->
        <sonar.jacoco.reportPaths>${project.build.directory}/jacoco.exec</sonar.jacoco.reportPaths>

        <!-- Plugin versions -->
        <gmavenplus-plugin.version>1.6.2</gmavenplus-plugin.version>
        <jacoco-maven-plugin.version>0.8.2</jacoco-maven-plugin.version>
        <swagger-maven-plugin.version>3.1.7</swagger-maven-plugin.version>

        <!-- Plugin JAR dependencies versions -->
        <asm.version>7.0</asm.version>
        <javax-interceptor-api.version>1.2.2</javax-interceptor-api.version>

        <!-- JAR dependencies versions -->

        <!-- CHVote dependencies versions -->
        <file-namer.version>0.1.2</file-namer.version>
        <chvote-protocol-core.version>1.0.16</chvote-protocol-core.version>
        <chvote-protocol-model.version>1.0.6</chvote-protocol-model.version>
        <jackson-serializer.version>1.0.1</jackson-serializer.version>
        <chvote-model-converter.version>1.0.19</chvote-model-converter.version>
        <chvote-interfaces.version>2.0.11</chvote-interfaces.version>

        <!-- Additional dependencies versions -->
        <guava.version>27.0-jre</guava.version>
        <swagger-annotations.version>1.5.21</swagger-annotations.version>

        <!-- Test dependencies versions -->
        <objenesis.version>3.0.1</objenesis.version>
        <cglib.version>3.2.9</cglib.version>
        <spock.version>1.2-groovy-2.5</spock.version>

        <!-- SonarQube: allow underscores in the method names of Spring Data JPA repositories -->
        <sonar.issue.ignore.multicriteria>allowUnderscores</sonar.issue.ignore.multicriteria>
        <sonar.issue.ignore.multicriteria.allowUnderscores.ruleKey>squid:S00100
        </sonar.issue.ignore.multicriteria.allowUnderscores.ruleKey>
        <sonar.issue.ignore.multicriteria.allowUnderscores.resourceKey>**/repository/**/*Repository.java
        </sonar.issue.ignore.multicriteria.allowUnderscores.resourceKey>
    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- CHVote dependencies -->
            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>file-namer</artifactId>
                <version>${file-namer.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-client</artifactId>
                <version>${chvote-protocol-core.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-support</artifactId>
                <version>${chvote-protocol-core.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-messages-api</artifactId>
                <version>${chvote-protocol-core.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-messages-impl</artifactId>
                <version>${chvote-protocol-core.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>chvote-protocol-model</artifactId>
                <version>${chvote-protocol-model.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>jackson-serializer</artifactId>
                <version>${jackson-serializer.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>chvote-model-converter-api</artifactId>
                <version>${chvote-model-converter.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>chvote-model-converter-impl</artifactId>
                <version>${chvote-model-converter.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>chvote-interfaces-jaxb</artifactId>
                <version>${chvote-interfaces.version}</version>
            </dependency>

            <!-- Additional depndencies -->
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>
            <dependency>
                <groupId>io.swagger</groupId>
                <artifactId>swagger-annotations</artifactId>
                <version>${swagger-annotations.version}</version>
            </dependency>

            <!-- Test dependencies -->
            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-core</artifactId>
                <version>${spock.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-spring</artifactId>
                <version>${spock.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>cglib</groupId>
                <artifactId>cglib-nodep</artifactId>
                <version>${cglib.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.objenesis</groupId>
                <artifactId>objenesis</artifactId>
                <version>${objenesis.version}</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${jacoco-maven-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.gmavenplus</groupId>
                    <artifactId>gmavenplus-plugin</artifactId>
                    <version>${gmavenplus-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>com.github.kongchen</groupId>
                    <artifactId>swagger-maven-plugin</artifactId>
                    <version>${swagger-maven-plugin.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <dependencies>
                    <dependency>
                        <groupId>org.ow2.asm</groupId>
                        <artifactId>asm</artifactId>
                        <version>${asm.version}</version>
                    </dependency>
                </dependencies>
            </plugin>

            <plugin>
                <groupId>org.codehaus.gmavenplus</groupId>
                <artifactId>gmavenplus-plugin</artifactId>
                <configuration>
                    <targetBytecode>1.8</targetBytecode>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>addTestSources</goal>
                            <goal>compileTests</goal>
                            <goal>removeTestStubs</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <doclint>all,-reference</doclint>
                    <!-- Silence error javax.interceptor.InterceptorBinding not found -->
                    <!--    see https://stackoverflow.com/questions/27808734 -->
                    <additionalDependencies>
                        <additionalDependency>
                            <groupId>javax.interceptor</groupId>
                            <artifactId>javax.interceptor-api</artifactId>
                            <version>${javax-interceptor-api.version}</version>
                        </additionalDependency>
                    </additionalDependencies>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- Code coverage -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <configuration>
                    <append>true</append>
                    <destFile>${sonar.jacoco.reportPaths}</destFile>
                </configuration>
                <executions>
                    <execution>
                        <id>agent-for-unit-tests</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>jacoco-site</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

        </plugins>
    </build>

</project>
