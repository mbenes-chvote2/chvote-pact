/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation

import javax.validation.Validation
import javax.validation.Validator
import spock.lang.Specification

class ElectionSiteConfigurationVoTest extends Specification {
  private Validator validator

  def setup() {
    def factory = Validation.buildDefaultValidatorFactory()
    validator = factory.getValidator()
  }

  def "should not signal any violation for a valid ElectionSiteConfigurationVo"() {
    given: "an election site configuration"
    def electionSiteConfiguration = new ElectionSiteConfigurationVo()
    electionSiteConfiguration.ballot = "test ballot"
    electionSiteConfiguration.displayCandidateSearchForm = false
    electionSiteConfiguration.allowChangeOfElectoralList = false
    electionSiteConfiguration.displayEmptyPosition = false
    electionSiteConfiguration.displayCandidatePositionOnACompactBallotPaper = false
    electionSiteConfiguration.displayCandidatePositionOnAModifiedBallotPaper = false
    electionSiteConfiguration.displaySuffrageCount = false
    electionSiteConfiguration.displayListVerificationCode = false
    electionSiteConfiguration.allowOpenCandidature = false
    electionSiteConfiguration.allowMultipleMandates = false
    electionSiteConfiguration.displayVoidOnEmptyBallotPaper = false
    electionSiteConfiguration.candidateInformationDisplayModel = "MyModel"
    electionSiteConfiguration.columnsOrderOnVerificationTable = ["A", "B", "C"]
    and: "an invalid column name in the displayedColumnsOnVerificationTable array"
    electionSiteConfiguration.displayedColumnsOnVerificationTable = ["YES", "NO"]

    when:
    def violations = validator.validate(electionSiteConfiguration)

    then:
    violations.empty
  }

  def "invalid column names should cause the validation to fail"() {
    given: "an election site configuration"
    def electionSiteConfiguration = new ElectionSiteConfigurationVo()
    electionSiteConfiguration.ballot = "test ballot"
    electionSiteConfiguration.displayCandidateSearchForm = false
    electionSiteConfiguration.allowChangeOfElectoralList = false
    electionSiteConfiguration.displayEmptyPosition = false
    electionSiteConfiguration.displayCandidatePositionOnACompactBallotPaper = false
    electionSiteConfiguration.displayCandidatePositionOnAModifiedBallotPaper = false
    electionSiteConfiguration.displaySuffrageCount = false
    electionSiteConfiguration.displayListVerificationCode = false
    electionSiteConfiguration.allowOpenCandidature = false
    electionSiteConfiguration.allowMultipleMandates = false
    electionSiteConfiguration.displayVoidOnEmptyBallotPaper = false
    electionSiteConfiguration.candidateInformationDisplayModel = "MyModel"
    electionSiteConfiguration.columnsOrderOnVerificationTable = ["A", "B", "C"]
    and: "an invalid column name in the displayedColumnsOnVerificationTable array"
    electionSiteConfiguration.displayedColumnsOnVerificationTable = ["YES", "NO", "AGFADSFGAgads068549865"]

    when:
    def violations = validator.validate(electionSiteConfiguration)

    then:
    violations.size() == 1
    violations[0].propertyPath.toString() == "displayedColumnsOnVerificationTable"
  }
}
