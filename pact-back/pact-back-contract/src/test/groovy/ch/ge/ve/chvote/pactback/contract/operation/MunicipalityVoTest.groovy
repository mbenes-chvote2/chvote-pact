/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation

import javax.validation.Validation
import javax.validation.Validator
import spock.lang.Specification

import java.nio.charset.StandardCharsets

class MunicipalityVoTest extends Specification {
  private static Validator validator
  private static List<String> validMunicipalityIds
  private static List<String> validMunicipalityNames

  def setupSpec() {
    def factory = Validation.buildDefaultValidatorFactory()
    validator = factory.getValidator()

    def stream = MunicipalityVoTest.getResourceAsStream("/municipalityNames")
    validMunicipalityNames = new InputStreamReader(stream, StandardCharsets.UTF_8).readLines()

    stream = MunicipalityVoTest.getResourceAsStream("/municipalityIds")
    validMunicipalityIds = new InputStreamReader(stream, StandardCharsets.UTF_8).readLines()
  }

  def "municipalityId validation should accept all known swiss municipality ids"() {
    given:
    def municipalityVo = new MunicipalityVo()
    municipalityVo.ofsId = Integer.valueOf(municipalityId)
    municipalityVo.name = "Random name"

    when:
    def violations = validator.validate(municipalityVo)

    then:
    violations.empty

    where:
    municipalityId << validMunicipalityIds
  }

  def "municipality name validation should accept all known swiss municipality names"() {
    given:
    def municipalityVo = new MunicipalityVo()
    municipalityVo.ofsId = 1
    municipalityVo.name = municipalityName

    when:
    def violations = validator.validate(municipalityVo)

    then:
    violations.empty

    where:
    municipalityName << validMunicipalityNames
  }

  def "should not validate a MunicipalityVo with an invalid municipalityId"() {
    given:
    def municipalityVo = new MunicipalityVo()
    municipalityVo.ofsId = 10000
    municipalityVo.name = "Random name"

    when:
    def violations = validator.validate(municipalityVo)

    then:
    violations.size() == 1
    violations[0].propertyPath.toString() == "ofsId"
  }

  def "should not validate a MunicipalityVo with an invalid name"() {
    given:
    def municipalityVo = new MunicipalityVo()
    municipalityVo.ofsId = 1
    municipalityVo.name = "Genève?"

    when:
    def violations = validator.validate(municipalityVo)

    then:
    violations.size() == 1
    violations[0].propertyPath.toString() == "name"
  }
}
