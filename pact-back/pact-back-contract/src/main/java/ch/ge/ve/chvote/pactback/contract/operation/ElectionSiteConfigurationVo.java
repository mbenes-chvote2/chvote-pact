/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import java.util.List;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * A DTO containing the configuration of the election site.
 */
public class ElectionSiteConfigurationVo {
  /**
   * Related Ballot
   */
  @NotNull
  @Pattern(regexp = "[\\p{L}\\d \\-',()/.]+")
  @Size(min = 1, max = 50)
  private String  ballot;
  /**
   * Should display candidate search form on the voting site
   */
  @NotNull
  private boolean displayCandidateSearchForm;

  /**
   * Should allow change of electoral list on the voting site
   */
  @NotNull
  private boolean allowChangeOfElectoralList;

  /**
   * Should display empty position on the voting site
   */
  @NotNull
  private boolean displayEmptyPosition;

  /**
   * Should display candidate position on a compact ballot paper
   */
  @NotNull
  private boolean displayCandidatePositionOnACompactBallotPaper;

  /**
   * Should display candidate position on a modified ballot paper
   */
  @NotNull
  private boolean displayCandidatePositionOnAModifiedBallotPaper;

  /**
   * Should display suffrage count
   */
  @NotNull
  private boolean displaySuffrageCount;

  /**
   * Should display verification code on election list
   */
  @NotNull
  private boolean displayListVerificationCode;

  /**
   * Should allow open candidature
   */
  @NotNull
  private boolean allowOpenCandidature;

  /**
   * Should allow multiple mandates
   */
  @NotNull
  private boolean allowMultipleMandates;

  /**
   * Should display void ballot paper when there is no candidate or no name of the party
   */
  @NotNull
  private boolean displayVoidOnEmptyBallotPaper;

  /**
   * Name of the display model used to display candidate information
   */
  @NotNull
  @Pattern(regexp = "[\\p{L} \\-',()/.]+")
  @Size(min = 1, max = 200)
  private String candidateInformationDisplayModel;

  /**
   * Columns to be displayed on the verification table
   */
  @NotNull
  @Pattern(regexp = "[A-Za-z]+-?[A-Za-z]*")
  private List<String> displayedColumnsOnVerificationTable;

  /**
   * Columns sort order on the verification table
   */
  @NotNull
  @Pattern(regexp = "[A-Za-z]+-?[A-Za-z]*")
  private List<String> columnsOrderOnVerificationTable;

  public String getBallot() {
    return ballot;
  }

  public void setBallot(String ballot) {
    this.ballot = ballot;
  }

  public boolean isDisplayCandidateSearchForm() {
    return displayCandidateSearchForm;
  }

  public void setDisplayCandidateSearchForm(boolean displayCandidateSearchForm) {
    this.displayCandidateSearchForm = displayCandidateSearchForm;
  }

  public boolean isAllowChangeOfElectoralList() {
    return allowChangeOfElectoralList;
  }

  public void setAllowChangeOfElectoralList(boolean allowChangeOfElectoralList) {
    this.allowChangeOfElectoralList = allowChangeOfElectoralList;
  }

  public boolean isDisplayEmptyPosition() {
    return displayEmptyPosition;
  }

  public void setDisplayEmptyPosition(boolean displayEmptyPosition) {
    this.displayEmptyPosition = displayEmptyPosition;
  }

  public boolean isDisplayCandidatePositionOnACompactBallotPaper() {
    return displayCandidatePositionOnACompactBallotPaper;
  }

  public void setDisplayCandidatePositionOnACompactBallotPaper(boolean
                                                                   displayCandidatePositionOnACompactBallotPaper) {
    this.displayCandidatePositionOnACompactBallotPaper = displayCandidatePositionOnACompactBallotPaper;
  }

  public boolean isDisplayCandidatePositionOnAModifiedBallotPaper() {
    return displayCandidatePositionOnAModifiedBallotPaper;
  }

  public void setDisplayCandidatePositionOnAModifiedBallotPaper(boolean
                                                                    displayCandidatePositionOnAModifiedBallotPaper) {
    this.displayCandidatePositionOnAModifiedBallotPaper = displayCandidatePositionOnAModifiedBallotPaper;
  }

  public boolean isDisplaySuffrageCount() {
    return displaySuffrageCount;
  }

  public void setDisplaySuffrageCount(boolean displaySuffrageCount) {
    this.displaySuffrageCount = displaySuffrageCount;
  }

  public boolean isDisplayListVerificationCode() {
    return displayListVerificationCode;
  }

  public void setDisplayListVerificationCode(boolean displayListVerificationCode) {
    this.displayListVerificationCode = displayListVerificationCode;
  }

  public boolean isAllowOpenCandidature() {
    return allowOpenCandidature;
  }

  public void setAllowOpenCandidature(boolean allowOpenCandidature) {
    this.allowOpenCandidature = allowOpenCandidature;
  }

  public boolean isAllowMultipleMandates() {
    return allowMultipleMandates;
  }

  public void setAllowMultipleMandates(boolean allowMultipleMandates) {
    this.allowMultipleMandates = allowMultipleMandates;
  }

  public boolean isDisplayVoidOnEmptyBallotPaper() {
    return displayVoidOnEmptyBallotPaper;
  }

  public void setDisplayVoidOnEmptyBallotPaper(boolean displayVoidOnEmptyBallotPaper) {
    this.displayVoidOnEmptyBallotPaper = displayVoidOnEmptyBallotPaper;
  }

  public String getCandidateInformationDisplayModel() {
    return candidateInformationDisplayModel;
  }

  public void setCandidateInformationDisplayModel(String candidateInformationDisplayModel) {
    this.candidateInformationDisplayModel = candidateInformationDisplayModel;
  }

  public List<String> getDisplayedColumnsOnVerificationTable() {
    return displayedColumnsOnVerificationTable;
  }

  public void setDisplayedColumnsOnVerificationTable(List<String> displayedColumnsOnVerificationTable) {
    this.displayedColumnsOnVerificationTable = displayedColumnsOnVerificationTable;
  }

  public List<String> getColumnsOrderOnVerificationTable() {
    return columnsOrderOnVerificationTable;
  }

  public void setColumnsOrderOnVerificationTable(List<String> columnsOrderOnVerificationTable) {
    this.columnsOrderOnVerificationTable = columnsOrderOnVerificationTable;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ElectionSiteConfigurationVo that = (ElectionSiteConfigurationVo) o;
    return Objects.equals(ballot, that.ballot);
  }

  @Override
  public int hashCode() {

    return Objects.hash(ballot);
  }
}
