/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation.status;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.List;

/**
 * A DTO containing the state of an operation configuration in test.
 */
public class InTestConfigurationStatusVo extends AbstractStatusVo {
  private final List<ProtocolStageProgressVo<VotingMaterialCreationStage>> protocolProgress;
  private final State                                                      state;
  private final String                                                     comment;
  private final String                                                     configurationPageUrl;
  private final String                                                     votingCardsLocation;
  private final String                                                     voteReceiverUrl;

  /**
   * Create a new operation configuration in test status DTO.
   *
   * @param lastChangeUser       the lastChangeUser that has modified this configuration for the last time.
   * @param lastChangeDate       the last status' date of this configuration.
   * @param state                the current state of the Test configuration.
   * @param comment              a comment regarding the state, only available when the state is {@link
   *                             State#DEPLOYMENT_REFUSED}.
   * @param configurationPageUrl The PACT frontend URL of the deployment page for the current configuration, relative to
   *                             the PACT application context URL
   * @param votingCardsLocation  the relative location of the test printer archive in the shared volume.
   * @param protocolProgress     a list with the completion ratio of each stage of the protocol instance.
   * @param voteReceiverUrl      the url to the voting site.
   */
  @JsonCreator
  public InTestConfigurationStatusVo(
      @JsonProperty("lastChangeUser") String lastChangeUser,
      @JsonProperty("lastChangeDate") LocalDateTime lastChangeDate,
      @JsonProperty("state") State state,
      @JsonProperty("comment") String comment,
      @JsonProperty("configurationPageUrl") String configurationPageUrl,
      @JsonProperty("votingCardsLocation") String votingCardsLocation,
      @JsonProperty("protocolProgress") List<ProtocolStageProgressVo<VotingMaterialCreationStage>> protocolProgress,
      @JsonProperty("voteReceiverUrl") String voteReceiverUrl) {
    super(lastChangeUser, lastChangeDate);
    this.state = state;
    this.comment = comment;
    this.configurationPageUrl = configurationPageUrl;
    this.votingCardsLocation = votingCardsLocation;
    this.protocolProgress = protocolProgress;
    this.voteReceiverUrl = voteReceiverUrl;
  }

  public State getState() {
    return state;
  }

  public String getComment() {
    return comment;
  }

  public String getConfigurationPageUrl() {
    return configurationPageUrl;
  }

  public String getVotingCardsLocation() {
    return votingCardsLocation;
  }

  public List<ProtocolStageProgressVo<VotingMaterialCreationStage>> getProtocolProgress() {
    return protocolProgress;
  }

  public String getVoteReceiverUrl() {
    return voteReceiverUrl;
  }

  /**
   * Describes the different states of an {@link InTestConfigurationStatusVo}.
   */
  public enum State {
    /**
     * The test operation is initializing and is not yet available.
     */
    TEST_SITE_IN_DEPLOYMENT,
    /**
     * If there was an error initializing the operation.
     */
    IN_ERROR,
    /**
     * The test operation is available for testing and waiting for validation.
     */
    IN_VALIDATION,
    /**
     * The test operation has been validated by the client.
     */
    VALIDATED,
    /**
     * The test operation has been invalidated by the client. The client can now submit a new configuration.
     */
    INVALIDATED,
    /**
     * A production deployment has been requested for the operation.
     */
    DEPLOYMENT_REQUESTED,
    /**
     * The production deployment request has been refused. The client can now submit a new configuration.
     */
    DEPLOYMENT_REFUSED,
    /**
     * The configuration has been deployed to production.
     */
    DEPLOYED
  }

}
