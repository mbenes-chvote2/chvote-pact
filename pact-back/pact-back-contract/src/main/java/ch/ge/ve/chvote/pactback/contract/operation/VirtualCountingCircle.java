/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * An enumeration of all the virtual counting circles used during the different testing phases of an operation.
 */
public enum VirtualCountingCircle {
  /**
   * Defines the counting circle whose voting cards are used as control tests during a production operation.
   */
  CONTROLLER_TESTING_CARDS("100000", "Controller testing cards"),
  /**
   * Defines the counting circle whose voting cards are used for testing and can be printed.
   */
  PRINTABLE_TESTING_CARDS("100001", "Printable testing cards"),
  /**
   * Defines the counting circle whose voting cards are used for testing but are not printed.
   */
  NOT_PRINTABLE_TESTING_CARDS("100002", "Not printable testing cards"),
  /**
   * Defines the c counting circle whose voting cards are used to test the printer.
   */
  PRINTER_TESTING_CARDS("100003", "Testing cards");

  public final String id;
  public final String countingCircleName;

  VirtualCountingCircle(String id, String countingCircleName) {
    this.id = id;
    this.countingCircleName = countingCircleName;
  }

  /**
   * Look up a virtual counting circle by its identifier.
   *
   * @param id the virtual counting circle identifier.
   *
   * @return a nullable optional containing the counting circle.
   */
  public static Optional<VirtualCountingCircle> byId(String id) {
    return Stream.of(VirtualCountingCircle.values())
                 .filter(virtualCountingCircle -> Objects.equals(virtualCountingCircle.id, id))
                 .findFirst();
  }

}
