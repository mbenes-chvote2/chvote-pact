/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation.status;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;

/**
 * A DTO representing the progress of the different steps of a voting material generation task.
 *
 * @param <T> An enum type with the stages of the protocol that this instance represents.
 */
public class ProtocolStageProgressVo<T extends Enum<T>> {
  private final T             stage;
  private final Integer       ccIndex;
  private final Double        ratio;
  private final LocalDateTime startDate;
  private final LocalDateTime lastModificationDate;

  /**
   * Create a new protocol stage progress DTO.
   *
   * @param stage                the protocol stage.
   * @param ccIndex              the control component index or -1 if this stage does not keep track of individual
   *                             control component progress.
   * @param ratio                the progress (from 0 to 1).
   * @param startDate            the moment this stage started.
   * @param lastModificationDate the moment this stage was updated for the last time.
   */
  public ProtocolStageProgressVo(
      @JsonProperty("stage") T stage,
      @JsonProperty("ccIndex") Integer ccIndex,
      @JsonProperty("ratio") Double ratio,
      @JsonProperty("startDate") LocalDateTime startDate,
      @JsonProperty("lastModificationDate") LocalDateTime lastModificationDate) {
    this.stage = stage;
    this.ccIndex = ccIndex;
    this.ratio = ratio;
    this.startDate = startDate;
    this.lastModificationDate = lastModificationDate;
  }

  public T getStage() {
    return stage;
  }

  public Integer getCcIndex() {
    return ccIndex;
  }

  public Double getRatio() {
    return ratio;
  }

  public LocalDateTime getStartDate() {
    return startDate;
  }

  public LocalDateTime getLastModificationDate() {
    return lastModificationDate;
  }
}
