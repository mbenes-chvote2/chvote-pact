/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import java.util.Collections;
import java.util.Set;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for incoming voting materials configuration requests.
 */
public class VotingMaterialsConfigurationSubmissionVo {

  @NotNull
  private String user;

  @NotNull
  private String votingCardLabel;

  @NotNull
  private DeploymentTarget target;

  private String simulationName;

  @NotNull
  @Size(min = 1)
  private Set<RegisterFileEntryVo> registerFilesCatalog = Collections.emptySet();

  private Set<MunicipalityVo>              municipalities                = Collections.emptySet();
  private Set<MunicipalityVo>              virtualMunicipalities         = Collections.emptySet();
  private Set<PrinterConfigurationVo>      printers                      = Collections.emptySet();
  private Set<MunicipalityToPrinterLinkVo> municipalitiesToPrintersLinks = Collections.emptySet();
  private PrinterConfigurationVo swissAbroadWithoutMunicipalityPrinter;

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getVotingCardLabel() {
    return votingCardLabel;
  }

  public void setVotingCardLabel(String votingCardLabel) {
    this.votingCardLabel = votingCardLabel;
  }

  public DeploymentTarget getTarget() {
    return target;
  }

  public void setTarget(DeploymentTarget target) {
    this.target = target;
  }

  public String getSimulationName() {
    return simulationName;
  }

  public void setSimulationName(String simulationName) {
    this.simulationName = simulationName;
  }

  public Set<RegisterFileEntryVo> getRegisterFilesCatalog() {
    return registerFilesCatalog;
  }

  public void setRegisterFilesCatalog(Set<RegisterFileEntryVo> registerFilesCatalog) {
    this.registerFilesCatalog = registerFilesCatalog;
  }

  public Set<MunicipalityVo> getMunicipalities() {
    return municipalities;
  }

  public void setMunicipalities(Set<MunicipalityVo> municipalities) {
    this.municipalities = municipalities;
  }

  public Set<MunicipalityVo> getVirtualMunicipalities() {
    return virtualMunicipalities;
  }

  public void setVirtualMunicipalities(Set<MunicipalityVo> virtualMunicipalities) {
    this.virtualMunicipalities = virtualMunicipalities;
  }

  public Set<PrinterConfigurationVo> getPrinters() {
    return printers;
  }

  public void setPrinters(Set<PrinterConfigurationVo> printers) {
    this.printers = printers;
  }

  public Set<MunicipalityToPrinterLinkVo> getMunicipalitiesToPrintersLinks() {
    return municipalitiesToPrintersLinks;
  }

  public void setMunicipalitiesToPrintersLinks(Set<MunicipalityToPrinterLinkVo> municipalitiesToPrintersLinks) {
    this.municipalitiesToPrintersLinks = municipalitiesToPrintersLinks;
  }

  public PrinterConfigurationVo getSwissAbroadWithoutMunicipalityPrinter() {
    return swissAbroadWithoutMunicipalityPrinter;
  }

  public void setSwissAbroadWithoutMunicipalityPrinter(PrinterConfigurationVo swissAbroadWithoutMunicipalityPrinter) {
    this.swissAbroadWithoutMunicipalityPrinter = swissAbroadWithoutMunicipalityPrinter;
  }
}
