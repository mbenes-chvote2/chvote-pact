/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.validation;

import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo;
import java.util.Objects;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * A validator implementation for {@link AttachmentType} constraint. Checks that the given  {@link
 * AttachmentFileEntryVo} is  listed in {@link AttachmentType#value()}}.
 */
public class AttachmentTypeValidator implements ConstraintValidator<AttachmentType, AttachmentFileEntryVo> {
  private AttachmentFileEntryVo.AttachmentType attachmentType;

  @Override
  public void initialize(AttachmentType constraintAnnotation) {
    attachmentType = constraintAnnotation.value();
  }

  @Override
  public boolean isValid(AttachmentFileEntryVo attachment, ConstraintValidatorContext constraintValidatorContext) {
    //null values are valid
    if (attachment == null) {
      return true;
    }

    return Objects.equals(attachmentType, attachment.getAttachmentType());
  }
}