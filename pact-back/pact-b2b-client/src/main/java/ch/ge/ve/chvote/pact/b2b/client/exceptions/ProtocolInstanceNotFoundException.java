/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client.exceptions;

/**
 * Thrown when a protocol instance is not found.
 */
public class ProtocolInstanceNotFoundException extends RuntimeException {
  /**
   * Creates a new {@link ProtocolInstanceNotFoundException} with a message and a root cause, see: {@link
   * Throwable#Throwable(String, Throwable)}.
   *
   * @param protocolId the protocol id (used to format the {@link Exception#getMessage()} of this exception).
   * @param cause      the original cause.
   */
  public ProtocolInstanceNotFoundException(String protocolId, Throwable cause) {
    super(String.format("No protocol instance was found for protocol id: [%s]", protocolId), cause);
  }
}
