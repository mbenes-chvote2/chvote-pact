/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.entity;

import ch.ge.ve.chvote.pactback.repository.action.deploy.entity.ConfigurationDeployment;
import ch.ge.ve.chvote.pactback.repository.exception.ProtocolInstanceNotFoundException;
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.BallotDocumentation;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.HighlightedQuestion;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.InformationAttachment;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * An entity that represents the operation configuration.
 */
@Entity
@Table(name = "PACT_T_OPERATION_CONFIGURATION")
public class OperationConfiguration {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "OPC_N_ID")
  protected Long id;

  @Column(name = "OPC_V_OPERATION_NAME", nullable = false)
  private String operationName;

  @Column(name = "OPC_V_OPERATION_LABEL", nullable = false)
  private String operationLabel;

  @Column(name = "OPC_D_OPERATION_DATE", nullable = false)
  private LocalDateTime operationDate;

  @Column(name = "OPC_V_VALIDATION_STATUS")
  @Enumerated(EnumType.STRING)
  private ValidationStatus validationStatus;

  @Column(name = "OPC_D_LAST_CHANGE_DATE")
  private LocalDateTime lastChangeDate;

  @Column(name = "OPC_V_LAST_CHANGE_USER")
  private String lastChangeUser;

  /**
   * Hash of the voting site text.
   */
  @Column(name = "OPC_V_SITE_TEXT_HASH")
  private String siteTextHash;

  @Column(name = "OPC_D_SITE_OPENING_DATE", nullable = false)
  private LocalDateTime siteOpeningDate;

  @Column(name = "OPC_D_SITE_CLOSING_DATE", nullable = false)
  private LocalDateTime siteClosingDate;

  /**
   * Grace period in minutes.
   */
  @Column(name = "OPC_N_GRACE_PERIOD", nullable = false)
  private Integer gracePeriod;

  @Column(name = "OPC_V_CANTON", nullable = false)
  private String canton;

  @Column(name = "OPC_B_GROUP_VOTATION", nullable = false)
  private boolean groupVotation;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
  @JoinColumn(name = "OPC_N_PCONF_ID", nullable = false)
  private PrinterConfiguration testPrinter;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "ORF_OPERATION_CONFIGURATION_ID")
  private List<OperationReferenceFile> operationReferenceFiles = new ArrayList<>();

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "IATTACH_OPERATION_CONFIGURATION_ID")
  private List<InformationAttachment> informationAttachments = new ArrayList<>();

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "SC_OPERATION_CONFIGURATION_ID")
  private List<ElectionSiteConfiguration> electionSiteConfigurations = new ArrayList<>();

  @OneToOne(cascade = CascadeType.ALL, mappedBy = "configuration", fetch = FetchType.LAZY, orphanRemoval = true)
  private ProtocolInstance protocolInstance;

  @OneToOne(cascade = CascadeType.ALL, mappedBy = "operationConfiguration", fetch = FetchType.EAGER,
            orphanRemoval = true)
  private ConfigurationDeployment action;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "BDOC_OPERATION_CONFIGURATION_ID")
  private List<BallotDocumentation> ballotDocumentations = new ArrayList<>();

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "HQ_OPERATION_CONFIGURATION_ID")
  private List<HighlightedQuestion> highlightedQuestions = new ArrayList<>();

  public Long getId() {
    return id;
  }

  public String getOperationName() {
    return operationName;
  }

  public void setOperationName(String operationName) {
    this.operationName = operationName;
  }

  public String getOperationLabel() {
    return operationLabel;
  }

  public void setOperationLabel(String operationLabel) {
    this.operationLabel = operationLabel;
  }

  public LocalDateTime getOperationDate() {
    return operationDate;
  }

  public void setOperationDate(LocalDateTime operationDate) {
    this.operationDate = operationDate;
  }

  public String getSiteTextHash() {
    return siteTextHash;
  }

  public void setSiteTextHash(String siteTextHash) {
    this.siteTextHash = siteTextHash;
  }

  public ValidationStatus getValidationStatus() {
    return validationStatus;
  }

  public void setValidationStatus(ValidationStatus validationStatus) {
    this.validationStatus = validationStatus;
  }

  public LocalDateTime getLastChangeDate() {
    return lastChangeDate;
  }

  public void setLastChangeDate(LocalDateTime lastChangeDate) {
    this.lastChangeDate = lastChangeDate;
  }

  public String getLastChangeUser() {
    return lastChangeUser;
  }

  public void setLastChangeUser(String lastChangeUser) {
    this.lastChangeUser = lastChangeUser;
  }

  public LocalDateTime getSiteOpeningDate() {
    return siteOpeningDate;
  }

  public void setSiteOpeningDate(LocalDateTime siteOpeningDate) {
    this.siteOpeningDate = siteOpeningDate;
  }

  public LocalDateTime getSiteClosingDate() {
    return siteClosingDate;
  }

  public void setSiteClosingDate(LocalDateTime siteClosingDate) {
    this.siteClosingDate = siteClosingDate;
  }

  public Integer getGracePeriod() {
    return gracePeriod;
  }

  public void setGracePeriod(Integer gracePeriod) {
    this.gracePeriod = gracePeriod;
  }

  public String getCanton() {
    return canton;
  }

  public void setCanton(String canton) {
    this.canton = canton;
  }

  public boolean isGroupVotation() {
    return groupVotation;
  }

  public void setGroupVotation(boolean groupVotation) {
    this.groupVotation = groupVotation;
  }

  public PrinterConfiguration getTestPrinter() {
    return testPrinter;
  }

  public void setTestPrinter(PrinterConfiguration testPrinter) {
    this.testPrinter = testPrinter;
  }

  public List<OperationReferenceFile> getOperationReferenceFiles() {
    return operationReferenceFiles;
  }

  public void setOperationReferenceFiles(List<OperationReferenceFile> operationReferenceFiles) {
    this.operationReferenceFiles = operationReferenceFiles;
  }

  public List<InformationAttachment> getInformationAttachments() {
    return informationAttachments;
  }

  public void setInformationAttachments(List<InformationAttachment> informationAttachments) {
    this.informationAttachments = informationAttachments;
  }

  public List<ElectionSiteConfiguration> getElectionSiteConfigurations() {
    return electionSiteConfigurations;
  }

  public void setElectionSiteConfigurations(List<ElectionSiteConfiguration> electionSiteConfigurations) {
    this.electionSiteConfigurations = electionSiteConfigurations;
  }

  public Optional<ConfigurationDeployment> getAction() {
    return Optional.ofNullable(action);
  }

  public void setAction(ConfigurationDeployment action) {
    this.action = action;
  }


  public List<BallotDocumentation> getBallotDocumentations() {
    return ballotDocumentations;
  }

  public void setBallotDocumentations(List<BallotDocumentation> ballotDocumentations) {
    this.ballotDocumentations = ballotDocumentations;
  }

  public List<HighlightedQuestion> getHighlightedQuestions() {
    return highlightedQuestions;
  }

  public void setHighlightedQuestions(List<HighlightedQuestion> highlightedQuestions) {
    this.highlightedQuestions = highlightedQuestions;
  }

  public Optional<ProtocolInstance> getProtocolInstance() {
    return Optional.ofNullable(protocolInstance);
  }

  /**
   * Fetch the protocol instance or else it throws a {@link ProtocolInstanceNotFoundException}
   */
  public ProtocolInstance fetchProtocolInstance() {
    return getProtocolInstance().orElseThrow(() -> new ProtocolInstanceNotFoundException(id));
  }

  public void setProtocolInstance(ProtocolInstance protocolInstance) {
    this.protocolInstance = protocolInstance;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OperationConfiguration that = (OperationConfiguration) o;

    return id != null ? id.equals(that.id) : that.id == null;
  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }
}
