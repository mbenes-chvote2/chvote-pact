/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.action;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import java.util.stream.Stream;

/**
 * A specialised {@link BasePrivilegedActionRepository} for interacting with {@link PrivilegedAction} entities,
 * regardless of the implementation.
 */
public interface PrivilegedActionRepository extends BasePrivilegedActionRepository<PrivilegedAction> {

  /**
   * Retrieve the number of {@link PrivilegedAction} by requester id different than the given id and the given status.
   *
   * @param requesterId The excluded requester of id.
   * @param status      The actions status.
   *
   * @return The number of occurrences for the given parameters.
   */
  int countByRequesterIdNotAndStatus(Long requesterId, PrivilegedAction.Status status);

  /**
   * Find all {@link PrivilegedAction} by status.
   *
   * @param status The requested status.
   *
   * @return A {@link Stream} containing the requested actions.
   */
  Stream<PrivilegedAction> findByStatus(PrivilegedAction.Status status);

}
