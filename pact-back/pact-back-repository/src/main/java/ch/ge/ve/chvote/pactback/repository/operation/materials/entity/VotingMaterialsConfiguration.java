/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.materials.entity;

import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsCreation;
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsInvalidation;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.VotersRegisterFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * An entity that represents the voting materials configuration for an operation.
 */
@Entity
@Table(name = "PACT_T_VOT_MAT_CONF")
public class VotingMaterialsConfiguration {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "VMC_N_ID")
  private Long id;

  @Column(name = "VMC_V_AUTHOR")
  private String author;

  @Column(name = "VMC_D_SUBMISSION_DATE")
  private LocalDateTime submissionDate;

  @Column(name = "VMC_V_VOTING_CARD_LABEL")
  private String votingCardLabel;

  @Column(name = "VMC_V_VALIDATION_STATUS")
  @Enumerated(EnumType.STRING)
  private ValidationStatus validationStatus;

  @Column(name = "VMC_V_TARGET")
  @Enumerated(EnumType.STRING)
  private DeploymentTarget target;

  @Column(name = "VMC_V_SIMULATION_NAME")
  private String simulationName;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
  @JoinColumn(name = "VRF_N_VMC_ID")
  private List<VotersRegisterFile> registers = new ArrayList<>();

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
  @JoinColumn(name = "PCONF_N_VMC_ID")
  private List<PrinterConfiguration> printers = new ArrayList<>();

  @OneToOne
  @JoinColumn(name = "PCONF_V_ABROAD_PRINTER_ID")
  private PrinterConfiguration swissAbroadPrinter;

  @OneToOne(cascade = CascadeType.ALL, mappedBy = "votingMaterialsConfiguration", fetch = FetchType.EAGER,
            orphanRemoval = true)
  private VotingMaterialsCreation creationAction;

  @OneToOne(cascade = CascadeType.ALL, mappedBy = "votingMaterialsConfiguration", fetch = FetchType.EAGER,
            orphanRemoval = true)
  private VotingMaterialsInvalidation invalidationAction;

  @Column(name = "VMC_V_VALIDATOR")
  private String validator;

  @Column(name = "VMC_D_VALIDATION_DATE")
  private LocalDateTime validationDate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public LocalDateTime getSubmissionDate() {
    return submissionDate;
  }

  public void setSubmissionDate(LocalDateTime submissionDate) {
    this.submissionDate = submissionDate;
  }

  public String getVotingCardLabel() {
    return votingCardLabel;
  }

  public void setVotingCardLabel(String votingCardLabel) {
    this.votingCardLabel = votingCardLabel;
  }

  public DeploymentTarget getTarget() {
    return target;
  }

  public void setTarget(DeploymentTarget target) {
    this.target = target;
  }

  public String getSimulationName() {
    return simulationName;
  }

  public void setSimulationName(String simulationName) {
    this.simulationName = simulationName;
  }

  public List<VotersRegisterFile> getRegisters() {
    return registers;
  }

  public void setRegisters(List<VotersRegisterFile> registers) {
    this.registers = registers;
  }

  public List<PrinterConfiguration> getPrinters() {
    return printers;
  }

  public void setPrinters(List<PrinterConfiguration> printers) {
    this.printers = printers;
  }

  public Optional<VotingMaterialsCreation> getCreationAction() {
    return Optional.ofNullable(creationAction);
  }

  public void setAction(VotingMaterialsCreation creationAction) {
    this.creationAction = creationAction;
  }


  public Optional<VotingMaterialsInvalidation> getInvalidationAction() {
    return Optional.ofNullable(invalidationAction);
  }

  public void setInvalidationAction(VotingMaterialsInvalidation invalidationAction) {
    this.invalidationAction = invalidationAction;
  }

  public PrinterConfiguration getSwissAbroadPrinter() {
    return swissAbroadPrinter;
  }

  public void setSwissAbroadPrinter(PrinterConfiguration swissAbroadPrinter) {
    this.swissAbroadPrinter = swissAbroadPrinter;
  }

  public ValidationStatus getValidationStatus() {
    return validationStatus;
  }

  public void setValidationStatus(ValidationStatus validationStatus) {
    this.validationStatus = validationStatus;
  }

  public String getValidator() {
    return validator;
  }

  public void setValidator(String validator) {
    this.validator = validator;
  }

  public LocalDateTime getValidationDate() {
    return validationDate;
  }

  public void setValidationDate(LocalDateTime validationDate) {
    this.validationDate = validationDate;
  }
}
