/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol.progress;

/**
 * All the stages of a protocol instance.
 */
public enum ProtocolStage {
  /**
   * Send public parameters stage.
   */
  SEND_PUBLIC_PARAMETERS,
  /**
   * Publish public key parts stage.
   */
  PUBLISH_PUBLIC_KEY_PARTS,
  /**
   * Store public key parts stage.
   */
  STORE_PUBLIC_KEY_PARTS,
  /**
   * Send election set stage.
   */
  SEND_ELECTION_SET,
  /**
   * Publish primes stage.
   */
  PUBLISH_PRIMES,
  /**
   * Send voters stage.
   */
  SEND_VOTERS,
  /**
   * Build public credentials stage.
   */
  BUILD_PUBLIC_CREDENTIALS,
  /**
   * Request private credentials stage.
   */
  REQUEST_PRIVATE_CREDENTIALS,
  /**
   * Send election officer key stage.
   */
  SEND_ELECTION_OFFICER_KEY,
  /**
   * Publish shuffle stage.
   */
  PUBLISH_SHUFFLE,
  /**
   * Publish partial decryption stage.
   */
  PUBLISH_PARTIAL_DECRYPTION
}
