/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol;

import ch.ge.ve.chvote.pactback.repository.protocol.entity.DeadLetterEvent;
import java.time.LocalDateTime;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository to manage {@link DeadLetterEvent} entities.
 */
public interface DeadLetterEventRepository extends JpaRepository<DeadLetterEvent, Long> {

  /**
   * Delete all messages associated to a given {@code protocolId}.
   * <p>
   *   <em>Implementation note:</em> this is done via the standard SpringData interpretation of a "deleteBy"
   *   based on a "selectBy" + "remove" each entity.
   *   This might not be the most efficient way to do it, but it ensures that the lifecycle of the entities is
   *   managed by the {@code EntityManager}.
   * </p>
   *
   * @param protocolId identifier of the protocol.
   *
   * @return the number of removed messages.
   */
  long deleteByProtocolId(String protocolId);

  /**
   * Returns whether an entity having the given values is already stored.
   *
   * @param protocolId   the protocol id.
   * @param type         the event's type.
   * @param creationDate the event's creation date.
   * @param content      the event's content.
   *
   * @return whether an entity having the given values is already stored.
   */
  boolean existsByProtocolIdAndTypeAndCreationDateAndContent(String protocolId, String type,
                                                             LocalDateTime creationDate, String content);
}
