/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * An entity that represents an attachment.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "PACT_T_ATTACHMENT")
public abstract class Attachment {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ATTACH_N_ID")
  // Keep it package protect in order to have access to in in spock since accessing through super class
  Long id;

  @Column(name = "ATTACH_V_NAME")
  private String name;

  @Column(name = "ATTACH_N_EXTERNAL_IDENTIFIER")
  private Long externalIdentifier;

  @Column(name = "ATTACH_V_TYPE")
  @Enumerated(EnumType.STRING)
  private AttachmentType type;

  @Column(name = "ATTACH_D_IMPORT_DATE")
  private LocalDateTime importDate;

  @Column(name = "ATTACH_V_FILE_HASH")
  private String fileHash;

  @Lob
  @Basic(fetch = FetchType.LAZY)
  @Column(name = "ATTACH_X_FILE")
  private byte[] file;

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public AttachmentType getType() {
    return type;
  }

  public void setType(AttachmentType type) {
    this.type = type;
  }

  public LocalDateTime getImportDate() {
    return importDate;
  }

  public void setImportDate(LocalDateTime importDate) {
    this.importDate = importDate;
  }

  public String getFileHash() {
    return fileHash;
  }

  public void setFileHash(String fileHash) {
    this.fileHash = fileHash;
  }

  public byte[] getFile() {
    return file;
  }

  public void setFile(byte[] file) {
    this.file = file;
  }

  public Long getExternalIdentifier() {
    return externalIdentifier;
  }

  public void setExternalIdentifier(Long externalIdentifier) {
    this.externalIdentifier = externalIdentifier;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Attachment that = (Attachment) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
