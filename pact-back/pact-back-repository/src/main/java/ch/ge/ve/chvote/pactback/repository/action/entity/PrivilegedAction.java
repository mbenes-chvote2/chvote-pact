/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.action.entity;

import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * An entity that represents an action that must be validated by a different user than the requester.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "PACT_T_PRIVILEGED_ACTION")
public abstract class PrivilegedAction {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "PVA_N_ID")
  protected Long id;

  /**
   * The type of action, value must be defined in derived classes.
   */
  @Column(name = "PVA_V_TYPE")
  private String type;

  @Column(name = "PVA_V_OPERATION_NAME")
  private String operationName;

  @Column(name = "PVA_D_OPERATION_DATE")
  private LocalDateTime operationDate;

  @Column(name = "PVA_V_REJECTION_REASON")
  private String rejectionReason;

  /**
   * LocalDateTime when the action was created, typically by an end user on the PACT GUI.
   */
  @Column(name = "PVA_D_CREATION_DATE")
  private LocalDateTime creationDate;

  @ManyToOne
  @JoinColumn(name = "PVA_N_REQUESTER_ID", nullable = false)
  private User requester;

  @ManyToOne
  @JoinColumn(name = "PVA_N_VALIDATOR_ID")
  private User validator;

  @Column(name = "PVA_V_STATUS")
  @Enumerated(EnumType.STRING)
  private Status status;

  @Column(name = "PVA_D_STATUS_DATE")
  private LocalDateTime statusDate;

  public PrivilegedAction(String type) {
    this.type = type;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public String getOperationName() {
    return operationName;
  }

  public void setOperationName(String operationName) {
    this.operationName = operationName;
  }

  public LocalDateTime getOperationDate() {
    return operationDate;
  }

  public void setOperationDate(LocalDateTime operationDate) {
    this.operationDate = operationDate;
  }

  public String getRejectionReason() {
    return rejectionReason;
  }

  public void setRejectionReason(String rejectionReason) {
    this.rejectionReason = rejectionReason;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(LocalDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public User getRequester() {
    return requester;
  }

  public void setRequester(User requester) {
    this.requester = requester;
  }

  public User getValidator() {
    return validator;
  }

  public void setValidator(User validator) {
    this.validator = validator;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public LocalDateTime getStatusDate() {
    return statusDate;
  }

  public void setStatusDate(LocalDateTime statusDate) {
    this.statusDate = statusDate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    return Objects.equals(id, ((PrivilegedAction) o).id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  /**
   * The possible status of an action.
   */
  public enum Status {

    /**
     * Submitted but not yet validated.
     */
    PENDING,

    /**
     * Accepted by the validator.
     */
    APPROVED,

    /**
     * Refused by the validator.
     */
    REJECTED;

    /**
     * Whether the action is in an state that can be updated.
     *
     * @return true if the status is {@link #PENDING}.
     */
    public boolean canUpdate() {
      return PENDING.equals(this);
    }
  }

}
