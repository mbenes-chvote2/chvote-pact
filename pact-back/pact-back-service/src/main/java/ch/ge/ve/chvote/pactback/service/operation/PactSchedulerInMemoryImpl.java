/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation;

import static java.util.Objects.requireNonNull;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A very simple implementation where the operations are only saved in memory.
 * <p>
 *   Designed to be used when there is no scheduler available, in TEST environments.
 *   Should not be used in production.
 * </p>
 */
public class PactSchedulerInMemoryImpl implements PactSchedulerApi {

  private final Map<String, LocalDateTime> repository = new ConcurrentHashMap<>();

  @Override
  public void register(String clientId, LocalDateTime scheduledTime) {
    repository.merge(requireNonNull(clientId), requireNonNull(scheduledTime),
                     PactSchedulerInMemoryImpl::failIfDifferent);
  }

  private static LocalDateTime failIfDifferent(LocalDateTime current, LocalDateTime newValue) {
    if (current.equals(newValue)) {
      return current;
    }
    throw new IllegalArgumentException("Already registered with " + current);
  }

  @Override
  public void unregister(String clientId) {
    repository.remove(requireNonNull(clientId));
  }

  /**
   * @return the registered identifiers and dates as a <em>read-only Map</em>.
   */
  public Map<String, LocalDateTime> readRepository() {
    return Collections.unmodifiableMap(repository);
  }
}
