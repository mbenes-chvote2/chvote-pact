/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration.printer;

import ch.ge.ve.chvote.pactback.contract.operation.PrinterConfigurationVo;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;

/**
 * A utility class for mapping {@link PrinterConfigurationVo} to {@link PrinterConfiguration}.
 */
public class PrinterConfigurationVoMapper {

  /**
   * Map the incoming printer configuration VO for switzerland residents only. It's equivalent to calling: {@link
   * #mapToPrinter(PrinterConfigurationVo, boolean)} with the second parameter as false.
   *
   * @param printerVo the incoming VO.
   *
   * @return the mapped printer configuration.
   */
  public static PrinterConfiguration mapToPrinter(
      PrinterConfigurationVo printerVo) {
    return mapToPrinter(printerVo, false);
  }

  /**
   * Map the incoming printer configuration VO.
   *
   * @param printerVo                        the incoming VO.
   * @param isSwissAbroadWithoutMunicipality whether the voting cards of this printer are for abroad residents or not.
   *
   * @return the mapped printer configuration.
   */
  public static PrinterConfiguration mapToPrinter(PrinterConfigurationVo printerVo,
                                                  boolean isSwissAbroadWithoutMunicipality) {
    PrinterConfiguration printer = new PrinterConfiguration();
    printer.setBusinessIdentifier(printerVo.getId());
    printer.setPrinterName(printerVo.getName());
    printer.setSwissAbroadWithoutMunicipality(isSwissAbroadWithoutMunicipality);
    printer.setPublicKey(printerVo.getPublicKey().toByteArray());
    return printer;
  }

  /**
   * Hide utility class constructor.
   */
  private PrinterConfigurationVoMapper() {
    throw new AssertionError("Not instantiable");
  }
}
