/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.materials;

import ch.ge.ve.chvote.pactback.contract.operation.RegisterFileEntryVo;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.VotersRegisterFile;
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties;
import ch.ge.ve.chvote.pactback.service.exception.AttachmentParsingException;
import ch.ge.ve.chvote.pactback.service.exception.InvalidVotingMaterialsConfigurationException;
import ch.ge.ve.chvote.pactback.service.operation.TimeService;
import ch.ge.ve.chvote.pactback.service.operation.configuration.attachment.AttachmentService;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.io.ByteStreams;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.xpath.XPathExpressionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

/**
 * A service for managing the attachments (voters registers) to voting materials.
 */
@Service
public class VotingMaterialsAttachmentService extends AttachmentService {

  private static final String NB_VOTERS = DELIVERY_ROOT + "/voterList/numberOfVoters";

  @Autowired
  public VotingMaterialsAttachmentService(TimeService timeService,
                                          ChVoteConfigurationProperties chVoteConfigurationProperties) {
    super(timeService, chVoteConfigurationProperties);
  }

  /**
   * Creates the list of voters register entities, by crossing the data in the specified list of {@link
   * RegisterFileEntryVo} with the data in the ZIP files.
   *
   * @param registerFilesCatalog the list of {@link RegisterFileEntryVo} metadata
   * @param zipFiles             the ZIP files, each containing one single XML voters register file
   */
  List<VotersRegisterFile> createVotersRegisterFiles(Collection<RegisterFileEntryVo> registerFilesCatalog,
                                                     Collection<MultipartFile> zipFiles) {
    checkConsistency(registerFilesCatalog, zipFiles);

    return zipFiles.stream()
                   .map(f -> mapToVotersRegisterFile(registerFilesCatalog, f))
                   .collect(Collectors.toList());
  }

  /**
   * Checks that the metadata for the voters ZIP files are consistent with the attached voters ZIP files.
   */
  @VisibleForTesting
  void checkConsistency(Collection<RegisterFileEntryVo> registerFileCatalog,
                        Collection<MultipartFile> zipFiles) {
    // check same number of files
    if (registerFileCatalog.size() != zipFiles.size()) {
      throw new InvalidVotingMaterialsConfigurationException(
          String.format(
              "The VotingMaterialConfiguration expects %d ZIP attachment(s) but %d ZIP attachment(s) are provided",
              registerFileCatalog.size(),
              zipFiles.size()));
    }

    // check consistency of file names
    Set<String> expectedZipFileNames = registerFileCatalog
        .stream()
        .map(RegisterFileEntryVo::getZipFileName)
        .collect(Collectors.toSet());
    Set<String> actualZipFileNames = zipFiles
        .stream()
        .map(MultipartFile::getOriginalFilename)
        .collect(Collectors.toSet());
    if (!actualZipFileNames.equals(expectedZipFileNames)) {
      throw new InvalidVotingMaterialsConfigurationException(
          String.format(
              "The ZIP file names in the actual attachments [%s] do not match the ZIP file names declared in the " +
              "metadata [%s]",
              actualZipFileNames,
              expectedZipFileNames));
    }
  }

  private VotersRegisterFile mapToVotersRegisterFile(Collection<RegisterFileEntryVo> registerFilesCatalog,
                                                     MultipartFile zipFile) {
    RegisterFileEntryVo registerFileVo = getRegisterFileEntryVo(registerFilesCatalog, zipFile.getOriginalFilename());

    VotersRegisterFile registerFile = new VotersRegisterFile();

    try (ZipInputStream zipStream = new ZipInputStream(zipFile.getInputStream())) {
      ZipEntry xmlEntry = zipStream.getNextEntry();
      byte[] fileContents = ByteStreams.toByteArray(zipStream);
      Document document = parse(zipFile.getOriginalFilename(), fileContents);
      populate(registerFile, AttachmentType.OPERATION_REGISTER, xmlEntry.getName(), registerFileVo.getImportDateTime(),
               fileContents,
               registerFileVo.getExternalIdentifier());
      populateInFileAttributes(registerFile, zipFile.getOriginalFilename(), document);
      registerFile.setNbOfVoters(Integer.parseInt(getXpathFactory().newXPath().compile(NB_VOTERS).evaluate(document)));
      zipStream.closeEntry();
    } catch (IOException | XPathExpressionException ex) {
      throw new AttachmentParsingException(
          String.format("An exception occurred while reading ZIP file [%s]", zipFile.getOriginalFilename()),
          ex);
    }

    return registerFile;
  }

  private RegisterFileEntryVo getRegisterFileEntryVo(Collection<RegisterFileEntryVo> registerFilesCatalog,
                                                     String zipFileName) {
    return registerFilesCatalog.stream()
                               .filter(r -> r.getZipFileName().equals(zipFileName))
                               .findFirst()
                               .orElseThrow(() -> new IllegalStateException(
                                   String.format("Unexpected error: no file found in catalog with name [%s]",
                                                 zipFileName)));
  }

}
