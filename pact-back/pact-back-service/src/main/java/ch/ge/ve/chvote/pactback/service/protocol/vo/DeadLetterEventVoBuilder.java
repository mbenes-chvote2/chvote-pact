/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.vo;

import java.time.LocalDateTime;

public final class DeadLetterEventVoBuilder {
  private String        protocolId;
  private String        originalChannel;
  private String        originalRoutingKey;
  private String        type;
  private String        exceptionMessage;
  private String        exceptionStacktrace;
  private LocalDateTime creationDate;
  private LocalDateTime pickupDate;
  private String        content;
  private String        contentType;

  DeadLetterEventVoBuilder() {
  }

  public DeadLetterEventVoBuilder setProtocolId(String protocolId) {
    this.protocolId = protocolId;
    return this;
  }

  public DeadLetterEventVoBuilder setOriginalChannel(String originalChannel) {
    this.originalChannel = originalChannel;
    return this;
  }

  public DeadLetterEventVoBuilder setOriginalRoutingKey(String originalRoutingKey) {
    this.originalRoutingKey = originalRoutingKey;
    return this;
  }

  public DeadLetterEventVoBuilder setType(String type) {
    this.type = type;
    return this;
  }

  public DeadLetterEventVoBuilder setExceptionMessage(String exceptionMessage) {
    this.exceptionMessage = exceptionMessage;
    return this;
  }

  public DeadLetterEventVoBuilder setExceptionStacktrace(String exceptionStacktrace) {
    this.exceptionStacktrace = exceptionStacktrace;
    return this;
  }

  public DeadLetterEventVoBuilder setCreationDate(LocalDateTime creationDate) {
    this.creationDate = creationDate;
    return this;
  }

  public DeadLetterEventVoBuilder setPickupDate(LocalDateTime pickupDate) {
    this.pickupDate = pickupDate;
    return this;
  }

  public DeadLetterEventVoBuilder setContent(String content) {
    this.content = content;
    return this;
  }

  public DeadLetterEventVoBuilder setContentType(String contentType) {
    this.contentType = contentType;
    return this;
  }

  public String getProtocolId() {
    return protocolId;
  }

  public String getOriginalChannel() {
    return originalChannel;
  }

  public String getOriginalRoutingKey() {
    return originalRoutingKey;
  }

  public String getType() {
    return type;
  }

  public String getExceptionMessage() {
    return exceptionMessage;
  }

  public String getExceptionStacktrace() {
    return exceptionStacktrace;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public LocalDateTime getPickupDate() {
    return pickupDate;
  }

  public String getContent() {
    return content;
  }

  public String getContentType() {
    return contentType;
  }

  public DeadLetterEventVo build() {
    return new DeadLetterEventVo(null, protocolId, originalChannel, originalRoutingKey, type, exceptionMessage,
                                 exceptionStacktrace, creationDate, pickupDate, content, contentType);
  }
}