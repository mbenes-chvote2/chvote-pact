/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.action.materials.VotingMaterialsCreationActionRepository;
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsCreation;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.VotingMaterialsConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.user.UserRepository;
import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import ch.ge.ve.chvote.pactback.service.action.vo.VotingMaterialsCreationVo;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyExistsException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException;
import ch.ge.ve.chvote.pactback.service.operation.OperationService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A specialised {@link BaseActionService} for {@link VotingMaterialsCreationVo}s.
 */
@Service
@Transactional(readOnly = true)
public class VotingMaterialsCreationService extends ActionCreationService<VotingMaterialsCreationVo> {

  private final VotingMaterialsCreationActionRepository                                    actionRepository;
  private final VotingMaterialsConfigurationRepository                                     materialsRepository;
  private final OperationService                                                           operationService;
  private final PendingActionRetriever<VotingMaterialsCreation, VotingMaterialsCreationVo> pendingActionRetriever;

  /**
   * Create a new {@link VotingMaterialsCreationService} with the given repositories.
   *
   * @param userRepository      the {@link User} repository.
   * @param materialsRepository the {@link VotingMaterialsConfiguration} repository.
   * @param actionRepository    the {@link VotingMaterialsCreation} repository.
   */
  public VotingMaterialsCreationService(UserRepository userRepository,
                                        VotingMaterialsConfigurationRepository materialsRepository,
                                        VotingMaterialsCreationActionRepository actionRepository,
                                        OperationService operationService) {
    super(userRepository);
    this.actionRepository = actionRepository;
    this.materialsRepository = materialsRepository;
    this.operationService = operationService;
    this.pendingActionRetriever = new PendingActionRetriever<>(actionRepository, this::mapToMaterialCreationVo);
  }

  @Override
  @Transactional(readOnly = true)
  public VotingMaterialsCreationVo getPendingPrivilegedAction(long id) {
    return pendingActionRetriever.getPendingPrivilegedActionVo(id);
  }

  @Override
  @Transactional(readOnly = true)
  public VotingMaterialsCreationVo getByBusinessId(Long businessId) {
    VotingMaterialsConfiguration configuration = materialsRepository.getOne(businessId);
    configuration.getCreationAction().ifPresent(action -> {
      if (!action.getStatus().canUpdate()) {
        throw new PrivilegedActionAlreadyProcessedException(action.getId());
      }
    });
    return mapToMaterialCreationVo(configuration);
  }

  @Override
  @Transactional
  public void createAction(Long businessId, String requesterName) {
    if (actionRepository.existsByVotingMaterialsConfiguration_Id(businessId)) {
      throw new PrivilegedActionAlreadyExistsException(
          "An action was already created for the VotingMaterialsConfiguration object with ID " + businessId);
    }

    VotingMaterialsConfiguration votingMaterialsConfiguration =
        materialsRepository.findById(businessId)
                           .orElseThrow(
                               () -> new EntityNotFoundException(
                                   "No VotingMaterialsConfiguration found for id: " + businessId));

    VotingMaterialsCreation action = new VotingMaterialsCreation();
    action.setVotingMaterialsConfiguration(votingMaterialsConfiguration);

    Operation operation = operationService.getOperationByVotingMaterialsConfigurationId(
        votingMaterialsConfiguration.getId());
    OperationConfiguration deployedConfiguration = operation.fetchDeployedConfiguration();

    actionRepository.save(populateNewAction(action, requesterName, deployedConfiguration.getOperationName(),
                                            deployedConfiguration.getOperationDate()));
  }

  private VotingMaterialsCreationVo mapToMaterialCreationVo(VotingMaterialsCreation action) {
    Operation operation = operationService.getOperationByVotingMaterialsConfigurationId(
        action.getVotingMaterialsConfiguration().getId());
    return new VotingMaterialsCreationVo(operation, action);
  }

  private VotingMaterialsCreationVo mapToMaterialCreationVo(VotingMaterialsConfiguration configuration) {
    Operation operation = operationService.getOperationByVotingMaterialsConfigurationId(configuration.getId());
    OperationConfiguration deployedConfiguration = operation.fetchDeployedConfiguration();
    return configuration.getCreationAction()
                        .map(action -> new VotingMaterialsCreationVo(operation, action))
                        .orElse(new VotingMaterialsCreationVo(operation, deployedConfiguration.getOperationName(),
                                                              deployedConfiguration.getOperationDate()));
  }

  @Override
  @Transactional(readOnly = true)
  public List<VotingMaterialsCreationVo> getNewOrPendingActions() {
    return materialsRepository
        .findAllByCreationActionIsNullOrCreationActionStatus(PrivilegedAction.Status.PENDING)
        .map(this::mapToMaterialCreationVo)
        .collect(Collectors.toList());
  }

  @Override
  @Transactional
  public void approveAction(long actionId, String validatorName) {
    User validator = getUserByName(validatorName);

    VotingMaterialsCreation action = pendingActionRetriever.getPendingPrivilegedAction(actionId);
    actionRepository.save(this.updatePrivilegedAction(action, validator, PrivilegedAction.Status.APPROVED));

    Operation operation = operationService.getOperationByVotingMaterialsConfigurationId(
        action.getVotingMaterialsConfiguration().getId());
    operationService.publishDeployedConfiguration(operation);
  }

  @Override
  @Transactional
  public void rejectAction(long actionId, String validatorName, String reason) {
    User validator = getUserByName(validatorName);

    VotingMaterialsCreation action = pendingActionRetriever.getPendingPrivilegedAction(actionId);
    actionRepository.save(this.updatePrivilegedAction(action, validator, PrivilegedAction.Status.REJECTED, reason));
  }
}
