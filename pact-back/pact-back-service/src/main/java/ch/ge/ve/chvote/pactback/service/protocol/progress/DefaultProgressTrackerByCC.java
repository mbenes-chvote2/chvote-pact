/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.progress;

import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService;
import ch.ge.ve.protocol.client.progress.api.ProgressTrackerByCC;

/**
 * A progress tracker strategy that persists the progress through a {@link ProtocolInstanceService}. This specialized
 * class keeps track of the progress for each control component of the protocol separately.
 */
public class DefaultProgressTrackerByCC extends ProgressTrackerByCC {
  private final Long                    protocolInstanceId;
  private final ProtocolStage           protocolStage;
  private final ProtocolInstanceService protocolInstanceService;

  /**
   * Create a new default progress tracker by control component.
   *
   * @param protocolInstanceId      the protocol instance id of the tasks to be monitored.
   * @param protocolStage           the {@link ProtocolStage}, i.e.: which task is going to be monitored.
   * @param protocolInstanceService the {@link ProtocolInstanceService} to persist changes in the progress.
   */
  public DefaultProgressTrackerByCC(Long protocolInstanceId,
                                    ProtocolStage protocolStage,
                                    ProtocolInstanceService protocolInstanceService) {
    this.protocolInstanceId = protocolInstanceId;
    this.protocolStage = protocolStage;
    this.protocolInstanceService = protocolInstanceService;
  }

  @Override
  protected void incrementProgress(int ccIndex, int done, int total) {
    protocolInstanceService.updateProtocolProgress(
        protocolInstanceId, protocolStage, ccIndex, done, total);
  }
}
