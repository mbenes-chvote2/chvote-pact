/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.service.exception.CannotCreateOrUpdateVotingPeriodConfigurationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A {@link ProtocolProcessorHandler} that sends the electoal officer public key to the production protocol instance.
 */
@Component
public class RealElectoralOfficerPublicKeyPublicationHandler extends ElectoralOfficerPublicKeyPublicationHandler {

  private final PublicKeyLoader publicKeyLoader;

  /**
   * Creates a new electoral officer public key publication handler for a production protocol instance.
   *
   * @param publicKeyLoader a service to lazily retrieve the public key.
   * @param objectMapper    a jackson Mapper object
   */
  @Autowired
  public RealElectoralOfficerPublicKeyPublicationHandler(ObjectMapper objectMapper,
                                                         PublicKeyLoader publicKeyLoader) {
    super(objectMapper);
    this.publicKeyLoader = publicKeyLoader;
  }

  private VotingPeriodConfiguration loadVotingPeriodConfiguration(ProtocolState state) {
    return state.withInstanceInTransaction(instance -> {
      Operation operation = instance.getOperation();
      return operation.getVotingPeriodConfiguration()
                      .orElseThrow(() -> CannotCreateOrUpdateVotingPeriodConfigurationException
                          .noConfigurationDeployed(operation.getClientId()));
    });
  }

  @Override
  protected byte[] getEncryptionPublicKeyStream(ProtocolState state) {
    return publicKeyLoader.loadPublicKey(loadVotingPeriodConfiguration(state).getId());
  }

}
