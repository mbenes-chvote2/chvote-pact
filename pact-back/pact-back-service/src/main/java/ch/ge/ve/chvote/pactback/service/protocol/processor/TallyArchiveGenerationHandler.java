/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.contract.tallyarchive.VotersPerCountingCircleVo;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import ch.ge.ve.chvote.pactback.service.operation.voter.VoterService;
import ch.ge.ve.protocol.support.ElectionVerificationDataWriterToFiles;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A {@link ProtocolProcessorHandler} that requests the shuffling and decryption of the results and writes them
 * to a tally archive.
 */
@Service
public class TallyArchiveGenerationHandler extends ProtocolInstanceStatusDrivenHandler {

  private final ObjectMapper                  objectMapper;
  private final OutputFilesManager            outputFilesManager;
  private final OperationReferenceFilesLoader operationReferenceFilesLoader;
  private final TallyArchiveGenerator         tallyArchiveGenerator;
  private final VoterService                  voterService;


  @Autowired
  public TallyArchiveGenerationHandler(ObjectMapper objectMapper,
                                       OutputFilesManager outputFilesManager,
                                       OperationReferenceFilesLoader operationReferenceFilesLoader,
                                       TallyArchiveGenerator tallyArchiveGenerator,
                                       VoterService voterService) {
    super(
        ProtocolInstanceStatus.RESULTS_AVAILABLE,
        ProtocolInstanceStatus.TALLY_ARCHIVE_GENERATION_FAILURE
    );
    this.objectMapper = objectMapper;
    this.outputFilesManager = outputFilesManager;
    this.operationReferenceFilesLoader = operationReferenceFilesLoader;
    this.tallyArchiveGenerator = tallyArchiveGenerator;
    this.voterService = voterService;
  }

  @Override
  protected void doProcess(ProtocolState state) throws IOException, VerificationDataWriterException {
    state.getClient().requestShufflingAndPartialDecryption();

    state.updateProtocolStatus(ProtocolInstanceStatus.SHUFFLING);
    state.getClient().ensureShufflesPublished(
        state.createProgressTracker(ProtocolStage.PUBLISH_SHUFFLE)
    );

    state.updateProtocolStatus(ProtocolInstanceStatus.DECRYPTING);
    state.getClient().ensurePartialDecryptionsPublished(
        state.createProgressTracker(ProtocolStage.PUBLISH_PARTIAL_DECRYPTION)
    );

    state.updateProtocolStatus(ProtocolInstanceStatus.GENERATING_RESULTS);
    Path outputDirectory = outputFilesManager.getOutputPath(state.getInstance());

    Path protocolClientPath = outputDirectory.resolve("electionVerificationData");

    // Setting the 'simulation' flag to true would cause the protocol client to calculate the tally with a preset
    // simulation key.
    ElectionVerificationDataWriterToFiles electionVerificationDataWriterToFiles =
        new ElectionVerificationDataWriterToFiles(protocolClientPath, objectMapper, false);
    state.getClient().writeProtocolAuditLog(electionVerificationDataWriterToFiles);

    Map<String, InputStream> repositoryStreamsByName =
        operationReferenceFilesLoader.loadRepositoriesByName(state.getConfiguration().getId());

    List<VotersPerCountingCircleVo> votersPerCountingCircles =
        voterService.countVotersPerDoiIdsPerCountingCircles(state.getInstance().getId());

    state.updateTallyArchiveLocation(
        outputFilesManager.relativize(
            tallyArchiveGenerator.createTallyArchive(outputDirectory,
                                                     electionVerificationDataWriterToFiles,
                                                     state.getConfiguration().getOperationName(),
                                                     repositoryStreamsByName,
                                                     votersPerCountingCircles)
        )

    );
  }
}
