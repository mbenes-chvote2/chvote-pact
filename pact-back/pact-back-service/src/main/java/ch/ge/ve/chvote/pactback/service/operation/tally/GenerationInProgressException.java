/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.tally;

/**
 * Exception thrown when the generation of the tally archive is requested for an operation for which the generation is
 * already in progress.
 */
public class GenerationInProgressException extends CannotPublishTallyArchiveException {
  private static final long serialVersionUID = 2506733828437255602L;

  /**
   * Create a new generation in progress exception.
   *
   * @param message the default message.
   *
   * @see RuntimeException#RuntimeException(String)
   */
  public GenerationInProgressException(String message) {
    super(message);
  }
}
