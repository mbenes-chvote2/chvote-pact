/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import com.google.common.util.concurrent.Runnables;
import java.util.Iterator;
import java.util.List;

/**
 * A chain of responsibility where it's nodes are {@link ProtocolProcessorHandler}s.
 */
public class ProtocolHandlerChain {
  private Runnable                           handleEndOfChain;
  private Iterator<ProtocolProcessorHandler> handlerIterator;

  /**
   * Creates a new protocol processor handler.
   *
   * @param chain all the processors that will be executed in order for this chain.
   */
  public ProtocolHandlerChain(List<ProtocolProcessorHandler> chain) {
    this(chain, Runnables.doNothing());
  }

  /**
   * Creates a new protocol processor handler that executes the given callback at the end of the chain.
   *
   * @param chain            all the processors that will be executed in order for this chain.
   * @param handleEndOfChain a callback that will be executed at the end of the chain.
   */
  public ProtocolHandlerChain(List<ProtocolProcessorHandler> chain, Runnable handleEndOfChain) {
    this.handlerIterator = chain.iterator();
    this.handleEndOfChain = handleEndOfChain;
  }

  /**
   * Executes the next {@link ProtocolProcessorHandler} in the chain.
   *
   * @param state the {@link ProtocolState} to process.
   */
  public void proceed(ProtocolState state) {
    if (handlerIterator.hasNext()) {
      handlerIterator.next().process(state, this);
    } else {
      handleEndOfChain.run();
    }
  }
}
