/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/
package ch.ge.ve.chvote.pactback.service.action.vo;

import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.entity.VotingPeriodInitialization;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.service.operation.period.vo.VotingPeriodConfigurationVo;
import java.time.LocalDateTime;

/**
 * A {@link VotingPeriodInitialization} value object.
 */
public class VotingPeriodInitializationVo extends PrivilegedActionVo {

  private final VotingPeriodConfigurationVo votingPeriodConfiguration;

  /**
   * Creates a new voting period initialization value object based on the given {@link VotingPeriodInitialization}
   * entity.
   *
   * @param operation the {@link VotingPeriodInitialization} entity. Its voting period configuration is expected to be
   *                  the voting period configuration of the action
   * @param action    the {@link VotingPeriodInitialization} entity
   */
  public VotingPeriodInitializationVo(Operation operation, VotingPeriodInitialization action) {
    super(action);
    this.votingPeriodConfiguration = new VotingPeriodConfigurationVo(operation);
  }

  /**
   * Creates a new voting period initialization value object based on the given {@link Operation} entity.
   *
   * @param operation     Its voting period configuration is expected to be the voting period configuration of the
   *                      action
   * @param operationName operation name
   * @param operationDate operation date
   */
  public VotingPeriodInitializationVo(Operation operation, String operationName, LocalDateTime operationDate) {
    super(VotingPeriodInitialization.TYPE,
          operationName,
          operationDate,
          operation.fetchVotingPeriodConfiguration().getAuthor(),
          operation.fetchVotingPeriodConfiguration().getSubmissionDate());
    this.votingPeriodConfiguration = new VotingPeriodConfigurationVo(operation);
  }

  public VotingPeriodConfigurationVo getVotingPeriodConfiguration() {
    return votingPeriodConfiguration;
  }
}
