/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration.attachment.vo;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile;
import java.time.LocalDateTime;

/**
 * A specialised {@link AttachmentVo} that represents an {@link OperationReferenceFile} value object.
 */
public class OperationReferenceFileVo extends AttachmentVo {
  private final String        messageId;
  private final String        sourceApplicationManufacturer;
  private final String        sourceApplicationProduct;
  private final String        sourceApplicationProductVersion;
  private final String        signatureAuthor;
  private final LocalDateTime signatureExpiryDate;
  private final String        signatureStatus;
  private final LocalDateTime generationDate;

  /**
   * Creates a new operation reference file value object based on the given {@link OperationReferenceFile} entity.
   *
   * @param attachment the {@link OperationReferenceFile} entity.
   */
  public OperationReferenceFileVo(OperationReferenceFile attachment) {
    super(attachment);
    this.messageId = attachment.getMessageId();
    this.sourceApplicationManufacturer = attachment.getSourceApplicationManufacturer();
    this.sourceApplicationProduct = attachment.getSourceApplicationProduct();
    this.sourceApplicationProductVersion = attachment.getSourceApplicationProductVersion();
    this.signatureAuthor = attachment.getSignatureAuthor();
    this.signatureExpiryDate = attachment.getSignatureExpiryDate();
    this.signatureStatus = attachment.getSignatureStatus() != null ? attachment.getSignatureStatus().name() : null;
    this.generationDate = attachment.getGenerationDate();
  }

  public String getMessageId() {
    return messageId;
  }

  public String getSourceApplicationManufacturer() {
    return sourceApplicationManufacturer;
  }

  public String getSourceApplicationProduct() {
    return sourceApplicationProduct;
  }

  public String getSourceApplicationProductVersion() {
    return sourceApplicationProductVersion;
  }

  public String getSignatureAuthor() {
    return signatureAuthor;
  }

  public LocalDateTime getSignatureExpiryDate() {
    return signatureExpiryDate;
  }

  public String getSignatureStatus() {
    return signatureStatus;
  }

  public LocalDateTime getGenerationDate() {
    return generationDate;
  }
}
