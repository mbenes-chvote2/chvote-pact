/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.OperationConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.Municipality;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.materials.VotingMaterialsConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import java.math.BigInteger;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class to load the printer configurations from the database.
 */
@Service
public class PrinterConfigurationsLoader {
  private final VotingMaterialsConfigurationRepository votingMaterialsConfigurationRepository;
  private final OperationConfigurationRepository       operationConfigurationRepository;

  public PrinterConfigurationsLoader(VotingMaterialsConfigurationRepository votingMaterialsConfigurationRepository,
                                     OperationConfigurationRepository operationConfigurationRepository) {
    this.votingMaterialsConfigurationRepository = votingMaterialsConfigurationRepository;
    this.operationConfigurationRepository = operationConfigurationRepository;
  }

  /**
   * Maps municipalities to their printer.
   *
   * @param votingMaterialsConfigurationId id of the voting material configuration.
   *
   * @return the mapping of municipalities to printers.
   */
  @Transactional(readOnly = true)
  public Map<Integer, String> generateMunicipalityToPrintersMap(Long votingMaterialsConfigurationId) {
    // need to reload the entity to be able to resolve the lazy links (this would not work with a detached entity)
    VotingMaterialsConfiguration votingMaterialsConfiguration = votingMaterialsConfigurationRepository
        .getOne(votingMaterialsConfigurationId);
    return generateMunicipalityToPrintersMap(votingMaterialsConfiguration.getPrinters());
  }

  /**
   * Maps municipalities to their printer for a test operation.
   *
   * @param operationConfigurationId id of the operation configuration.
   *
   * @return the mapping of municipalities to printers.
   */
  @Transactional(readOnly = true)
  public Map<Integer, String> generateTestMunicipalityToPrintersMap(Long operationConfigurationId) {
    // need to reload the entity to be able to resolve the lazy links (this would not work with a detached entity)
    OperationConfiguration configuration = operationConfigurationRepository.getOne(operationConfigurationId);
    return generateMunicipalityToPrintersMap(Collections.singletonList(configuration.getTestPrinter()));
  }

  private Map<Integer, String> generateMunicipalityToPrintersMap(List<PrinterConfiguration> printers) {
    Map<Integer, String> municipalityToPrinters = new HashMap<>();

    printers.forEach(
        printer ->
            municipalityToPrinters.putAll(printer.getMunicipalities().stream()
                                                 .collect(Collectors.toMap(Municipality::getOfsId,
                                                                           m -> printer.getBusinessIdentifier()))
            )
    );

    return municipalityToPrinters;
  }

  /**
   * Maps canton to its default printer for the swiss abroad without municipalities.
   *
   * @param votingMaterialsConfigurationId id of the voting material configuration.
   * @param canton                         canton for which to map the swiss abroad printer without municipalities.
   *
   * @return the list of {@code PrinterConfiguration}.
   */
  @Transactional(readOnly = true)
  public Map<String, String> generateCantonToPrintersMap(Long votingMaterialsConfigurationId, String canton) {
    // need to reload the entity to be able to resolve the lazy links (this would not work with a detached entity)
    VotingMaterialsConfiguration votingMaterialsConfiguration = votingMaterialsConfigurationRepository
        .getOne(votingMaterialsConfigurationId);
    return generateCantonToPrintersMap(votingMaterialsConfiguration.getSwissAbroadPrinter(), canton);
  }

  /**
   * Maps canton to its default printer for a test operation.
   *
   * @param operationConfigurationId id of the operation configuration.
   * @param canton                   canton for which the test operation.
   *
   * @return the list of {@code PrinterConfiguration}.
   */
  @Transactional(readOnly = true)
  public Map<String, String> generateTestCantonToPrintersMap(Long operationConfigurationId, String canton) {
    OperationConfiguration configuration = operationConfigurationRepository.getOne(operationConfigurationId);
    return generateCantonToPrintersMap(configuration.getTestPrinter(), canton);
  }

  private Map<String, String> generateCantonToPrintersMap(PrinterConfiguration printer, String canton) {
    Map<String, String> cantonToPrinters = new HashMap<>();
    if (printer != null) {
      cantonToPrinters.put(canton, printer.getBusinessIdentifier());
    }
    return cantonToPrinters;
  }

  /**
   * Retrieves the list of printing authorities with their public keys.
   *
   * @param votingMaterialsConfigurationId id of the voting material configuration.
   *
   * @return the list of {@code PrintingAuthorityWithPublicKey} for this voting material.
   */
  @Transactional(readOnly = true)
  public List<PrintingAuthorityWithPublicKey> createPrintingAuthority(Long votingMaterialsConfigurationId) {
    // need to reload the entity to be able to resolve the lazy links (this would not work with a detached entity)
    VotingMaterialsConfiguration votingMaterialsConfiguration = votingMaterialsConfigurationRepository
        .getOne(votingMaterialsConfigurationId);
    List<PrinterConfiguration> allPrinters = votingMaterialsConfiguration.getPrinters();

    if (votingMaterialsConfiguration.getSwissAbroadPrinter() != null) {
      allPrinters.add(votingMaterialsConfiguration.getSwissAbroadPrinter());
    }

    return allPrinters.stream()
                      .map(this::createPrintingAuthority)
                      .distinct()
                      .collect(Collectors.toList());
  }

  /**
   * Retrieves the list of printing authorities with their public keys for a test operation.
   *
   * @param operationConfigurationId id of the operation configuration.
   *
   * @return the list of {@code PrintingAuthorityWithCert} for this voting material.
   */
  @Transactional
  public List<PrintingAuthorityWithPublicKey> createTestPrintingAuthority(Long operationConfigurationId) {
    // need to reload the entity to be able to resolve the lazy links (this would not work with a detached entity)
    OperationConfiguration configuration = operationConfigurationRepository.getOne(operationConfigurationId);
    return Collections.singletonList(this.createPrintingAuthority(configuration.getTestPrinter()));
  }

  private PrintingAuthorityWithPublicKey createPrintingAuthority(PrinterConfiguration printerConfiguration) {
    return new PrintingAuthorityWithPublicKey(printerConfiguration.getBusinessIdentifier(),
                                              new BigInteger(printerConfiguration.getPublicKey()));
  }
}
