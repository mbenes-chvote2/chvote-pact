/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService;
import ch.ge.ve.chvote.pactback.service.protocol.client.ProtocolClientFactory;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * A {@link ProtocolState} factory.
 */
@Component
public class ProtocolStateFactory {
  private final ProtocolInstanceService instanceService;
  private final ProtocolClientFactory   protocolClientFactory;
  private final PublicParametersFactory publicParametersFactory;

  /**
   * Creates a new {@link ProtocolStateFactory}.
   *
   * @param instanceService       the {@link ProtocolInstanceService}.
   * @param protocolClientFactory the {@link ProtocolClientFactory}.
   */
  @Autowired
  public ProtocolStateFactory(ProtocolInstanceService instanceService,
                              ProtocolClientFactory protocolClientFactory,
                              @Value("${ch.ge.ve.security-level}") int securityLevel) {
    this.instanceService = instanceService;
    this.protocolClientFactory = protocolClientFactory;
    this.publicParametersFactory = PublicParametersFactory.forLevel(securityLevel);
  }

  /**
   * Create a new {@link ProtocolState} object for the given parameters.
   *
   * @param instance the {@link ProtocolInstance} of the nee state object.
   *
   * @return a new {@link ProtocolState}
   */
  public ProtocolState initProtocolState(ProtocolInstance instance) {
    return new ProtocolState(instance,
                             instanceService,
                             publicParametersFactory,
                             protocolClientFactory.createClient(instance.getProtocolId()));
  }
}
