/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.exception.InvalidApplicationConfigurationException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

/**
 * Service to store, retrieve and delete the files managed by the application (printer files, etc.). Manages only the
 * files stored on disk, not the files stored in the database (those are managed by the JPA entities).
 */
@Service
public class OutputFilesManager {

  private static final Logger logger = LoggerFactory.getLogger(OutputFilesManager.class);

  private final Path outputDir;

  public OutputFilesManager(@Value("${chvote.output-dir}") Path outputDir) {
    this.outputDir = outputDir;
  }

  /**
   * Gets the path of the directory where the files for the specified protocol instance are stored.
   *
   * @param protocolInstance protocol instance
   *
   * @return a directory path
   *
   * @throws InvalidApplicationConfigurationException if the base directory of this manager has not been configured
   */
  public Path getOutputPath(ProtocolInstance protocolInstance) {
    if (!outputDir.toFile().isDirectory()) {
      throw new InvalidApplicationConfigurationException(
          "No base directory for the output files of the protocol instances has been configured");
    }
    return outputDir.resolve(this.getRelativePath(protocolInstance));
  }

  /**
   * Gets the relative path of the directory where the files for the specified protocol instance are stored.
   *
   * @param protocolInstance protocol instance
   *
   * @return a directory path
   */
  public Path getRelativePath(ProtocolInstance protocolInstance) {
    return Paths.get("protocol", protocolInstance.getProtocolId());
  }

  /**
   * Deletes all files stored on disk for the specified protocol instance.
   */
  public void deleteAllFiles(ProtocolInstance protocolInstance) {
    Path dir = getOutputPath(protocolInstance);
    if (dir.toFile().exists()) {
      FileSystemUtils.deleteRecursively(dir.toFile());
    } else {
      logger.info("Could not find the output directory [{}] of protocol with instance ID [{}] and protocol ID " +
                  "[{}]. Likely cause: the protocol is being deleted before it had time to be (asynchronously) " +
                  "fully created",
                  dir,
                  protocolInstance.getId(),
                  protocolInstance.getProtocolId());
    }
  }

  public Path relativize(Path path) {
    return outputDir.relativize(path);
  }
}
