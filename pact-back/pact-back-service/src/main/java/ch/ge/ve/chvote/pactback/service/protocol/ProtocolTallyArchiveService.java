/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol;

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.exception.TallyArchiveGenerationException;
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolHandlerChain;
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolState;
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolStateFactory;
import ch.ge.ve.chvote.pactback.service.protocol.processor.TallyArchiveGenerationHandler;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * A service that triggers the tallying of a {@link ProtocolInstance}.
 */
@Service
public class ProtocolTallyArchiveService {
  private static final Logger log = LoggerFactory.getLogger(ProtocolInitializationService.class);

  /**
   * Minimum number of confirmed votes required by the protocol to initiate the tally process.
   */
  private static final long MINIMUM_NUMBER_OF_CONFIRMED_VOTES_TO_INITIATE_TALLY = 2L;

  private final ProtocolStateFactory          protocolStateFactory;
  private final TallyArchiveGenerationHandler tallyArchiveGenerationHandler;

  /**
   * Create a new {@link ProtocolTallyArchiveService}.
   *
   * @param protocolStateFactory          the {@link ProtocolStateFactory}.
   * @param tallyArchiveGenerationHandler the {@link TallyArchiveGenerationHandler}.
   */
  @Autowired
  public ProtocolTallyArchiveService(ProtocolStateFactory protocolStateFactory,
                                     TallyArchiveGenerationHandler tallyArchiveGenerationHandler) {
    this.protocolStateFactory = protocolStateFactory;
    this.tallyArchiveGenerationHandler = tallyArchiveGenerationHandler;
  }

  private void execute(ProtocolHandlerChain chain, ProtocolState state) {
    try {
      chain.proceed(state);
    } catch (Exception e) {
      log.error("Tallying failed", e);
      tallyArchiveGenerationHandler.indicateFailure(state);
      throw new TallyArchiveGenerationException(
          String.format("Unable to finish tallying for operation [%s]", state.getOperation().getClientId()), e);
    }
  }

  /**
   * Initiate the tallying process for the given {@link ProtocolInstance}.
   *
   * @param instance the {@link ProtocolInstance}.
   */
  @Async
  public void startTallying(ProtocolInstance instance) {
    ProtocolState state = protocolStateFactory.initProtocolState(instance);
    if (state.getClient().getVotesCastCount() < MINIMUM_NUMBER_OF_CONFIRMED_VOTES_TO_INITIATE_TALLY) {
      state.updateProtocolStatus(ProtocolInstanceStatus.NOT_ENOUGH_VOTES_TO_INITIATE_TALLY);
    } else {
      ProtocolHandlerChain chain = new ProtocolHandlerChain(Collections.singletonList(tallyArchiveGenerationHandler));
      execute(chain, state);
    }
  }
}
