/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.contract.printerarchive.ElectionConfigurationVo;
import ch.ge.ve.chvote.pactback.contract.printerarchive.PrinterOperationConfigurationVo;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.ElectionSiteConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import ch.ge.ve.chvote.pactback.service.exception.PrinterArchivesGenerationException;
import ch.ge.ve.model.convert.api.ElectionConverter;
import ch.ge.ve.model.convert.api.ElectionSetBuilder;
import ch.ge.ve.model.convert.api.VotationConverter;
import ch.ge.ve.model.convert.api.VoterConverter;
import ch.ge.ve.model.convert.model.ElectionConfiguration;
import ch.ge.ve.model.convert.model.ElectionDefinition;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import ch.ge.ve.protocol.model.Voter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A {@link ProtocolProcessorHandler} that requests the credential generation.
 */
public abstract class CredentialsGenerationHandler extends ProtocolProcessorHandler {
  private static final Logger log = LoggerFactory.getLogger(CredentialsGenerationHandler.class);

  final         OperationReferenceFilesLoader operationReferenceFilesLoader;
  private final OutputFilesManager            outputFilesManager;
  private final VoterConverter                voterConverter;
  private final PrinterArchiveGenerator       printerArchiveGenerator;
  private final VotationConverter             votationConverter;
  private final ElectionConverter             electionConverter;

  /**
   * Create a new credentials generation handler.
   *
   * @param votationConverter             the {@link VotationConverter}.
   * @param electionConverter             the {@link ElectionConverter}.
   * @param operationReferenceFilesLoader the {@link OperationReferenceFilesLoader}.
   * @param outputFilesManager            the {@link OutputFilesManager}.
   * @param voterConverter                the {@link VoterConverter}.
   * @param printerArchiveGenerator       the {@link PrinterArchiveGenerator}.
   */
  CredentialsGenerationHandler(VotationConverter votationConverter,
                               ElectionConverter electionConverter,
                               OperationReferenceFilesLoader operationReferenceFilesLoader,
                               OutputFilesManager outputFilesManager,
                               VoterConverter voterConverter,
                               PrinterArchiveGenerator printerArchiveGenerator) {
    this.votationConverter = votationConverter;
    this.electionConverter = electionConverter;
    this.operationReferenceFilesLoader = operationReferenceFilesLoader;
    this.outputFilesManager = outputFilesManager;
    this.voterConverter = voterConverter;
    this.printerArchiveGenerator = printerArchiveGenerator;
  }

  private ElectionSetWithPublicKey getElectionSet(ProtocolState state, List<Voter> voters) {
    ElectionSetBuilder builder = new ElectionSetBuilder();
    List<ElectionDefinition> electionDefinitions = getVotationDefinitions(state);
    electionDefinitions.addAll(getElectionDefinitions(state));
    builder.addElectionDefinitions(electionDefinitions);
    builder.addPrintingAuthorities(getPrintingAuthorities(state));
    builder.setVoterCount(voters.size());
    builder.setCountingCircleCount((int) voters.stream().map(Voter::getCountingCircle).distinct().count());

    return builder.build();
  }

  private List<ElectionDefinition> getVotationDefinitions(ProtocolState state) {
    InputStream[] inputStreams =
        operationReferenceFilesLoader.loadAttachments(
            state.getConfiguration().getId(),
            AttachmentType.OPERATION_REPOSITORY_VOTATION
        );

    return votationConverter.convertToElectionDefinitionList(inputStreams);
  }

  private List<ElectionDefinition> getElectionDefinitions(ProtocolState state) {
    InputStream[] inputStreams =
        operationReferenceFilesLoader.loadAttachments(
            state.getConfiguration().getId(),
            AttachmentType.OPERATION_REPOSITORY_ELECTION
        );

    List<ElectionConfiguration> electionConfigurations =
        state.withInstanceInTransaction(
            protocolInstance ->
                protocolInstance.getConfiguration()
                                .getElectionSiteConfigurations()
                                .stream()
                                .map(this::toProtocolElectionConfiguration)
                                .collect(Collectors.toList()));

    return electionConverter.convertToElectionDefinitionList(electionConfigurations, inputStreams);
  }


  @Override
  protected void process(ProtocolState state) {
    final VotersCreationHelper votersHelper = createHelper(state, voterConverter);

    final List<Voter> voters = votersHelper.getVotersList();
    final ElectionSetWithPublicKey electionSet = getElectionSet(state, voters);

    state.getClient().sendElectionSet(electionSet);
    state.updateProtocolStatus(ProtocolInstanceStatus.GENERATING_CREDENTIALS);
    state.getClient().ensureElectionSetForwardedToCCs(state.createProgressTracker(ProtocolStage.SEND_ELECTION_SET));
    state.getClient().ensurePrimesSentToBB(state.createProgressTracker(ProtocolStage.PUBLISH_PRIMES));

    final LocalDateTime printerArchiveCreationDate = LocalDateTime.now();
    state.getClient().sendVoters(voters, 25);
    state.updateCreationStatistics(votersHelper.getCreationStatistics());
    state.getClient().ensureVotersForwardedToCCs(state.createProgressTracker(ProtocolStage.SEND_VOTERS));
    state.getClient().ensurePublicCredentialsBuiltForAllCCs(
        state.createProgressTracker(ProtocolStage.BUILD_PUBLIC_CREDENTIALS));

    Map<String, Long> nbrOfVotersByPrinter = votersHelper.getNumberOfVotersByPrinter();

    List<PrintingAuthorityWithPublicKey> printingAuthorities = getPrintingAuthorities(state)
        .stream()
        .filter(printingAuthority -> nbrOfVotersByPrinter.containsKey(printingAuthority.getName()))
        .collect(Collectors.toList());

    final Path outputPath = outputFilesManager.getOutputPath(state.getInstance());
    final PrinterArchiveGenerator.Packing packing =
        printerArchiveGenerator.prepareForPrinters(outputPath, printingAuthorities);
    try {
      packing.setPublicParameters(state.getInstance().getPublicParameters());
      packing.setElectionSet(electionSet);
      packing.writeRemappedVoterFiles(votersHelper.getMunicipalityToPrinter(),
                                        votersHelper.getPrintingAuthoritiesByCanton(),
                                        nbrOfVotersByPrinter,
                                        votersHelper.getRegisterInputStreams());

      state.getClient()
           .requestPrivateCredentials(outputPath,
                                      packing::setOnePrinterFile,
                                      state.createProgressTrackerByCC(ProtocolStage.REQUEST_PRIVATE_CREDENTIALS));
      state.getClient().ensurePrivateCredentialsPublished();

      packing.writeOperationRepositories(
          operationReferenceFilesLoader.loadRepositoriesByName(state.getConfiguration().getId()));

      packing.setElectionConfigurations(state.withInstanceInTransaction(
          protocolInstance -> protocolInstance.getConfiguration().getElectionSiteConfigurations().stream()
                                              .map(this::toElectionConfigurationVo)
                                              .collect(Collectors.toList())
      ));

      packing.setOperationConfiguration(toPrinterOperationConfigurationVo(state));

    } catch (IOException e) {
      log.error("Failed to generate printer files", e);
      throw new PrinterArchivesGenerationException(
          String.format("Unable to generate printer files for operation [%s]", state.getOperation().getClientId()), e);
    }

    final String operationLabel = state.getConfiguration().getOperationLabel();
    state.updatePrinterArchiveCreationDate(printerArchiveCreationDate);
    packing.createPrinterArchives(operationLabel, printerArchiveCreationDate);

    state.updateProtocolStatus(ProtocolInstanceStatus.CREDENTIALS_GENERATED);
  }

  private ElectionConfigurationVo toElectionConfigurationVo(ElectionSiteConfiguration config) {
    return new ElectionConfigurationVo(config.getBallot(), config.isDisplayVoidOnEmptyBallotPaper());
  }

  private ElectionConfiguration toProtocolElectionConfiguration(ElectionSiteConfiguration config) {
    return new ElectionConfiguration(config.getBallot(), config.isDisplayVoidOnEmptyBallotPaper());
  }

  protected abstract VotersCreationHelper createHelper(ProtocolState state, VoterConverter voterConverter);

  protected abstract List<PrintingAuthorityWithPublicKey> getPrintingAuthorities(ProtocolState state);

  protected abstract PrinterOperationConfigurationVo toPrinterOperationConfigurationVo(ProtocolState state);

  @Override
  public boolean hasBeenProcessed(ProtocolState state) {
    return state.isProtocolInstanceStatusAtOrAfter(ProtocolInstanceStatus.CREDENTIALS_GENERATED);
  }

}
