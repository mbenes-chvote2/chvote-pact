/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import static com.google.common.base.Preconditions.checkState;

import ch.ge.ve.chvote.pactback.contract.printerarchive.ElectionConfigurationVo;
import ch.ge.ve.chvote.pactback.contract.printerarchive.PrinterOperationConfigurationVo;
import ch.ge.ve.chvote.pactback.service.exception.PrinterArchivesGenerationException;
import ch.ge.ve.filenamer.EpfFileName;
import ch.ge.ve.filenamer.PrinterArchiveFileName;
import ch.ge.ve.filenamer.archive.PrinterArchiveMaker;
import ch.ge.ve.model.convert.api.VoterConverter;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.PrintingAuthority;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Closer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This component handles the generation of printer archives.
 */
@Component
public class PrinterArchiveGenerator {
  private static final Logger log = LoggerFactory.getLogger(PrinterArchiveGenerator.class);

  private static final String PRINTERS_ROOT_FOLDER_NAME        = "printerFiles";
  private static final String COMMON_FOLDER_NAME               = "commonFiles";
  private static final String PRINTER_FOLDER_TEMPLATE          = "printer-archive-%s";
  private static final String PUBLIC_PARAMETERS_FILENAME       = "public-parameters.json";
  private static final String ELECTION_SET_FILENAME            = "election-set.json";
  private static final String ELECTION_CONFIGURATIONS_FILENAME = "election-configurations.json";
  private static final String OPERATION_CONFIGURATION_FILENAME = "operation-configuration.json";

  private final VoterConverter voterConverter;
  private final ObjectMapper   objectMapper;

  @Autowired
  public PrinterArchiveGenerator(VoterConverter voterConverter, ObjectMapper objectMapper) {
    this.voterConverter = voterConverter;
    this.objectMapper = objectMapper;
  }

  /**
   * Get a packing helper object to prepare, for an operation, the archives for all printers.
   * <p>
   * The generation process is currently done as multiple steps, each of them consisting of the creation - and temporary
   * storage - of a piece of the printer archive's content. <br> This packing object will hold references to everything
   * configured for that operation, so that the archives' content can be defined step by step - up to the archives
   * generation.
   * </p>
   * <p>
   * A tree structure will be generated from the parentPath provided, to temporarily store each document until the
   * archives are created - and where the archives will be made available. <br> The tree structure would be in the
   * following form:
   * <pre>
   *     parentDirectory
   *            +-- printerFiles <em>(will be deleted and created anew if it already exists)</em>
   *            |        +-- printer-archive-<i>$printerName</i> (1 per printingAuthority)
   *            |
   *            +-- commonFiles  <em>(stores all files that are common to all printers)</em>
   *   </pre>
   *
   * @param parentDirectory     a root directory where we can store all temp files and ultimately generate the archives
   * @param printingAuthorities the printing authorities for which archives should be generated
   *
   * @return a new {@code Packing} helper object
   */
  public Packing prepareForPrinters(Path parentDirectory, Collection<? extends PrintingAuthority> printingAuthorities) {
    try {
      return new Packing(parentDirectory, ImmutableList.copyOf(printingAuthorities));
    } catch (IOException e) {
      throw new PrinterArchivesGenerationException("Failed to prepare archives generation", e);
    }
  }

  /**
   * Retrieves the path of the archive containing the printer files for the given printer name, from the given parent
   * path
   *
   * @param operationLabel             the operation's label
   * @param parentPath                 the root relative to which the target path is computed
   * @param printerBusinessId          the business identifier of the printer
   * @param printerArchiveCreationDate the common creation date for all the printer archives
   *
   * @return the path to the archive containing the given printer
   */
  // It might be a better idea to store this archivePath (instead of the printerArchiveCreationDate) for further access
  public Path getArchivePath(String operationLabel, Path parentPath, String printerBusinessId,
                             LocalDateTime printerArchiveCreationDate) {
    return getPrintersRootFolder(parentPath)
        .resolve(
            new PrinterArchiveFileName(printerBusinessId, operationLabel, printerArchiveCreationDate).toString()
        );
  }

  private static Path getPrintersRootFolder(Path parentPath) {
    return parentPath.resolve(PRINTERS_ROOT_FOLDER_NAME);
  }

  /**
   * This helper class holds references to the directories, printers and every registered input throughout the
   * generation process.
   * <p>
   * By keeping the state of the progression like such, we can make intermediary methods' signatures less cumbersome. It
   * also allows to register every piece of data to be added to the archive directly to {@link PrinterArchiveMaker}s, as
   * soon as they are defined here.
   * </p>
   *
   * @see #prepareForPrinters(Path, Collection)
   */
  public class Packing {
    private final Path baseDirectory;
    private final Path commonDirectory;

    private final List<? extends PrintingAuthority> printingAuthorities;
    private final Map<String, PrinterArchiveMaker>  makers;

    private Packing(Path baseDirectory, List<? extends PrintingAuthority> printingAuthorities) throws IOException {
      this.baseDirectory = baseDirectory;
      this.printingAuthorities = printingAuthorities;

      this.commonDirectory = ensureEmptyDirectory(baseDirectory.resolve(COMMON_FOLDER_NAME));
      this.makers = new HashMap<>();

      this.mkPrinterDirs();
      this.prepareArchiveMakers();
    }

    // Prepare the directories for the printers, cleaning if necessary
    private void mkPrinterDirs() throws IOException {
      log.info("Creating printer archives directories");
      final Path rootFolder = getPrintersRootFolder(baseDirectory);
      ensureEmptyDirectory(rootFolder);

      for (PrintingAuthority printer : printingAuthorities) {
        final Path printerDirectory = getPrinterDirectory(printer.getName());
        log.debug("creating directory {}", printerDirectory);
        Files.createDirectory(printerDirectory);
      }
    }

    private void prepareArchiveMakers() {
      for (PrintingAuthority printer : printingAuthorities) {
        makers.put(printer.getName(), new PrinterArchiveMaker(printer.getName()));
      }
    }

    private Path ensureEmptyDirectory(Path directory) throws IOException {
      if (directory.toFile().exists()) {
        log.debug("Removing previous data from \"{}\"", directory);
        recursiveDelete(directory);
      }
      Files.createDirectories(directory);
      log.info("Created empty directory \"{}\"", directory);

      return directory;
    }

    private void recursiveDelete(Path directory) throws IOException {
      try (Stream<Path> fileStream = Files.walk(directory)) {
        boolean allDeleted = fileStream.map(Path::toFile)
                                       .sorted(Comparator.reverseOrder())
                                       .reduce(true, (status, file) -> status && file.delete(), Boolean::logicalAnd);

        if (!allDeleted) {
          throw new PrinterArchivesGenerationException("Failed to clear directory " + directory);
        }
      }
    }

    /**
     * Take the supplied {@link InputStream}s containing all eCH-0045 data, and write one eCH-0045 file per printer.
     * <p>
     * Voter information may be split across several eCH-0045 files, according to various business rules. Here, we need
     * the voter information to be regrouped into a single file per printing authority, so that printers only need to
     * open and parse a single eCH-0045 file, and have no visibility on the private information pertaining to voters
     * they are not responsible for printing the voting materials for.
     * </p>
     * <p>
     * Each generated eCH-0045 file will be placed in the corresponding printer directory.
     * </p>
     *
     * @param printingAuthorityByMunicipality a mapping defining which printing authority is responsible for each
     *                                        municipality
     * @param printingAuthorityByCanton       a mapping defining which printing authority is responsible for each canton
     *                                        (only needed for the Swiss living abroad, for which the eCH-0045 format
     *                                        allows for the omission of the municipality information)
     * @param nbrOfVotersByPrinter            the number of voters by printer.
     * @param voterDeliveries                 a supplier providing fresh input streams for all the eCH-0045 files on
     *                                        each call
     *
     * @throws IOException if an error occurs while reading from the input streams or writing the newly generated files
     */
    public void writeRemappedVoterFiles(Map<Integer, String> printingAuthorityByMunicipality,
                                        Map<String, String> printingAuthorityByCanton,
                                        Map<String, Long> nbrOfVotersByPrinter,
                                        InputStream[] voterDeliveries) throws IOException {
      log.info("remapping voter files");
      final Map<String, OutputStream> outputStreamMap = new HashMap<>();
      // Use Guava's Closer to close all outputStreams after being written by the converter
      try (Closer closer = Closer.create()) {
        for (PrintingAuthority pA : printingAuthorities) {
          String printerName = pA.getName();
          Path outputPath = getPrinterDirectory(printerName).resolve("eCH-0045.xml");
          outputStreamMap.put(printerName, closer.register(Files.newOutputStream(outputPath)));
          log.debug("Data for printer {} will be written at {}", printerName, outputPath);

          makers.get(printerName).withVotersReferenceSource(outputPath);
        }

        voterConverter.remapByPrinter(outputStreamMap, printingAuthorityByMunicipality, printingAuthorityByCanton,
                                      nbrOfVotersByPrinter, voterDeliveries);
      }
    }

    /**
     * Define the public parameters used by the protocol to generate the voters' credentials
     *
     * @param publicParameters the (previously serialized) public parameters
     *
     * @throws IOException if an error occurs on writing the file
     */
    public void setPublicParameters(String publicParameters) throws IOException {
      log.info("writing public parameters to common folder");
      Path outputPath = commonDirectory.resolve(PUBLIC_PARAMETERS_FILENAME);
      Files.write(outputPath, publicParameters.getBytes(StandardCharsets.UTF_8));

      makers.values().forEach(m -> m.withPublicParametersSource(outputPath));
    }

    /**
     * Define the election set used by the protocol to generate the voters' credentials
     *
     * @param electionSet the election set object
     *
     * @throws IOException if an I/O error occurs
     */
    public void setElectionSet(ElectionSetWithPublicKey electionSet) throws IOException {
      log.info("writing election set to common folder");
      Path outputPath = commonDirectory.resolve(ELECTION_SET_FILENAME);
      try (OutputStream outputStream = Files.newOutputStream(outputPath)) {
        objectMapper.writeValue(outputStream, electionSet);
      }

      makers.values().forEach(m -> m.withElectionSetSource(outputPath));
    }

    /**
     * Define one printer file (epf).
     * <p>
     * Multiple printer files might be generated per printer (typically one series per control component, split in
     * voters batches). This method gathers them one by one, extracting the printer name from the convention used in the
     * file name - and register it to be included in the right printer archive.
     * </p>
     * <p>
     * Implementation note : the protocol client retrieves the printer files from the protocol system when saving the
     * printer file. This requires a {@code Consumer<Path>} to handle each printer file - this method is a good
     * candidate for that.
     * </p>
     *
     * @param protocolClientPath the path to the file which should be moved
     *
     * @throws PrinterArchivesGenerationException if the input filename doesn't match the expected template, or an
     *                                            {@link IOException} occurs during processing
     */
    public void setOnePrinterFile(Path protocolClientPath) {
      final String printerName = EpfFileName.parse(protocolClientPath).getPrintingAuthorityName();
      final Path printerDirectory = getPrinterDirectory(printerName);
      checkState(Files.isDirectory(printerDirectory), "Printer directory %s not found", printerDirectory);

      try {
        final Path toPath = printerDirectory.resolve(protocolClientPath.getFileName());
        log.info("Moving file {} to {}", protocolClientPath, toPath);
        Files.move(protocolClientPath, toPath);

        makers.get(printerName).addPrivateCredentialsSource(toPath);
      } catch (IOException e) {
        throw new PrinterArchivesGenerationException("failed to copy encrypted file", e);
      }
    }

    /**
     * Writes the operation repositories to each printer's archive preparation folder
     *
     * @param repositoryStreamsByName one {@link InputStream} per operation repository, keyed by filename
     *
     * @throws IOException if an I/O error occurs
     */
    public void writeOperationRepositories(Map<String, InputStream> repositoryStreamsByName) throws IOException {
      log.info("Writing operation repositories to common folder");

      List<Path> allRepositories = new ArrayList<>(repositoryStreamsByName.size());
      for (Map.Entry<String, InputStream> entry : repositoryStreamsByName.entrySet()) {
        final Path outputPath = commonDirectory.resolve(entry.getKey());
        Files.copy(entry.getValue(), outputPath);

        allRepositories.add(outputPath);
      }

      makers.values().forEach(m -> m.withOperationReferenceSources(allRepositories));
    }

    /**
     * Define the given election configurations specified by the back-office.
     *
     * @param electionConfigurations the election configurations to write.
     *
     * @throws IOException if an I/O error occurs
     */
    public void setElectionConfigurations(List<ElectionConfigurationVo> electionConfigurations) throws IOException {
      log.info("Writing election configurations to common folder");
      Path outputPath = commonDirectory.resolve(ELECTION_CONFIGURATIONS_FILENAME);
      try (OutputStream out = Files.newOutputStream(outputPath)) {
        objectMapper.writeValue(out, electionConfigurations);
      }

      makers.values().forEach(m -> m.withElectionConfigurationsSource(outputPath));
    }

    /**
     * Define the given operation configuration specified by the back-office.
     *
     * @param operationConfiguration the operation configurations to write.
     *
     * @throws IOException if an I/O error occurs
     */
    public void setOperationConfiguration(PrinterOperationConfigurationVo operationConfiguration) throws IOException {
      log.info("Writing operation configuration to common folder");
      Path outputPath = commonDirectory.resolve(OPERATION_CONFIGURATION_FILENAME);
      try (OutputStream out = Files.newOutputStream(outputPath)) {
        objectMapper.writeValue(out, operationConfiguration);
      }

      makers.values().forEach(m -> m.withOperationConfigurationSource(outputPath));
    }

    /**
     * Create the printer archives, based on the files previously created.
     * <p>
     * <p>
     * A printer archive contains:
     * </p>
     * <ul>
     * <li>the eCH-0157 and/or eCH-0159 files defining the elections being run</li>
     * <li>one eCH-0045 xml file, containing the voter data needed for the printing of the voting cards;</li>
     * <li>one encrypted printer file per control component, containing the private credentials for those same voters
     * .</li>
     * </ul>
     *
     * @param operationLabel             the operation's label
     * @param printerArchiveCreationDate the common creation date for all the archives
     *
     * @throws IllegalStateException              if the printer archive directories have not been generated first
     * @throws PrinterArchivesGenerationException if the archives fail to be generated
     */
    public void createPrinterArchives(String operationLabel, LocalDateTime printerArchiveCreationDate) {
      final Path outputDir = getPrintersRootFolder(baseDirectory);
      for (Map.Entry<String, PrinterArchiveMaker> entry : makers.entrySet()) {
        log.debug("Writing archive for printer {} to directory {}, (replace if exists!)", entry.getKey(), outputDir);
        Path archivePath = entry.getValue()
                                .withOperationName(operationLabel)
                                .withCreationDate(printerArchiveCreationDate)
                                .make(outputDir);
        log.info("Printer archive created : {}", archivePath.getFileName());
      }
    }

    private Path getPrinterDirectory(String printingAuthorityName) {
      final Path rootFolder = getPrintersRootFolder(baseDirectory);
      try {
        return rootFolder.resolve(String.format(PRINTER_FOLDER_TEMPLATE,
                                                URLEncoder.encode(printingAuthorityName, "UTF-8"))).normalize();
      } catch (UnsupportedEncodingException e) {
        throw new PrinterArchivesGenerationException("failed to encode printing authority name", e);
      }
    }
  }
}
