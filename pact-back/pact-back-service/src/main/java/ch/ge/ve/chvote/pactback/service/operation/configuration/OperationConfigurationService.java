/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration;

import ch.ge.ve.chvote.pactback.contract.operation.ElectionSiteConfigurationVo;
import ch.ge.ve.chvote.pactback.contract.operation.OperationConfigurationSubmissionVo;
import ch.ge.ve.chvote.pactback.contract.operation.TestSitePrinter;
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.OperationConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.ElectionSiteConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.operation.configuration.printer.MunicipalityVoMapper;
import ch.ge.ve.chvote.pactback.service.operation.configuration.printer.PrinterConfigurationVoMapper;
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * A service for retrieving and modifying {@link OperationConfiguration} entities.
 */
@Service
public class OperationConfigurationService {

  private final OperationConfigurationRepository configurationRepository;

  private final OperationConfigurationAttachmentService attachmentService;

  private final ProtocolInstanceService protocolInstanceService;

  /**
   * Create a new operation configuration service.
   *
   * @param configurationRepository the {@link OperationConfiguration} repository.
   * @param attachmentService       the attachment service.
   * @param protocolInstanceService the {@link ProtocolInstance} service
   */
  @Autowired
  public OperationConfigurationService(OperationConfigurationRepository configurationRepository,
                                       OperationConfigurationAttachmentService attachmentService,
                                       ProtocolInstanceService protocolInstanceService) {
    this.configurationRepository = configurationRepository;
    this.attachmentService = attachmentService;
    this.protocolInstanceService = protocolInstanceService;
  }

  /**
   * Build a new {@link OperationConfiguration} for the given {@link OperationConfigurationSubmissionVo} and attachments
   * ZIP file.
   *
   * @param vo             the {@link OperationConfigurationSubmissionVo}.
   * @param zipAttachments the attachments file in a formatted ZIP.
   *
   * @return The non-persisted {@link OperationConfiguration}.
   */
  public OperationConfiguration createOperationConfiguration(OperationConfigurationSubmissionVo vo,
                                                             List<MultipartFile> zipAttachments) {
    OperationConfiguration operationConfiguration = new OperationConfiguration();

    operationConfiguration = attachmentService
        .populateAttachments(operationConfiguration, vo.getBallotDocumentations(), vo.getHighlightedQuestions(),
                             vo.getAttachments(), zipAttachments);

    operationConfiguration.setOperationDate(vo.getOperationDate());
    operationConfiguration.setLastChangeUser(vo.getUser());
    operationConfiguration.setLastChangeDate(LocalDateTime.now());
    operationConfiguration.setSiteTextHash("8f3899aead04b608c0d444a50714bc997fcc23028d4e71ce8e9323c17a798ba1");
    operationConfiguration.setOperationName(vo.getOperationName());
    operationConfiguration
        .setOperationLabel(vo.getOperationLabel() != null ? vo.getOperationLabel() : vo.getOperationName());
    operationConfiguration.setGracePeriod(vo.getGracePeriod());
    operationConfiguration.setCanton(vo.getCanton());
    operationConfiguration.setGroupVotation(vo.isGroupVotation());

    TestSitePrinter testSitePrinter = vo.getTestSitePrinter();

    PrinterConfiguration printerConfiguration = PrinterConfigurationVoMapper.mapToPrinter(testSitePrinter.getPrinter());
    printerConfiguration.setMunicipalities(
        Collections.singletonList(MunicipalityVoMapper.mapToRealMunicipality(testSitePrinter.getMunicipality()))
    );
    operationConfiguration.setTestPrinter(printerConfiguration);

    operationConfiguration.setElectionSiteConfigurations(vo.getElectionSiteConfigurations().stream()
                                                           .map(this::toElectionSiteConfiguration)
                                                           .collect(Collectors.toList()));

    operationConfiguration
        .setSiteOpeningDate(vo.getMilestones().get(OperationConfigurationSubmissionVo.Milestone.SITE_OPEN));
    operationConfiguration
        .setSiteClosingDate(vo.getMilestones().get(OperationConfigurationSubmissionVo.Milestone.SITE_CLOSE));
    operationConfiguration.setValidationStatus(ValidationStatus.NOT_YET_VALIDATED);

    return operationConfiguration;
  }

  private ElectionSiteConfiguration toElectionSiteConfiguration(ElectionSiteConfigurationVo vo) {
    ElectionSiteConfiguration entity = new ElectionSiteConfiguration();
    entity.setBallot(vo.getBallot());
    entity.setDisplayCandidateSearchForm(vo.isDisplayCandidateSearchForm());
    entity.setAllowChangeOfElectoralList(vo.isAllowChangeOfElectoralList());
    entity.setDisplayEmptyPosition(vo.isDisplayEmptyPosition());
    entity.setDisplayCandidatePositionOnACompactBallotPaper(vo.isDisplayCandidatePositionOnACompactBallotPaper());
    entity.setDisplayCandidatePositionOnAModifiedBallotPaper(vo.isDisplayCandidatePositionOnAModifiedBallotPaper());
    entity.setDisplaySuffrageCount(vo.isDisplaySuffrageCount());
    entity.setDisplayListVerificationCode(vo.isDisplayListVerificationCode());
    entity.setAllowOpenCandidature(vo.isAllowOpenCandidature());
    entity.setAllowMultipleMandates(vo.isAllowMultipleMandates());
    entity.setDisplayVoidOnEmptyBallotPaper(vo.isDisplayVoidOnEmptyBallotPaper());
    entity.setCandidateInformationDisplayModel(vo.getCandidateInformationDisplayModel());
    entity.setDisplayedColumnsOnVerificationTable(vo.getDisplayedColumnsOnVerificationTable());
    entity.setColumnsOrderOnVerificationTable(vo.getColumnsOrderOnVerificationTable());
    return entity;
  }

  /**
   * Invalidates the {@link OperationConfiguration} currently deployed in test for the given {@link Operation}.
   *
   * @param operation the {@link Operation} to invalidate.
   * @param user      the username of the user performing the invalidation.
   *
   * @throws EntityNotFoundException if there is no operation configuration in test.
   * @throws IllegalStateException   If the configuration has already been validated or its in production.
   */
  @Transactional
  public void invalidateLatestOperationConfiguration(Operation operation, String user) {
    OperationConfiguration operationConfiguration =
        operation.getConfigurationInTest()
                 .orElseThrow(() -> new EntityNotFoundException("There is no configuration to validate"));

    if (ValidationStatus.NOT_YET_VALIDATED != operationConfiguration.getValidationStatus()) {
      throw new IllegalStateException("Configuration may not be invalidated unless it currently is in validation");
    }

    operationConfiguration.setValidationStatus(ValidationStatus.INVALIDATED);
    operationConfiguration.setLastChangeUser(user);
    operationConfiguration.setLastChangeDate(LocalDateTime.now());

    operationConfiguration.getProtocolInstance().ifPresent(instance -> {
      // Defensive check
      if (ProtocolEnvironment.PRODUCTION == instance.getEnvironment()) {
        throw new IllegalStateException(
            "Should never have a protocol instance in production if the configuration has" +
            "not been validated first");
      }
      protocolInstanceService.deleteProtocolInstance(instance);
      operationConfiguration.setProtocolInstance(null);
    });

    configurationRepository.save(operationConfiguration);
  }

  /**
   * Validates the {@link OperationConfiguration} currently deployed in test for the given {@link Operation}.
   *
   * @param operation the {@link Operation} to validate.
   * @param user      the username of the user performing the validation.
   */
  @Transactional
  public void validateLatestOperationConfiguration(Operation operation, String user) {
    OperationConfiguration operationConfiguration =
        operation.getConfigurationInTest()
                 .orElseThrow(() -> new EntityNotFoundException("There is no configuration to validate"));
    if (ValidationStatus.NOT_YET_VALIDATED != operationConfiguration.getValidationStatus()) {
      throw new IllegalStateException("Configuration may not be validated unless it currently is in validation");
    }
    Optional<ProtocolInstance> protocolInstance = operationConfiguration.getProtocolInstance();
    if (!protocolInstance.isPresent()) {
      // Defensive check
      throw new IllegalStateException("Configuration may not be validated if no protocol has been instantiated");
    } else if (ProtocolEnvironment.PRODUCTION == protocolInstance.get().getEnvironment()) {
      // Defensive check
      throw new IllegalStateException("Configuration may not be validated if the protocol is running in production");
    }

    operationConfiguration.setValidationStatus(ValidationStatus.VALIDATED);
    operationConfiguration.setLastChangeUser(user);
    operationConfiguration.setLastChangeDate(LocalDateTime.now());

    configurationRepository.save(operationConfiguration);
  }

}
