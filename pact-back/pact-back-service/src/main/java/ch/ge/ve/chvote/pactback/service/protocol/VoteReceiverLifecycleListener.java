/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol;


import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZED;

import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolDeletionEvent;
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolStatusChangeEvent;
import ch.ge.ve.chvote.pactback.service.receiver.VoteReceiverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Listens to the changes of {@code ProtocolInstance}s and notify the VoteReceiver.
 *
 * @deprecated Should use pact-notification-events library.
 */
@Deprecated
@Component
public class VoteReceiverLifecycleListener {
  private static final Logger logger = LoggerFactory.getLogger(VoteReceiverLifecycleListener.class);

  private final VoteReceiverService voteReceiverService;

  @Autowired
  public VoteReceiverLifecycleListener(VoteReceiverService voteReceiverService) {
    this.voteReceiverService = voteReceiverService;
  }

  /**
   * When a protocol instance is initialized with all necessary data ready, the VoteReceiver should be notified so that
   * it can fetch the information and initialize a voting site on its side.
   *
   * @param event raised when the status changed - will be checked to transition to "ELECTION_OFFICER_KEY_INITIALIZED"
   */
  @EventListener
  public void notifyProtocolDeployed(ProtocolStatusChangeEvent event) {
    if (event.getStatusAfter() == ELECTION_OFFICER_KEY_INITIALIZED) {
      // notify the VoteReceiver that the voting site is ready to get initialized
      final String protocolId = event.getProtocolInstance().getProtocolId();
      logger.info("Send deploy notification for protocol id = {}", protocolId);
      voteReceiverService.deployInstance(protocolId);
    }
  }

  /**
   * When a protocol instance is deleted, the VoteReceiver should be notified so that it can remove the associated
   * voting site (if any) as well.
   *
   * @param event raised when a {@code ProtocolInstance} is deleted
   */
  @EventListener
  public void notifyProtocolDeleted(ProtocolDeletionEvent event) {
    voteReceiverService.destroyInstance(event.getProtocolInstance().getProtocolId());
  }
}
