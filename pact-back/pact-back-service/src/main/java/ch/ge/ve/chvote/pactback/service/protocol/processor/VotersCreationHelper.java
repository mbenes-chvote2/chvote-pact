/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats;
import ch.ge.ve.chvote.pactback.service.operation.voter.Batch;
import ch.ge.ve.chvote.pactback.service.operation.voter.VoterBatchService;
import ch.ge.ve.chvote.pactback.service.operation.voter.vo.VoterCountingCircleVo;
import ch.ge.ve.chvote.pactback.service.operation.voter.vo.DateOfBirthVo;
import ch.ge.ve.chvote.pactback.service.operation.voter.vo.VoterVo;
import ch.ge.ve.model.convert.api.StatsCounter;
import ch.ge.ve.model.convert.api.VoterConverter;
import ch.ge.ve.model.convert.model.VoterDetails;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import ch.ge.ve.protocol.model.Voter;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Helper that keeps the state and regroups access to it while in the process of generating the voters list.
 */
class VotersCreationHelper {

  private final ProtocolInstance                protocolInstance;
  private final VoterBatchService               voterService;
  private final VoterConverter                  voterConverter;
  private final Map<Integer, String>            municipalityToPrinter;
  private final Map<String, String>             printingAuthoritiesByCanton;
  private final Supplier<InputStream[]>         registerInputStreamsSupplier;
  private final StatsCounter<Voter, StatsGroup> counter = new StatsCounter<>(StatsGroup::fromVoter);

  private List<Voter> votersList;

  /**
   * Creation of a Helper instance. One instance is meant to generate one Voters list.
   *
   * @param voterService                 the voter service.
   * @param voterConverter               model converter - generates the Voters from the InputStreams
   * @param protocolInstance             the protocol instance.
   * @param municipalityToPrinter        mapping of the printers by municipalityId - required by the model converter
   * @param printingAuthoritiesByCanton  mapping of the printers by canton - required by the model converter
   * @param registerInputStreamsSupplier a supplier to initialize the streams containing the register data
   */
  public VotersCreationHelper(VoterBatchService voterService,
                              VoterConverter voterConverter,
                              ProtocolInstance protocolInstance,
                              Map<Integer, String> municipalityToPrinter,
                              Map<String, String> printingAuthoritiesByCanton,
                              Supplier<InputStream[]> registerInputStreamsSupplier) {
    this.protocolInstance = protocolInstance;
    this.voterService = voterService;
    this.voterConverter = voterConverter;
    this.municipalityToPrinter = municipalityToPrinter;
    this.printingAuthoritiesByCanton = printingAuthoritiesByCanton;
    this.registerInputStreamsSupplier = registerInputStreamsSupplier;
  }

  private void createAndPersistVoters() {
    Batch<VoterVo> batch = voterService.startBatchInsert(protocolInstance.getId());
    this.votersList = voterConverter.convertToVoterList(municipalityToPrinter,
                                                        printingAuthoritiesByCanton,
                                                        getRegisterInputStreams())
                                    .peek(voter -> batch.add(toVoterVo(voter)))
                                    .map(this::toVoter)
                                    .peek(counter::append)
                                    .collect(Collectors.toList());
    batch.flush();
  }

  private Voter toVoter(VoterDetails voter) {
    return new Voter(voter.getId(), voter.getRegisterPersonId(),
                     voter.getCountingCircle(), voter.getPrintingAuthorityName(),
                     voter.getAllowedDomainsOfInfluence().toArray(new DomainOfInfluence[0]));
  }

  private VoterVo toVoterVo(VoterDetails voter) {
    DateOfBirthVo dateOfBirth = new DateOfBirthVo(voter.getDateOfBirth().getYear(),
                                                  voter.getDateOfBirth().getMonthOfYear().orElse(null),
                                                  voter.getDateOfBirth().getDayOfMonth().orElse(null));
    VoterCountingCircleVo countingCircle = new VoterCountingCircleVo(voter.getCountingCircle().getId(),
                                                                     voter.getCountingCircle().getBusinessId(),
                                                                     voter.getCountingCircle().getName());
    List<String> doiIds = voter.getAllowedDomainsOfInfluence().stream()
                               .map(DomainOfInfluence::getIdentifier).collect(Collectors.toList());
    return new VoterVo(voter.getId(), voter.getRegisterPersonId(), dateOfBirth, countingCircle,
                       voter.getPrintingAuthorityName(), doiIds);
  }

  /**
   * Generates the {@link Voter}s list, based on given context and the register streams.
   * <p>
   * This list is a lazy-initialized attribute : it is only computed the first time it is accessed, then the
   * same list is returned for every subsequent call.
   * </p>
   *
   * @return the list of Voter objects
   */
  public List<Voter> getVotersList() {
    if (votersList == null) {
      createAndPersistVoters();
    }
    return votersList;
  }

  /**
   * Initialize the register streams as configured.
   * <p>
   * The streams are initialized for each call, so that they can be consumed and closed
   * (at the caller's discretion though).
   * </p>
   *
   * @return the InputStream holding the register content
   */
  public InputStream[] getRegisterInputStreams() {
    return registerInputStreamsSupplier.get();
  }

  public Map<Integer, String> getMunicipalityToPrinter() {
    return municipalityToPrinter;
  }

  public Map<String, String> getPrintingAuthoritiesByCanton() {
    return printingAuthoritiesByCanton;
  }

  /**
   * @return the number of voters created, mapped by associated printer's name
   */
  public Map<String, Long> getNumberOfVotersByPrinter() {
    return counter.asAggregatedMap(group -> group.printerName);
  }

  /**
   * Create the statistics of how many voters have been created, per printer name and counting circle
   *
   * @return a list of statistics entities
   *
   * @see VotersCreationStats
   */
  public List<VotersCreationStats> getCreationStatistics() {
    return counter.stream()
                  .map(entry -> {
                    final VotersCreationStats stats = new VotersCreationStats();
                    stats.setPrinterAuthorityName(entry.getKey().printerName);
                    stats.setCountingCircleId(entry.getKey().countingCircleId);
                    stats.setCountingCircleName(entry.getKey().countingCircleName);
                    stats.setCountingCircleBusinessId(entry.getKey().countingCircleBusinessId);
                    stats.setNumberOfVoters(entry.getValue());

                    return stats;
                  })
                  .collect(Collectors.toList());
  }

  // A grouping value object for the statistics - unexposed outside this component
  private static class StatsGroup {
    private final String printerName;
    private final String countingCircleName;
    private final String countingCircleBusinessId;
    private final int    countingCircleId;

    StatsGroup(String printerName, String countingCircleName, String countingCircleBusinessId, int countingCircleId) {
      this.printerName = printerName;
      this.countingCircleName = countingCircleName;
      this.countingCircleBusinessId = countingCircleBusinessId;
      this.countingCircleId = countingCircleId;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      StatsGroup that = (StatsGroup) o;
      return countingCircleId == that.countingCircleId &&
             Objects.equals(printerName, that.printerName);
    }

    @Override
    public int hashCode() {
      return Objects.hash(printerName, countingCircleId);
    }

    static StatsGroup fromVoter(Voter voter) {
      return new StatsGroup(voter.getPrintingAuthorityName(),
                            voter.getCountingCircle().getName(),
                            voter.getCountingCircle().getBusinessId(),
                            voter.getCountingCircle().getId());
    }
  }
}
