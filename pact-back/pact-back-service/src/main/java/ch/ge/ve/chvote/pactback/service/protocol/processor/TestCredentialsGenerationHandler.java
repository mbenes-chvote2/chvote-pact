/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.contract.printerarchive.PrinterOperationConfigurationVo;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.service.exception.CannotDeployOperationConfigurationException;
import ch.ge.ve.chvote.pactback.service.operation.voter.VoterBatchService;
import ch.ge.ve.model.convert.api.ElectionConverter;
import ch.ge.ve.model.convert.api.VotationConverter;
import ch.ge.ve.model.convert.api.VoterConverter;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import java.io.InputStream;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A protocol processor handler for the generation of test credentials.
 */
@Component
public class TestCredentialsGenerationHandler extends CredentialsGenerationHandler {
  private static final String TEST_CARD_LABEL = "Test cards";

  private final PrinterConfigurationsLoader printerConfigurationsLoader;
  private final VoterBatchService           voterBatchService;

  /**
   * Create a new credentials handler for a protocol instance in test.
   *
   * @param voterBatchService             the {@link VoterBatchService}.
   * @param votationConverter             the {@link VotationConverter}.
   * @param electionConverter             the {@link ElectionConverter}.
   * @param voterConverter                the {@link VoterConverter}.
   * @param printerConfigurationsLoader   a service to lazily retrieve the printer configurations
   * @param operationReferenceFilesLoader the {@link OperationReferenceFilesLoader}
   * @param outputFilesManager            the {@link OutputFilesManager}
   * @param printerArchiveGenerator       the {@link PrinterArchiveGenerator}
   */
  @Autowired
  public TestCredentialsGenerationHandler(VoterBatchService voterBatchService,
                                          VotationConverter votationConverter,
                                          ElectionConverter electionConverter,
                                          VoterConverter voterConverter,
                                          PrinterConfigurationsLoader printerConfigurationsLoader,
                                          OperationReferenceFilesLoader operationReferenceFilesLoader,
                                          OutputFilesManager outputFilesManager,
                                          PrinterArchiveGenerator printerArchiveGenerator) {
    super(votationConverter, electionConverter, operationReferenceFilesLoader, outputFilesManager, voterConverter,
          printerArchiveGenerator);
    this.voterBatchService = voterBatchService;
    this.printerConfigurationsLoader = printerConfigurationsLoader;
  }

  private InputStream[] getRegisterInputStreams(Long configurationId) {

    return operationReferenceFilesLoader.loadAttachments(configurationId, AttachmentType.OPERATION_REGISTER);
  }

  @Override
  protected VotersCreationHelper createHelper(ProtocolState state, VoterConverter voterConverter) {
    final OperationConfiguration configuration = state.getConfiguration();

    return new VotersCreationHelper(
        voterBatchService,
        voterConverter,
        state.getInstance(),
        printerConfigurationsLoader.generateTestMunicipalityToPrintersMap(configuration.getId()),
        printerConfigurationsLoader.generateTestCantonToPrintersMap(configuration.getId(), configuration.getCanton()),
        () -> getRegisterInputStreams(configuration.getId()));
  }

  @Override
  protected List<PrintingAuthorityWithPublicKey> getPrintingAuthorities(ProtocolState state) {
    OperationConfiguration configuration =
        state.getOperation()
             .getConfigurationInTest()
             .orElseThrow(() -> new CannotDeployOperationConfigurationException(
                 "No configuration in test defined yet"));

    return printerConfigurationsLoader.createTestPrintingAuthority(configuration.getId());
  }

  @Override
  protected PrinterOperationConfigurationVo toPrinterOperationConfigurationVo(ProtocolState state) {
    return new PrinterOperationConfigurationVo(state.getConfiguration().getOperationName(), TEST_CARD_LABEL);
  }

}
