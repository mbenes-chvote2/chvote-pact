/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.operation.voter.VoterBatchService
import ch.ge.ve.chvote.pactback.service.operation.voter.VoterService
import ch.ge.ve.model.convert.api.VoterConverter
import ch.ge.ve.model.convert.model.PartialLocalDate
import ch.ge.ve.model.convert.model.VoterDetails
import ch.ge.ve.protocol.model.CountingCircle
import java.time.Year
import java.util.concurrent.atomic.AtomicInteger
import java.util.function.Supplier
import spock.lang.Specification

class VotersCreationHelperTest extends Specification {

  private final AtomicInteger idGenerator = new AtomicInteger()

  VoterConverter voterConverter
  Supplier<InputStream[]> registerInputStreamsSupplier
  final Map<Integer, String> municipalityToPrinter = [:]
  final Map<String, String> printingAuthoritiesByCanton = [:]
  final ProtocolInstance protocolInstance = new ProtocolInstance(id: 1L) // we only need the id here...


  VotersCreationHelper helper
  VoterService voterService
  VoterBatchService voterBatchService

  void setup() {
    voterConverter = Mock()
    registerInputStreamsSupplier = Mock()
    voterService = Mock()
    voterBatchService = new VoterBatchService(voterService, 10)

    helper = new VotersCreationHelper(voterBatchService, voterConverter, protocolInstance,
                                      municipalityToPrinter, printingAuthoritiesByCanton, registerInputStreamsSupplier)
  }

  // static factory method to create a Voter adapted to those tests
  def newVoterForPrinterAndCountingCircle(String printerName, int countingCircleId, String coutningCircleBusinessId) {
    int id = idGenerator.incrementAndGet()
    return new VoterDetails(id, "voter $id", new PartialLocalDate(Year.of(1984)),
                            new CountingCircle(countingCircleId, coutningCircleBusinessId, ""), printerName, [])
  }

  def "getVotersList should delegate the generation to voterConverter and persist the generated voters"() {
    given:
    def modelVotersList = [
            newVoterForPrinterAndCountingCircle("printer1", 1, "1001"),
            newVoterForPrinterAndCountingCircle("printer1", 1, "1001"),
            newVoterForPrinterAndCountingCircle("printer1", 1, "1001")
    ]
    voterConverter.convertToVoterList(municipalityToPrinter, printingAuthoritiesByCanton, _) >> {
      modelVotersList.stream()
    }

    when:
    def votersList = helper.getVotersList()

    then:
    (1.._) * voterService.saveAll(protocolInstance.id, _)
    votersList*.voterId == modelVotersList*.id
  }

  def "getVotersList should only be really computed once"() {
    given:
    def modelVotersList = [newVoterForPrinterAndCountingCircle("printer1", 1, "1001")]
    voterConverter.convertToVoterList(municipalityToPrinter, printingAuthoritiesByCanton, _) >> {
      modelVotersList.stream()
    }

    when:
    def firstAccessList = helper.getVotersList()
    def secondAccessList = helper.getVotersList()

    then:
    firstAccessList.is secondAccessList
    // registerInputStreams should only have been accessed once
    1 * registerInputStreamsSupplier.get()
  }

  // To document the fact that those InputStream should not be memoized
  def "GetRegisterInputStreams should call the supplier each time it is solicited"() {
    when:
    helper.getRegisterInputStreams()
    helper.getRegisterInputStreams()

    then:
    2 * registerInputStreamsSupplier.get()
  }

  // We briefly verify the statistics but without getting into too much details, as we do not need to test StatsCounter
  def "GetCreationStatistics should extract the statistics entities"() {
    given:
    def modelVotersList = [
            newVoterForPrinterAndCountingCircle("printer1", 1, "1001"),
            newVoterForPrinterAndCountingCircle("printer1", 1, "1001"),
            newVoterForPrinterAndCountingCircle("printer1", 2, "1002"),
            newVoterForPrinterAndCountingCircle("printer1", 1, "1001"),
            newVoterForPrinterAndCountingCircle("printer2", 5, "1005"),
            newVoterForPrinterAndCountingCircle("printer2", 5, "1005")
    ]
    voterConverter.convertToVoterList(municipalityToPrinter, printingAuthoritiesByCanton, _) >> {
      modelVotersList.stream()
    }
    forceGeneration()

    when:
    def statistics = helper.getCreationStatistics()

    then:
    statistics.toSorted { s1, s2 ->
      def comp = (s1.printerAuthorityName <=> s2.printerAuthorityName)
      return comp == 0 ? (s1.countingCircleId <=> s2.countingCircleId) : comp

    }.collect { [it.printerAuthorityName, it.countingCircleId, it.numberOfVoters, it.countingCircleBusinessId] } == [
            [ "printer1", 1, 3L, "1001" ],
            [ "printer1", 2, 1L, "1002" ],
            [ "printer2", 5, 2L, "1005" ]
    ]
  }

  // We briefly verify the statistics but without getting into too much details, as we do not need to test StatsCounter
  def "GetNumberOfVotersByPrinter should aggregate the statistics"() {
    given:
    def modelVotersList = [
            newVoterForPrinterAndCountingCircle("printer 1", 1, "1001"),
            newVoterForPrinterAndCountingCircle("printer 1", 1, "1001"),
            newVoterForPrinterAndCountingCircle("printer 1", 2, "1002"),
            newVoterForPrinterAndCountingCircle("printer 1", 1, "1001"),
            newVoterForPrinterAndCountingCircle("printer 2", 5, "1005"),
            newVoterForPrinterAndCountingCircle("printer 2", 5, "1005")
    ]
    voterConverter.convertToVoterList(municipalityToPrinter, printingAuthoritiesByCanton, _) >> {
      modelVotersList.stream()
    }
    forceGeneration()

    when:
    def mapping = helper.getNumberOfVotersByPrinter()

    then:
    mapping == ["printer 1": 4L, "printer 2": 2L]
  }

  def forceGeneration() {
    helper.getVotersList()
  }
}
