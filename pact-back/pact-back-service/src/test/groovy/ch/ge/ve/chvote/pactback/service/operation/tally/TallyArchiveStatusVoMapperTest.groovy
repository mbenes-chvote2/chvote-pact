/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.tally

import ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import spock.lang.Specification

class TallyArchiveStatusVoMapperTest extends Specification {

  TallyArchiveStatusVoMapper mapper

  def setup() {
    mapper = new TallyArchiveStatusVoMapper()
  }

  def "should map the expected state for each protocol instance status"() {
    given:
    def instance = new ProtocolInstance()
    def configuration = new OperationConfiguration()
    instance.configuration = configuration
    instance.status = status

    when:
    def result = mapper.map(instance)

    then:
    result.state == expectedState

    where:
    status                                                  || expectedState
    ProtocolInstanceStatus.CREDENTIALS_GENERATED            || TallyArchiveStatusVo.State.NOT_REQUESTED
    ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES           || TallyArchiveStatusVo.State.NOT_REQUESTED
    ProtocolInstanceStatus.DECRYPTING                       || TallyArchiveStatusVo.State.CREATION_IN_PROGRESS
    ProtocolInstanceStatus.SHUFFLING                        || TallyArchiveStatusVo.State.CREATION_IN_PROGRESS
    ProtocolInstanceStatus.RESULTS_AVAILABLE                || TallyArchiveStatusVo.State.CREATED
    ProtocolInstanceStatus.TALLY_ARCHIVE_GENERATION_FAILURE || TallyArchiveStatusVo.State.CREATION_FAILED
  }
}
