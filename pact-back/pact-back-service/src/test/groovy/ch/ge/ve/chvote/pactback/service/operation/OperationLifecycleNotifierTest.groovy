/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation

import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.CREDENTIALS_GENERATED
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.GENERATING_CREDENTIALS
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.INITIALIZING
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.PUBLIC_PARAMETERS_INITIALIZED

import ch.ge.ve.chvote.pactback.notification.event.VotingMaterialsGeneratedEvent
import ch.ge.ve.chvote.pactback.notification.publisher.NotificationEventsPublisher
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolStatusChangeEvent
import spock.lang.Specification

class OperationLifecycleNotifierTest extends Specification {
  private NotificationEventsPublisher publisher
  private OperationLifecycleNotifier notifier

  def setup() {
    publisher = Mock(NotificationEventsPublisher)
    notifier = new OperationLifecycleNotifier(publisher)
  }

  private String protocolId = "sample_protocol_id_1234"

  def "notifyVotingMaterialsReady should trigger the publisher if the status transitions to 'CREDENTIALS_GENERATED'"() {
    given:
    def instance = new ProtocolInstance(protocolId: protocolId)
    def event = new ProtocolStatusChangeEvent(instance, GENERATING_CREDENTIALS, CREDENTIALS_GENERATED)

    when:
    notifier.notifyVotingMaterialsReady(event)

    then:
    1 * publisher.publish(*_) >> { arguments ->
      (arguments[0] as VotingMaterialsGeneratedEvent).protocolId == protocolId
    }
  }

  def "notifyVotingMaterialsReady should not trigger the publisher if the status transitions to any value other than 'CREDENTIALS_GENERATED'"() {
    given:
    def instance = new ProtocolInstance(protocolId: protocolId)
    def event = new ProtocolStatusChangeEvent(instance, INITIALIZING, PUBLIC_PARAMETERS_INITIALIZED)

    when:
    notifier.notifyVotingMaterialsReady(event)

    then:
    0 * publisher._
  }
}
