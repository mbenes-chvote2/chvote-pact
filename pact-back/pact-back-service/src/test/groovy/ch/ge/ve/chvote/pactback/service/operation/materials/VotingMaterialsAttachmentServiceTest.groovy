/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.materials

import ch.ge.ve.chvote.pactback.contract.operation.RegisterFileEntryVo
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile.SignatureStatus
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.VotersRegisterFile
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties
import ch.ge.ve.chvote.pactback.service.exception.InvalidVotingMaterialsConfigurationException
import ch.ge.ve.chvote.pactback.service.operation.TimeService
import java.time.LocalDateTime
import java.time.ZoneId
import javax.xml.bind.DatatypeConverter
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartFile
import spock.lang.Specification

class VotingMaterialsAttachmentServiceTest extends Specification {

  public static final String TIMEZONE = "Europe/Zurich"

  private VotingMaterialsAttachmentService service

  private TimeService timeService
  private LocalDateTime dateNow

  void setup() {
    dateNow = LocalDateTime.now()

    timeService = Mock(TimeService)
    timeService.now() >> dateNow

    def chVoteConfigurationProperties = Mock(ChVoteConfigurationProperties)
    chVoteConfigurationProperties.timezone >> TIMEZONE
    service = new VotingMaterialsAttachmentService(timeService, chVoteConfigurationProperties)
  }

  def "should parse the two ZIP files and populate the VotingMaterialsConfiguration entity"() {
    given:
    List<RegisterFileEntryVo> registerFileVos = createRegisterFilesCatalog()

    def is1 = this.class.getClassLoader().getResourceAsStream("attachments/votersRegister1.zip")
    MultipartFile file1 = new MockMultipartFile("reg1", "votersRegister1.zip", null, is1)
    def is2 = this.class.getClassLoader().getResourceAsStream("attachments/votersRegister2.zip")
    MultipartFile file2 = new MockMultipartFile("reg2", "votersRegister2.zip", null, is2)

    when:
    List<VotersRegisterFile> registerFiles = service.createVotersRegisterFiles(registerFileVos, Arrays.asList(file1, file2))

    then:
    registerFiles.size() == 2
    registerFiles[0].nbOfVoters == 1
    registerFiles[0].name == "votersRegister1.xml"
    registerFiles[0].type == AttachmentType.OPERATION_REGISTER
    registerFiles[0].file.size() == 5497
    registerFiles[0].fileHash.startsWith("a9e1ed66f80f21")
    registerFiles[0].messageId == "register1"
    registerFiles[0].signatureStatus == SignatureStatus.INVALID_SIGNATURE
    registerFiles[0].importDate == dateNow.minusDays(1)
    registerFiles[0].generationDate == parseGenerationDate("2017-10-20T15:21:57.481")
    registerFiles[1].nbOfVoters == 100
    registerFiles[1].name == "votersRegister2.xml"
    registerFiles[1].file.size() == 5499
    registerFiles[1].importDate == dateNow.minusDays(2)
    registerFiles[1].generationDate == parseGenerationDate("2017-10-20T15:21:57.481")
  }

  private static LocalDateTime parseGenerationDate(String s) {
    return LocalDateTime.ofInstant(DatatypeConverter.parseDateTime(s).toInstant(), ZoneId.of(TIMEZONE))
  }

  def "checkConsistency() should raise an exception due to a mismatch in the number of attached ZIP files"() {
    given:
    List<RegisterFileEntryVo> registerFileVos = createRegisterFilesCatalog()

    and:
    def is = this.class.getClassLoader().getResourceAsStream("attachments/votersRegister1.zip")
    MultipartFile file = new MockMultipartFile("reg1", "votersRegister1.zip", null, is)

    when:
    service.checkConsistency(registerFileVos, Arrays.asList(file))

    then:
    def ex = thrown(InvalidVotingMaterialsConfigurationException)
    ex.message == "The VotingMaterialConfiguration expects 2 ZIP attachment(s) but 1 ZIP attachment(s) are provided"
  }

  def "checkConsistency() should raise an exception due to a mismatch in the names of the ZIP files"() {
    given:
    List<RegisterFileEntryVo> registerFileVos = createRegisterFilesCatalog()

    and:
    def is1 = this.class.getClassLoader().getResourceAsStream("attachments/votersRegister1.zip")
    MultipartFile file1 = new MockMultipartFile("reg1", "votersRegister1.zip", null, is1)
    def is2 = this.class.getClassLoader().getResourceAsStream("attachments/votersRegister2.zip")
    MultipartFile file2 = new MockMultipartFile("reg1", "votersRegister2_WRONG.zip", null, is2)

    when:
    service.checkConsistency(registerFileVos, Arrays.asList(file1, file2))

    then:
    def ex = thrown(InvalidVotingMaterialsConfigurationException)
    ex.message ==
            "The ZIP file names in the actual attachments [[votersRegister1.zip, votersRegister2_WRONG.zip]] do not match the ZIP file names declared in the metadata [[votersRegister1.zip, votersRegister2.zip]]"
  }

  private List<RegisterFileEntryVo> createRegisterFilesCatalog() {
    def catalog = new ArrayList()
    for (int i = 1; i < 3; i++) {
      def file = new RegisterFileEntryVo()
      file.zipFileName = "votersRegister" + i + ".zip"
      file.importDateTime = dateNow.minusDays(i)
      catalog.add(file)
    }
    catalog
  }

}
