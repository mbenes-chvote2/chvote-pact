/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor

import ch.ge.ve.model.convert.api.VoterConverter
import ch.ge.ve.protocol.model.PrintingAuthorityForVerification
import com.fasterxml.jackson.databind.ObjectMapper
import java.nio.file.Path
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

/**
 * This test class is meant to test some abuse scenarios against the {@link PrinterArchiveGenerator}
 */
class PrinterArchiveGeneratorAbuseTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  VoterConverter voterConverter
  ObjectMapper objectMapper
  PrinterArchiveGenerator printerArchiveGenerator

  Path outputDir

  void setup() {
    outputDir = temporaryFolder.newFolder("printer-archive-generator-abuse-test").toPath()

    voterConverter = Mock()
    objectMapper = Mock()

    printerArchiveGenerator = new PrinterArchiveGenerator(voterConverter, objectMapper)
  }

  def "escaping attempts should get encoded"() {
    when:
    printerArchiveGenerator.prepareForPrinters(outputDir, [new PrintingAuthorityForVerification("../../etc/password")])

    then:
    outputDir.resolve("printerFiles").toFile().list()
        .contains("printer-archive-..%2F..%2Fetc%2Fpassword")
  }

  def "printerName with spaces should be encoded properly"() {
    when:
    printerArchiveGenerator.prepareForPrinters(outputDir, [new PrintingAuthorityForVerification("a b c")])

    then:
    outputDir.resolve("printerFiles").toFile().list().contains("printer-archive-a+b+c")
  }
}
