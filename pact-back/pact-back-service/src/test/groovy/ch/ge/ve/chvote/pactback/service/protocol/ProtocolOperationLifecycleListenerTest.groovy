/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol

import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.INITIALIZING
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES

import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.operation.PactSchedulerApi
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolDeletionEvent
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolStatusChangeEvent
import java.time.LocalDate
import spock.lang.Specification

class ProtocolOperationLifecycleListenerTest extends Specification {

  private static final DEFAULT_TECHNICAL_DELAY = 5

  private PactSchedulerApi pactSchedulerApi = Mock(PactSchedulerApi)

  private ProtocolOperationLifecycleListener listener

  void setup() {
    listener = new ProtocolOperationLifecycleListener(DEFAULT_TECHNICAL_DELAY, pactSchedulerApi)
  }

  def "register should not be called when env is not PRODUCTION or status does not become READY_TO_RECEIVE_VOTES"(
          ProtocolEnvironment env, ProtocolInstanceStatus status) {
    given:
    def operation = new Operation()
    def protocolInstance = new ProtocolInstance(operation: operation, environment: env, status: status)
    def event = new ProtocolStatusChangeEvent(protocolInstance, INITIALIZING, status)

    when:
    listener.registerOperationForScheduledEnding(event)

    then:
    0 * pactSchedulerApi._

    where:
    env << [ProtocolEnvironment.TEST] + [ProtocolEnvironment.PRODUCTION] * (ProtocolInstanceStatus.values().length - 1)
    status << [READY_TO_RECEIVE_VOTES] + (ProtocolInstanceStatus.values().toList() - [READY_TO_RECEIVE_VOTES])
  }

  def "register should call the PactSchedulerApi and apply the technicalDelay"() {
    given:
    def operation = new Operation(clientId: "test-operation")
    def protocolInstance = new ProtocolInstance(operation: operation, environment: ProtocolEnvironment.PRODUCTION,
            status: READY_TO_RECEIVE_VOTES)
    def event = new ProtocolStatusChangeEvent(protocolInstance, INITIALIZING, READY_TO_RECEIVE_VOTES)

    and:
    def closingDate = LocalDate.now().plusDays(1).atTime(18, 30)
    def votingPeriod = new VotingPeriodConfiguration(siteOpeningDate: closingDate.minusWeeks(2),
            siteClosingDate: closingDate, gracePeriod: 45)
    operation.votingPeriodConfiguration = votingPeriod

    when:
    listener.registerOperationForScheduledEnding(event)

    then:
    1 * pactSchedulerApi.register("test-operation", closingDate.plusMinutes(50))
  }

  def "unregister should not happen when the environment is not PRODUCTION"() {
    given:
    def operation = new Operation()
    def protocolInstance = new ProtocolInstance(operation: operation, environment: ProtocolEnvironment.TEST)
    def event = new ProtocolDeletionEvent(protocolInstance)

    when:
    listener.unregisterUndeployedOperation(event)

    then:
    0 * pactSchedulerApi._
  }

  def "unregister should call the PactSchedulerApi"() {
    given:
    def operation = new Operation(clientId: "a-deployed-operation")
    def protocolInstance = new ProtocolInstance(operation: operation, environment: ProtocolEnvironment.PRODUCTION)
    def event = new ProtocolDeletionEvent(protocolInstance)

    when:
    listener.unregisterUndeployedOperation(event)

    then:
    1 * pactSchedulerApi.unregister("a-deployed-operation")
  }
}
