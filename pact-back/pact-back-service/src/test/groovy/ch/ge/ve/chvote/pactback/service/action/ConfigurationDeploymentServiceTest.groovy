/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action

import ch.ge.ve.chvote.pactback.repository.action.deploy.ConfigurationDeploymentRepository
import ch.ge.ve.chvote.pactback.repository.action.deploy.entity.ConfigurationDeployment
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus
import ch.ge.ve.chvote.pactback.repository.operation.configuration.OperationConfigurationRepository
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.user.UserRepository
import ch.ge.ve.chvote.pactback.repository.user.entity.User
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyExistsException
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException
import ch.ge.ve.chvote.pactback.service.operation.OperationService
import java.util.stream.Stream
import spock.lang.Specification

class ConfigurationDeploymentServiceTest extends Specification {

  private ConfigurationDeploymentService service
  private UserRepository userRepository
  private ConfigurationDeploymentRepository actionRepository
  private OperationConfigurationRepository configurationRepository
  private OperationService operationService

  void setup() {
    userRepository = Mock(UserRepository)
    actionRepository = Mock(ConfigurationDeploymentRepository)
    configurationRepository = Mock(OperationConfigurationRepository)
    operationService = Mock(OperationService)
    service = new ConfigurationDeploymentService(userRepository, actionRepository, configurationRepository, operationService)
  }

  def "newOrPendingActions should return 2 action VOs"() {
    given:
    def configuration1 = buildOperationConfiguration(1L, true)
    def configuration2 = buildOperationConfiguration(2L, false)
    configurationRepository.findAllConfigurationsDeployableOrWithDeploymentInStatus(PrivilegedAction.Status.PENDING) >>
            Stream.of(configuration1, configuration2)

    when:
    def pendingPrivilegedActions = service.newOrPendingActions

    then:
    pendingPrivilegedActions.size() == 2
  }

  def "getPendingAction should return a pending action"() {
    given:
    def action = buildOperationConfiguration(1L, true).action

    when:
    def pendingAction = service.getPendingPrivilegedAction(1L)

    then:
    1 * actionRepository.findById(1L) >> action
    pendingAction.status == "PENDING"
  }

  def "getByBusinessId should return an action"() {
    given:
    def configuration = buildOperationConfiguration(1L, false)

    when:
    def pendingAction = service.getByBusinessId(1L)

    then:
    1 * configurationRepository.findById(1L) >> Optional.of(configuration)
    pendingAction.status == "NOT_CREATED"
  }

  def "getByBusinessId should throw an exception if the action is already approved"() {
    given:
    def configuration = buildOperationConfiguration(1L, true)
    configuration.action.get().status = PrivilegedAction.Status.APPROVED

    when:
    service.getByBusinessId(1L)

    then:
    1 * configurationRepository.findById(1L) >> Optional.of(configuration)
    PrivilegedActionAlreadyProcessedException ex = thrown()
    ex.message == 'Action with id 1 has already been processed and cannot be displayed'
  }

  def "createAction should save an action into the repository"() {
    given:
    def user = Mock(User)
    def configuration = Mock(OperationConfiguration)

    when:
    service.createAction(100L, "requester")

    then:
    1 * userRepository.findByUsername("requester") >> Optional.of(user)
    1 * configurationRepository.findById(100L) >> Optional.of(configuration)
    1 * actionRepository.existsByOperationConfiguration_Id(100L) >> false
    1 * actionRepository.save(*_) >> { arguments ->
      assert (arguments[0] as ConfigurationDeployment).status == PrivilegedAction.Status.PENDING
      assert (arguments[0] as ConfigurationDeployment).operationConfiguration == configuration
      assert (arguments[0] as ConfigurationDeployment).requester == user
    }
  }

  def "createAction should raise an exception when an action already exists for the operation configuration"() {
    when:
    service.createAction(100L, "requester")

    then:
    0 * userRepository.findByUsername(*_)
    1 * actionRepository.existsByOperationConfiguration_Id(100L) >> true
    PrivilegedActionAlreadyExistsException ex = thrown()
    ex.message == 'An action was already created for the OperationConfiguration object with ID 100'
  }

  def "approveAction should set the status of an action to APPROVED"() {
    given:
    def configuration = buildOperationConfiguration(1L, true)
    def user = Mock(User)
    user.username = "Bart"
    user.id = 2L
    actionRepository.findById(1L) >> configuration.action

    when:
    service.approveAction(1L, "Bart")

    then:
    1 * userRepository.findByUsername("Bart") >> Optional.of(user)
    1 * operationService.deployConfigurationInTest(_)
    1 * actionRepository.save(*_) >> { arguments ->
      assert (arguments[0] as PrivilegedAction).status == PrivilegedAction.Status.APPROVED
    }
  }

  def "approveAction should fail if the action status is not PENDING"() {
    given:
    def configuration = buildOperationConfiguration(1L, true)
    def user = Mock(User)
    user.username = "Bart"
    user.id = 2L
    configuration.action.get().status = PrivilegedAction.Status.APPROVED
    actionRepository.findById(1L) >> configuration.action

    when:
    service.approveAction(1L, "Bart")

    then:
    1 * userRepository.findByUsername("Bart") >> Optional.of(user)
    0 * operationService.deployConfigurationInTest(_)
    0 * actionRepository.save(*_)
    thrown(PrivilegedActionAlreadyProcessedException)
  }

  def "rejectAction should set the status of an action to REJECTED"() {
    given:
    actionRepository.findById(1L) >>
            buildOperationConfiguration(1L, true).action
    def user = new User()
    user.username = "Bart"
    user.id = 2L

    when:
    service.rejectAction(1L, "Bart", "Reason")

    then:
    1 * userRepository.findByUsername("Bart") >> Optional.of(user)
    1 * actionRepository.save(*_) >> { arguments ->
      assert (arguments[0] as PrivilegedAction).status == PrivilegedAction.Status.REJECTED
      assert (arguments[0] as PrivilegedAction).rejectionReason == "Reason"
    }
  }

  def buildOperationConfiguration(Long id, boolean alreadyHasAction) {
    def configuration = new OperationConfiguration()
    configuration.id = id
    configuration.operationReferenceFiles = new ArrayList<>()
    configuration.validationStatus = ValidationStatus.VALIDATED

    if (alreadyHasAction) {
      def action = new ConfigurationDeployment()
      def user = new User()

      action.id = 1L
      action.status = PrivilegedAction.Status.PENDING
      action.operationConfiguration = configuration
      user.id = 1L
      user.username = "Anne Dupont"
      action.requester = user

      configuration.action = action
    }

    return configuration
  }

}
