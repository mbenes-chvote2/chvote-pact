/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolDeletionEvent
import ch.ge.ve.chvote.pactback.service.protocol.client.ProtocolClientFactory
import ch.ge.ve.chvote.pactback.service.protocol.processor.OutputFilesManager
import spock.lang.Specification

class ProtocolCleanupServiceTest extends Specification {

  private ProtocolCleanupService protocolCleanupService

  private ProtocolClientFactory protocolClientFactory

  private OutputFilesManager outputFilesManager

  void setup() {
    protocolClientFactory = Mock(ProtocolClientFactory)
    outputFilesManager = Mock(OutputFilesManager)

    protocolCleanupService = new ProtocolCleanupService(protocolClientFactory, outputFilesManager)
  }

  def "deleteProtocolFiles should delete the files"() {
    given:
    def instance = new ProtocolInstance()
    instance.id = 3
    def event = new ProtocolDeletionEvent(instance)

    when:
    protocolCleanupService.deleteProtocolFiles(event)

    then:
    1 * outputFilesManager.deleteAllFiles(_) >> { arguments ->
      def eventInstance = arguments[0] as ProtocolInstance
      assert eventInstance.id == 3
    }
  }

}
