/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import ch.ge.ve.chvote.pactback.repository.action.materials.VotingMaterialsCreationActionRepository
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsCreation
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.Municipality
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.materials.VotingMaterialsConfigurationRepository
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration
import ch.ge.ve.chvote.pactback.repository.user.UserRepository
import ch.ge.ve.chvote.pactback.repository.user.entity.User
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyExistsException
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException
import ch.ge.ve.chvote.pactback.service.operation.OperationService
import java.time.LocalDateTime
import java.util.stream.Stream
import spock.lang.Specification

class VotingMaterialsCreationServiceTest extends Specification {

  private VotingMaterialsCreationService service
  private UserRepository userRepository
  private VotingMaterialsCreationActionRepository actionRepository
  private VotingMaterialsConfigurationRepository materialsRepository
  private OperationService operationService

  void setup() {
    userRepository = Mock(UserRepository)
    actionRepository = Mock(VotingMaterialsCreationActionRepository)
    materialsRepository = Mock(VotingMaterialsConfigurationRepository)
    operationService = Mock(OperationService)
    service = new VotingMaterialsCreationService(userRepository, materialsRepository, actionRepository, operationService)
  }

  def "newOrPendingActions should return 2 action VOs"() {
    given:
    def configuration1 = buildVotingMaterialsConfiguration(1L, true)
    def configuration2 = buildVotingMaterialsConfiguration(2L, false)
    materialsRepository.findAllByCreationActionIsNullOrCreationActionStatus(_) >> Stream.of(configuration1, configuration2)

    def operation1 = buildOperation(configuration1)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation1
    def operation2 = buildOperation(configuration2)
    operationService.getOperationByVotingMaterialsConfigurationId(2L) >> operation2

    when:
    def pendingPrivilegedActions = service.newOrPendingActions

    then:
    pendingPrivilegedActions.size() == 2
  }

  def "getPendingAction should return a pending action"() {
    given:
    def action = buildVotingMaterialsConfiguration(1L, true).creationAction
    def operation = buildOperation(action.get().votingMaterialsConfiguration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation

    when:
    def pendingAction = service.getPendingPrivilegedAction(1L)

    then:
    1 * actionRepository.findById(1L) >> action
    pendingAction.status == "PENDING"
  }

  def "getByBusinessId should return an action"() {
    given:
    def configuration = buildVotingMaterialsConfiguration(1L, false)
    def operation = buildOperation(configuration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation

    when:
    def pendingAction = service.getByBusinessId(1L)

    then:
    1 * materialsRepository.getOne(1L) >> configuration
    pendingAction.status == "NOT_CREATED"
  }

  def "getByBusinessId should throw an exception if the action is already approved"() {
    given:
    def configuration = buildVotingMaterialsConfiguration(1L, true)
    def operation = buildOperation(configuration)
    configuration.creationAction.get().status = PrivilegedAction.Status.APPROVED
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation

    when:
    service.getByBusinessId(1L)

    then:
    1 * materialsRepository.getOne(1L) >> configuration
    PrivilegedActionAlreadyProcessedException ex = thrown()
    ex.message == 'Action with id 1 has already been processed and cannot be displayed'
  }

  def "getByBusinessId should return an action pointing to a voting material configuration with two printers"() {
    given:
    // create a VotingMaterialsConfiguration without printers
    def configuration = buildVotingMaterialsConfiguration(1L, false)
    def operation = buildOperation(configuration)
    operationService.getOperationByVotingMaterialsConfigurationId(1L) >> operation

    // add 1 printer with 2 municipalities and a virtual municipality

    def printers = new ArrayList()
    configuration.printers = printers

    def printer = new PrinterConfiguration()
    printer.businessIdentifier = "PRINTER-1"
    printer.printerName = "Imprimerie A"
    printers.add(printer)

    // municipalities
    def municipalities = new ArrayList()
    printer.municipalities = municipalities
    def geneve = Mock(Municipality)
    geneve.ofsId >> 51
    geneve.name >> "Genève"
    geneve.virtual >> false
    municipalities.add(geneve)
    def carouge = Mock(Municipality)
    carouge.ofsId >> 52
    carouge.name >> "Carouge"
    carouge.virtual >> false
    municipalities.add(carouge)
    def virtualMunicipality = Mock(Municipality)
    virtualMunicipality.ofsId >> 100
    virtualMunicipality.virtual >> true
    municipalities.add(virtualMunicipality)

    when:
    def pendingAction = service.getByBusinessId(1L)

    then:
    1 * materialsRepository.getOne(1L) >> configuration
    pendingAction.status == "NOT_CREATED"
    pendingAction.votingMaterialsConfiguration.printers.size() == 2
    pendingAction.votingMaterialsConfiguration.printers[0].municipalities.size() == 2
    pendingAction.votingMaterialsConfiguration.printers[0].municipalities[0].name == "Genève"
    pendingAction.votingMaterialsConfiguration.printers[0].municipalities[1].name == "Carouge"
    pendingAction.votingMaterialsConfiguration.printers[1].municipalities.size() == 1
    pendingAction.votingMaterialsConfiguration.printers[1].municipalities[0].ofsId == 100
  }

  def "createAction should save an action into the repository"() {
    given:
    def user = Mock(User)
    def configuration = buildVotingMaterialsConfiguration(100L, false)
    def operation = buildOperation(configuration)
    operationService.getOperationByVotingMaterialsConfigurationId(100L) >> operation

    when:
    service.createAction(100L, "requester")

    then:
    1 * userRepository.findByUsername("requester") >> Optional.of(user)
    1 * materialsRepository.findById(100L) >> Optional.of(configuration)
    1 * actionRepository.existsByVotingMaterialsConfiguration_Id(100L) >> false
    1 * actionRepository.save(*_) >> { arguments ->
      assert (arguments[0] as VotingMaterialsCreation).status == PrivilegedAction.Status.PENDING
      assert (arguments[0] as VotingMaterialsCreation).votingMaterialsConfiguration == configuration
      assert (arguments[0] as VotingMaterialsCreation).requester == user
    }
  }

  def "createAction should raise an exception when an action already exists for the voting materials configuration"() {
    when:
    service.createAction(100L, "requester")

    then:
    0 * userRepository.findByUsername(*_)
    1 * actionRepository.existsByVotingMaterialsConfiguration_Id(100L) >> true
    PrivilegedActionAlreadyExistsException ex = thrown()
    ex.message == 'An action was already created for the VotingMaterialsConfiguration object with ID 100'
  }

  def "approveAction should set the status of an action to APPROVED"() {
    given:
    def votingMaterialsConfiguration = buildVotingMaterialsConfiguration(1L, true)
    actionRepository.findById(1L) >> votingMaterialsConfiguration.creationAction
    def user = Mock(User)
    user.username = "Bart"
    user.id = 2L

    when:
    service.approveAction(1L, "Bart")

    then:
    1 * userRepository.findByUsername("Bart") >> Optional.of(user)
    1 * operationService.publishDeployedConfiguration(*_)
    1 * actionRepository.save(*_) >> { arguments ->
      assert (arguments[0] as PrivilegedAction).status == PrivilegedAction.Status.APPROVED
    }
  }

  def "approveAction should fail if the action status is not PENDING"() {
    given:
    def votingMaterialsConfiguration = buildVotingMaterialsConfiguration(1L, true)
    votingMaterialsConfiguration.creationAction.get().status = PrivilegedAction.Status.APPROVED
    actionRepository.findById(1L) >> votingMaterialsConfiguration.creationAction
    def user = Mock(User)
    user.username = "Bart"
    user.id = 2L

    when:
    service.approveAction(1L, "Bart")

    then:
    1 * userRepository.findByUsername("Bart") >> Optional.of(user)
    0 * operationService.publishDeployedConfiguration(_)
    0 * actionRepository.save(*_)
    thrown(PrivilegedActionAlreadyProcessedException)
  }

  def "rejectAction should set the status of an action to REJECTED"() {
    given:
    actionRepository.findById(1L) >>
            buildVotingMaterialsConfiguration(1L, true).creationAction
    def user = new User()
    user.username = "Bart"
    user.id = 2L

    when:
    service.rejectAction(1L, "Bart", "Reason")

    then:
    1 * userRepository.findByUsername("Bart") >> Optional.of(user)
    1 * actionRepository.save(*_) >> { arguments ->
      assert (arguments[0] as PrivilegedAction).status == PrivilegedAction.Status.REJECTED
      assert (arguments[0] as PrivilegedAction).rejectionReason == "Reason"
    }
  }

  def buildVotingMaterialsConfiguration(Long id, boolean alreadyHasAction) {
    def votingConf = new VotingMaterialsConfiguration()

    votingConf.id = id
    votingConf.target = DeploymentTarget.SIMULATION
    votingConf.simulationName = "simulation-" + id
    votingConf.registers = new ArrayList<>()
    votingConf.printers = new ArrayList<>()

    if (alreadyHasAction) {
      def action = new VotingMaterialsCreation()
      def user = new User()

      action.id = 1L
      action.status = PrivilegedAction.Status.PENDING
      action.votingMaterialsConfiguration = votingConf
      user.username = "Anne Dupont"
      user.id = 1L
      action.requester = user

      votingConf.creationAction = action
    }

    return votingConf
  }

  def buildOperation(VotingMaterialsConfiguration configuration) {
    def operation = new Operation()

    operation.votingMaterialsConfiguration = configuration

    def deployedConf = new OperationConfiguration()
    deployedConf.operationName = "some op name"
    deployedConf.operationReferenceFiles = new ArrayList<>()
    operation.deployedConfiguration = deployedConf

    operation
  }

  def createPrivilegedActionMock(Long id, String username) {
    def user = new User()
    user.id = 1L
    user.username = username

    def configuration = new VotingMaterialsConfiguration()

    def action = Mock(VotingMaterialsCreation)
    action.id >> id
    action.requester >> user
    action.votingMaterialsConfiguration >> configuration
    action.creationDate >> LocalDateTime.now()
    action.status >> PrivilegedAction.Status.PENDING
    action.setStatus(_) >> { arguments -> action.status >> arguments[0] }
    action.setRejectionReason(_) >> { arguments -> action.rejectionReason >> arguments[0] }

    return action
  }

}
