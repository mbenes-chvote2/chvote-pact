/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.materials

import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.AVAILABLE_FOR_CREATION
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_FAILED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_IN_PROGRESS
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_REJECTED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_REQUESTED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.INVALIDATED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.INVALIDATION_REJECTED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.INVALIDATION_REQUESTED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.VALIDATED
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.APPROVED
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.PENDING
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.REJECTED

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsCreation
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsInvalidation
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage
import ch.ge.ve.chvote.pactback.repository.protocol.progress.entity.ProtocolStageProgress
import ch.ge.ve.chvote.pactback.repository.user.entity.User
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties
import ch.ge.ve.chvote.pactback.service.protocol.processor.OutputFilesManager
import ch.ge.ve.chvote.pactback.service.protocol.processor.PrinterArchiveGenerator
import java.nio.file.Path
import java.nio.file.Paths
import java.time.LocalDateTime
import java.util.stream.IntStream
import spock.lang.Specification

class VotingMaterialsStatusVoMapperTest extends Specification {
  private static final String SUBMITTER = "submitter"
  private static final String REQUESTER = "requester"
  private static final String REQUEST_VALIDATOR = "validator"
  private static final String VM_VALIDATOR = "VM_VALIDATOR"

  private static final LocalDateTime DATE_CONFIG = LocalDateTime.now().minusDays(3)
  private static final LocalDateTime DATE_REQUEST = LocalDateTime.now().minusDays(2)
  private static final LocalDateTime DATE_REQUEST_REJECTED = LocalDateTime.now().minusHours(1)
  private static final LocalDateTime DATE_REQUEST_APPROVED = LocalDateTime.now().minusHours(1)
  private static final LocalDateTime DATE_MATERIALS_CREATED = LocalDateTime.now()
  private static final LocalDateTime DATE_VALIDATED = LocalDateTime.now().minusHours(7)

  private static final LocalDateTime DATE_MATERIALS_CREATION_FAILED = LocalDateTime.now().minusHours(5)

  private OutputFilesManager outputFilesManager
  private PrinterArchiveGenerator printerArchiveGenerator
  private VotingMaterialsStatusVoMapper votingMaterialsConfigurationStatusVoMapper

  void setup() {
    def protocolCoreConf = Mock(ChVoteConfigurationProperties.ProtocolCore)
    protocolCoreConf.votingMaterialsTimeoutMinutes >> 240

    def configurationProperties = Mock(ChVoteConfigurationProperties)
    configurationProperties.protocolCore >> protocolCoreConf
    configurationProperties.pactContextUrl >> "/pact"
    configurationProperties.vrIdentificationUrl >> "/vrIdentificationUrl"

    outputFilesManager = Mock(OutputFilesManager)
    printerArchiveGenerator = Mock(PrinterArchiveGenerator)
    votingMaterialsConfigurationStatusVoMapper =
            new VotingMaterialsStatusVoMapper(outputFilesManager, printerArchiveGenerator, configurationProperties)
  }

  def "operation should be mapped to a VotingMaterialsConfigurationStatusVo"() {
    when:
    def statusVo = votingMaterialsConfigurationStatusVoMapper.map(operation)

    then:
    statusVo.state == expectedState
    statusVo.lastChangeDate == expectedLastChangeDate
    statusVo.lastChangeUser == expectedUser
    statusVo.configurationPageUrl == "/pact/voting-materials-creation/9903"

    where:
    operation                            || expectedLastChangeDate         || expectedUser      || expectedState
    pending()                            || DATE_CONFIG                    || SUBMITTER         || AVAILABLE_FOR_CREATION
    creationRequested()                  || DATE_REQUEST                   || REQUESTER         || CREATION_REQUESTED
    requestProcessed(REJECTED)           || DATE_REQUEST_REJECTED          || REQUEST_VALIDATOR || CREATION_REJECTED
    requestProcessed(APPROVED)           || DATE_REQUEST_APPROVED          || REQUEST_VALIDATOR || CREATION_IN_PROGRESS
    materialsCreated()                   || DATE_MATERIALS_CREATED         || REQUEST_VALIDATOR || CREATED
    materialsValidated()                 || DATE_VALIDATED                 || VM_VALIDATOR      || VALIDATED
    invalidationRequested()              || DATE_REQUEST                   || REQUESTER         || INVALIDATION_REQUESTED
    invalidationRejected()               || DATE_REQUEST_REJECTED          || VM_VALIDATOR      || INVALIDATION_REJECTED
    materialsInvalidated()               || DATE_VALIDATED                 || VM_VALIDATOR      || INVALIDATED
    materialsCreationFailedWithTimeout() || DATE_MATERIALS_CREATION_FAILED || REQUEST_VALIDATOR || CREATION_FAILED
    materialsCreationFailed()            || DATE_MATERIALS_CREATED         || REQUEST_VALIDATOR || CREATION_FAILED
  }


  def "should return voting card location for each configured printer"() {
    given:
    def operation = materialsCreated()
    operation.getVotingMaterialsConfiguration().get().setPrinters(
            IntStream.range(0, 5).collect({ i ->
              def pConf = new PrinterConfiguration()
              pConf.setBusinessIdentifier(Integer.toString(i))
              return pConf
            })
    )
    def baseLocation = Mock(Path)
    def file = Mock(File)
    baseLocation.toFile() >> file
    file.exists() >> true
    outputFilesManager.getOutputPath(_) >> baseLocation
    printerArchiveGenerator.getArchivePath(_, baseLocation, _, _) >> baseLocation
    outputFilesManager.relativize(_) >> { Path path -> Paths.get("/") }

    when:
    def statusVo = votingMaterialsConfigurationStatusVoMapper.map(operation)

    then:
    statusVo.getVotingCardLocations().size() == 5
    statusVo.getVotingCardLocations().get("0") == "/"
  }

  def "should not return the voting card location of any printer whose printer archive does not exists"() {
    given:
    def operation = materialsCreated()
    operation.getVotingMaterialsConfiguration().get().setPrinters(
            IntStream.range(0, 5).collect({ i ->
              def pConf = new PrinterConfiguration()
              pConf.setBusinessIdentifier(Integer.toString(i))
              return pConf
            })
    )
    def baseLocation = Mock(Path)
    def file = Mock(File)
    baseLocation.toFile() >> file
    file.exists() >> false
    outputFilesManager.getOutputPath(_) >> baseLocation
    printerArchiveGenerator.getArchivePath(_, baseLocation, _, _) >> baseLocation
    outputFilesManager.relativize(_) >> { Path path -> Paths.get("/") }

    when:
    def statusVo = votingMaterialsConfigurationStatusVoMapper.map(operation)

    then:
    statusVo.getVotingCardLocations().size() == 0
  }

  def pending() {
    def operation = new Operation()

    def votingMaterialsConfiguration = new VotingMaterialsConfiguration()
    votingMaterialsConfiguration.id = 9903
    votingMaterialsConfiguration.submissionDate = DATE_CONFIG
    votingMaterialsConfiguration.author = SUBMITTER

    operation.votingMaterialsConfiguration = votingMaterialsConfiguration
    return operation
  }

  def creationRequested() {

    def operation = pending()
    def votingMaterialsConfiguration = operation.getVotingMaterialsConfiguration().get()

    def action = new VotingMaterialsCreation()
    action.status = PENDING
    action.statusDate = DATE_REQUEST
    votingMaterialsConfiguration.action = action

    def requester = new User()
    requester.username = REQUESTER
    action.requester = requester
    action.creationDate = DATE_REQUEST

    return operation
  }

  def requestProcessed(PrivilegedAction.Status actionStatus) {

    def operation = creationRequested()
    def votingMaterialsConfiguration = operation.getVotingMaterialsConfiguration().get()

    def action = votingMaterialsConfiguration.getCreationAction().get()
    action.status = actionStatus

    def validator = new User()
    validator.username = REQUEST_VALIDATOR
    action.validator = validator
    action.statusDate = DATE_REQUEST_REJECTED

    def protocolInstance = new ProtocolInstance()
    protocolInstance.status = ProtocolInstanceStatus.INITIALIZING
    protocolInstance.statusDate = DATE_REQUEST_APPROVED

    def deployedConf = new OperationConfiguration()
    deployedConf.protocolInstance = protocolInstance

    operation.deployedConfiguration = deployedConf

    return operation
  }

  def materialsCreated() {
    def operation = requestProcessed(APPROVED)
    def protocolInstance = operation
            .deployedConfiguration.get()
            .protocolInstance.get()

    protocolInstance.configuration = operation.deployedConfiguration.get()
    protocolInstance.status = ProtocolInstanceStatus.CREDENTIALS_GENERATED
    protocolInstance.statusDate = DATE_MATERIALS_CREATED
    def credentialsProgress = new ProtocolStageProgress()
    credentialsProgress.stage = ProtocolStage.REQUEST_PRIVATE_CREDENTIALS
    credentialsProgress.ccIndex = 0
    credentialsProgress.done = 10
    credentialsProgress.total = 10
    credentialsProgress.startDate = LocalDateTime.now()
    credentialsProgress.lastModificationDate = LocalDateTime.now()
    protocolInstance.progress.add(credentialsProgress)
    def sendPublicParametersProgress = new ProtocolStageProgress()
    sendPublicParametersProgress.stage = ProtocolStage.SEND_PUBLIC_PARAMETERS
    sendPublicParametersProgress.ccIndex = -1
    sendPublicParametersProgress.done = 1
    sendPublicParametersProgress.total = 1
    sendPublicParametersProgress.startDate = LocalDateTime.now()
    sendPublicParametersProgress.lastModificationDate = LocalDateTime.now()
    protocolInstance.progress.add(sendPublicParametersProgress)
    protocolInstance.printerArchiveCreationDate = LocalDateTime.now()

    return operation
  }

  def materialsValidated() {
    def operation = requestProcessed(APPROVED)
    operation.votingMaterialsConfiguration.get().validationStatus = ValidationStatus.VALIDATED
    operation.votingMaterialsConfiguration.get().validationDate = DATE_VALIDATED
    operation.votingMaterialsConfiguration.get().validator = VM_VALIDATOR
    operation
  }

  def invalidationRequested() {
    def operation = requestProcessed(APPROVED)
    def invalidationAction = new VotingMaterialsInvalidation()
    invalidationAction.setStatus(PENDING)
    def requester = new User()
    requester.username = REQUESTER
    invalidationAction.setRequester(requester)
    invalidationAction.setStatusDate(DATE_REQUEST)

    operation.votingMaterialsConfiguration.get().invalidationAction = invalidationAction
    operation
  }

  def invalidationRejected() {
    def operation = requestProcessed(APPROVED)
    def invalidationAction = new VotingMaterialsInvalidation()
    invalidationAction.setStatus(REJECTED)

    def validator = new User()
    validator.username = VM_VALIDATOR
    invalidationAction.validator = validator
    invalidationAction.statusDate = DATE_REQUEST_REJECTED

    operation.votingMaterialsConfiguration.get().invalidationAction = invalidationAction
    operation
  }


  def materialsInvalidated() {
    def operation = requestProcessed(APPROVED)
    operation.votingMaterialsConfiguration.get().validationStatus = ValidationStatus.INVALIDATED
    operation.votingMaterialsConfiguration.get().validationDate = DATE_VALIDATED
    operation.votingMaterialsConfiguration.get().validator = VM_VALIDATOR
    operation
  }

  def materialsCreationFailedWithTimeout() {
    def operation = requestProcessed(APPROVED)
    def protocolInstance = operation
            .deployedConfiguration.get()
            .protocolInstance.get()

    protocolInstance.status = ProtocolInstanceStatus.GENERATING_CREDENTIALS
    protocolInstance.statusDate = DATE_MATERIALS_CREATION_FAILED
    def credentialsProgress = new ProtocolStageProgress()
    credentialsProgress.stage = ProtocolStage.REQUEST_PRIVATE_CREDENTIALS
    credentialsProgress.ccIndex = 0
    credentialsProgress.done = 2
    credentialsProgress.total = 10
    credentialsProgress.startDate = LocalDateTime.now()
    credentialsProgress.lastModificationDate = LocalDateTime.now()
    protocolInstance.progress.add(credentialsProgress)
    def sendPublicParametersProgress = new ProtocolStageProgress()
    sendPublicParametersProgress.stage = ProtocolStage.SEND_PUBLIC_PARAMETERS
    sendPublicParametersProgress.ccIndex = -1
    sendPublicParametersProgress.done = 1
    sendPublicParametersProgress.total = 1
    sendPublicParametersProgress.startDate = LocalDateTime.now()
    sendPublicParametersProgress.lastModificationDate = LocalDateTime.now()
    protocolInstance.progress.add(sendPublicParametersProgress)

    return operation
  }

  def materialsCreationFailed() {
    def operation = requestProcessed(APPROVED)
    def protocolInstance = operation
            .deployedConfiguration.get()
            .protocolInstance.get()

    protocolInstance.status = ProtocolInstanceStatus.CREDENTIALS_GENERATION_FAILURE
    protocolInstance.statusDate = DATE_MATERIALS_CREATED
    def credentialsProgress = new ProtocolStageProgress()
    credentialsProgress.stage = ProtocolStage.REQUEST_PRIVATE_CREDENTIALS
    credentialsProgress.ccIndex = 0
    credentialsProgress.done = 5
    credentialsProgress.total = 10
    credentialsProgress.startDate = LocalDateTime.now()
    credentialsProgress.lastModificationDate = LocalDateTime.now()
    protocolInstance.progress.add(credentialsProgress)
    def sendPublicParametersProgress = new ProtocolStageProgress()
    sendPublicParametersProgress.stage = ProtocolStage.SEND_PUBLIC_PARAMETERS
    sendPublicParametersProgress.ccIndex = -1
    sendPublicParametersProgress.done = 1
    sendPublicParametersProgress.total = 1
    sendPublicParametersProgress.startDate = LocalDateTime.now()
    sendPublicParametersProgress.lastModificationDate = LocalDateTime.now()
    protocolInstance.progress.add(sendPublicParametersProgress)

    return operation
  }
}
