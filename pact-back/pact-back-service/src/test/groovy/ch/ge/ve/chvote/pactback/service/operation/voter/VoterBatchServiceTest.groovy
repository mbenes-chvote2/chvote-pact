/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.voter

import ch.ge.ve.chvote.pactback.service.operation.voter.vo.VoterCountingCircleVo
import ch.ge.ve.chvote.pactback.service.operation.voter.vo.DateOfBirthVo
import ch.ge.ve.chvote.pactback.service.operation.voter.vo.VoterVo
import java.time.Year
import spock.lang.Specification

class VoterBatchServiceTest extends Specification {

  def voterService = Mock(VoterService)

  def "the batch should be flushed automatically when the batch size is reached"() {
    given:
    def batchSize = 2
    def protocolInstanceId = 1L
    def batch = new VoterBatchService(voterService, batchSize).startBatchInsert(protocolInstanceId)
    def voters = [createVoterVo(), createVoterVo()]

    when:
    batch.add(voters[0])

    then:
    0 * voterService.saveAll(protocolInstanceId, _)

    when:
    batch.add(voters[1])

    then:
    1 * voterService.saveAll(protocolInstanceId, voters)
  }

  def "the batch should be flushed when flush() is called"() {
    given:
    def batchSize = 20
    def protocolInstanceId = 1L
    def batch = new VoterBatchService(voterService, batchSize).startBatchInsert(protocolInstanceId)
    def voter = createVoterVo()

    when:
    batch.add(voter)

    then:
    0 * voterService.saveAll(protocolInstanceId, _)

    when:
    batch.flush()

    then:
    1 * voterService.saveAll(protocolInstanceId, [voter])
  }

  private static VoterVo createVoterVo() {
    return new VoterVo(10L, 1, "a", new DateOfBirthVo(Year.of(1984), null, null),
                       new VoterCountingCircleVo(1, "GE", "Geneve"), "printer1", ["CH", "GE"])
  }
}
