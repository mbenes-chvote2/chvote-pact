swagger: '2.0'
info:
  description: PACT Mangement Api Documentation
  version: '1.0'
  title: PACT Management Api Documentation
  termsOfService: 'urn:tos'
  contact: {}
  license:
    name: AGPL v3
    url: 'http://www.gnu.org/licenses'
host: 'localhost:8080'
basePath: /api
tags:
- name: "OperationConfiguration"
  description: "Operation configuration related endpoints."
- name: "VotingMaterials"
  description: "Voting Materials related endpoints"
paths:
  '/operation/{clientId}/configuration':
    post:
      tags:
      - OperationConfiguration
      summary: Create or update an operation configuration, create the operation if it does not exist yet
      consumes:
      - multipart/form-data
      produces:
      - application/json
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation to configure
        required: true
        type: string
      - name: data
        in: formData
        description: Operation ZIP file
        required: true
        type: file
      - name: operation
        in: formData
        type: string
        description: "Operation configuration, structured according to #/definitions/OperationConfigurationSubmissionVo"
        required: true

      responses:
        '200':
          description: OK
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/operation/{clientId}/configuration/status':
    get:
      tags:
      - OperationConfiguration
      summary: Get status of configurations by operation client id
      description: Retrieve status of both deployed (if available) and in test configurations of an operation identified by the given client id.
      consumes:
      - application/json
      produces:
      - application/json
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation to configure
        required: true
        type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/OperationConfigurationsStatusVo'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/operation/{clientId}/configuration/invalidate':
    post:
      tags:
      - OperationConfiguration
      summary: Invalidates the configuration in test
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation
        required: true
        type: string
      - in: "body"
        name: "user"
        description: "user accountable for invalidating the configuration"
        required: true
        schema:
          $ref: '#/definitions/UserVo'
      responses:
        '200':
          description: OK
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/operation/{clientId}/configuration/validate':
    post:
      tags:
      - OperationConfiguration
      summary: Validates the configuration in test
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation
        required: true
        type: string
      - in: "body"
        name: "user"
        description: "user accountable for validating the configuration"
        required: true
        schema:
          $ref: '#/definitions/UserVo'
      responses:
        '200':
          description: OK
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found

  '/operation/{clientId}/voting-materials/configuration':
    put:
      tags:
      - VotingMaterials
      summary: Create a voting materials configuration
      consumes:
      - multipart/form-data
      produces:
      - application/json
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation
        required: true
        type: string
      - name: data
        in: formData
        description: Electoral register zip files. One zip file is sent for each existing electoral register file.
        required: true
        type: file
      - name: configuration
        in: formData
        description: "Voting materials configuration, structured according to #/definitions/VotingMaterialsConfigurationSubmissionVo"
        required: true
        type: string
      responses:
        '200':
          description: OK
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found

  '/operation/{clientId}/voting-materials/status':
    get:
      tags:
      - VotingMaterials
      summary: Returns information about the state of a voting materials configuration
      produces:
      - application/json
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation
        required: true
        type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/VotingMaterialsStatusVo'
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found

  '/operation/{clientId}/voting-materials/statistics':
    get:
      tags:
      - VotingMaterials
      summary: Returns statistics about created voting materials for the deployed configuration of an operation
      produces:
      - application/json
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation
        required: true
        type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/VotersCreationStatisticsVo'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found


  '/operation/{clientId}/voting-period/configuration':
    post:
      tags:
      - VotingPeriods
      summary: Create a voting period configuration
      consumes:
      - multipart/form-data
      produces:
      - application/json
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation
        required: true
        type: string
      - in: body
        name: configuration
        description: "Voting period configuration"
        schema:
          $ref: '#/definitions/VotingPeriodConfigurationSubmissionVo'
        required: true
      responses:
        '200':
          description: OK
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found

  '/operation/{clientId}/voting-period/status':
    get:
      tags:
      - VotingPeriods
      summary: Returns information about the state of a voting period configuration
      produces:
      - application/json
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation
        required: true
        type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/VotingPeriodStatusVo'
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found

  '/operation/{clientId}/tally-archive/status':
    get:
      tags:
      - TallyArchive
      summary: Returns the state of the tally archive generation
      produces:
      - application/json
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation
        required: true
        type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/TallyArchiveStatusVo'
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/operation/{clientId}/tally-archive/demand':
    put:
      tags:
      - TallyArchive
      summary: Demands results (tally archive) generation at the end of an operation
      description: Triggers the tally archive generation process. The operation end  date is verified to ensure it is over.
      produces:
      - application/json
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation
        required: true
        type: string
      - name: restart
        in: query
        description: restart the generation task
        required: true
        type: boolean
      responses:
        200:
          description: OK - demand being processed
        201:
          description: Created - demand created
        403:
          description: Forbidden - if the operation has not ended yet
        404:
          description: Not Found - if the operation doesn't exist or is not deployed
  '/operation/{clientId}/tally-archive/early-demand':
    put:
      tags:
      - Tally archive
      summary: Demands early results (tally archive) generation
      description: Triggers the tally archive generation process, even if the operation has not ended yet
      produces:
      - application/json
      parameters:
      - name: clientId
        in: path
        description: client ID of the operation
        required: true
        type: string
      - name: restart
        in: query
        description: restart the generation task
        required: true
        type: boolean
      responses:
        200:
          description: OK - demand being processed
        201:
          description: Created - demand created
        403:
          description: Forbidden - if the environment is not SIMULATION
        404:
          description: Not Found - if the operation doesn't exist or is not deployed

  '/protocol/{protocolId}/voters':
    get:
      tags:
      - ProtocolInstance
      summary: Retrieves a list of voters for the given protocol id
      produces:
      - application/json
      parameters:
      - name: protocolId
        in: path
        description: the protocol ID
        required: true
        type: string
      responses:
        '200':
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/VoterVo'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/protocol/{protocolId}/operation':
    get:
      tags:
      - ProtocolInstance
      summary: Retrieves the operation metadata
      produces:
      - application/json
      parameters:
      - name: protocolId
        in: path
        description: the protocol ID
        required: true
        type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/OperationVo'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/protocol/{protocolId}/voting-site-configuration':
    get:
      tags:
      - ProtocolInstance
      summary: Retrieves the voting site configuration
      produces:
      - application/json
      parameters:
      - name: protocolId
        in: path
        description: the protocol ID
        required: true
        type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/VotingSiteConfigurationVo'
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found

  '/protocol/{protocolId}/attachment/{attachmentId}':
    get:
      tags:
      - ProtocolInstance
      summary: Retrieves an attachment
      produces:
      - application/zip
      parameters:
      - name: protocolId
        in: path
        description: the protocol identifier
        required: true
        type: string
      - name: attachmentId
        in: path
        description: the attachment identifier
        required: true
        type: string
      responses:
        '200':
          description: OK
          schema:
            type: file
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found

securityDefinitions:

  Custom Header:
    type: apiKey
    name: X-Requested-With
    in: header

definitions:

  OperationConfigurationSubmissionVo:
    type: object
    required:
    - user
    - clientId
    - operationName
    - operationDate
    - gracePeriod
    - testSitePrinter
    - attachments
    - milestones
    - canton
    - electionSiteConfigurations
    - highlightedQuestions
    - ballotDocumentations
    properties:
      user:
        type: string
        description: login of the user that pushed the configuration from the client application (backoffice)
      clientId:
        type: string
        description: id of the operation provided by the client application (backoffice)
      operationName:
        type: string
        description: short label of the operation
      operationDate:
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
        description: date of the operation
      gracePeriod:
        type: integer
        description: grace period in minutes
      testSitePrinter:
        description: printer used to decrypt the test site voting materials
        type: object
        properties:
          municipality:
            $ref: '#/definitions/MunicipalityVo'
          printer:
            $ref: '#/definitions/PrinterConfigurationVo'
      attachments:
        description: catalog of the file attachments. Registers uploaded here are only used for generating the test site voting materials.
        type: array
        items:
          $ref: '#/definitions/AttachmentFileEntryVo'
      canton:
        type: string
        format: '[A-Z]{2}'
        description: Two letters code of the canton of the operation
      milestones:
        description: map of milestone to local date time
        type: object
        additionalProperties:
          type: object
          properties:
            milestone:
              type: string
              enum:
              - SITE_VALIDATION
              - CERTIFICATION
              - PRINTER_FILES
              - BALLOT_BOX_INIT
              - SITE_OPEN
              - SITE_CLOSE
              - BALLOT_BOX_DECRYPT
              - RESULT_VALIDATION
              - DATA_DESTRUCTION
            datetime:
              type: string
              format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
      electionSiteConfigurations:
        description: Election site configurations by ballot.
        type: array
        items:
          $ref: '#/definitions/ElectionSiteConfigurationVo'
      highlightedQuestions:
        description: Questions to highlight on the web site.
        type: array
        items:
          $ref: '#/definitions/HighlightedQuestionsVo'
      ballotDocumentations:
        description: Documentations relative to ballot
        type: array
        items:
          $ref: '#/definitions/BallotDocumentationVo'

  ElectionSiteConfigurationVo:
    type: object
    required:
    - ballot
    - displayCandidateSearchForm
    properties:
      ballot:
        description: Related Ballot
        type: string
      displayCandidateSearchForm:
        description: Should display candidate search form on the voting site
        type: boolean
      allowChangeOfElectoralList:
        description: Should allow change of electoral list on the voting site
        type: boolean
      displayEmptyPosition:
        description: Should display empty position on the voting site
        type: boolean
      displayCandidatePositionOnACompactBallotPaper:
        description: Should display candidate position on a compact ballot paper
        type: boolean
      displayCandidatePositionOnAModifiedBallotPaper:
        description: Should display candidate position on a modified ballot paper
        type: boolean
      displaySuffrageCount:
        description: Should display suffrage count
        type: boolean
      displayListVerificationCode:
        description: Should display verification code on election list
        type: boolean
      allowOpenCandidature:
        description: Should allow open candidature
        type: boolean
      allowMultipleMandates:
        description: Should allow multiple mandates
        type: boolean
      displayVoidOnEmptyBallotPaper:
        description: Should display void ballot paper when there is no candidate or no name of the party
        type: boolean
      candidateInformationDisplayModel:
        description: Name of the display model used to display candidate information
        type: string
      displayedColumnsOnVerificationTable:
        description: Columns to be displayed on the verification table
        type: array
        items:
          type: string
      columnsOrderOnVerificationTable:
        description: Columns sort order on the verification table
        type: array
        items:
          type: string

  HighlightedQuestionsVo:
    type: object
    required:
    - answerFile
    - question
    properties:
      answerFile:
        $ref: '#/definitions/AttachmentFileEntryVo'
      question:
        description: Localized question
        type: string

  BallotDocumentationVo:
    type: object
    required:
    - documentation
    - label
    - ballot
    properties:
      documentation:
        $ref: '#/definitions/AttachmentFileEntryVo'
      label:
        description: Localized label
        type: string
      ballot:
        description: Related Ballot
        type: string

  AttachmentFileEntryVo:
    type: object
    required:
    - zipFileName
    - importDateTime
    - attachmentType
    properties:
      zipFileName:
        description: name of the zip file
        type: string
      importDateTime:
        description: date and time when the file has been imported into the backoffice
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
      attachmentType:
        type: string
        enum:
        - REPOSITORY_VOTATION
        - REPOSITORY_ELECTION
        - DOMAIN_OF_INFLUENCE
        - DOCUMENT_FAQ
        - DOCUMENT_TERMS
        - DOCUMENT_CERTIFICATE
        - REGISTER
        - BALLOT_DOCUMENTATION
        - TRANSLATIONS
        - HIGHLIGHTED_QUESTION
        - ELECTION_OFFICER_KEY

  OperationConfigurationsStatusVo:
    type: object
    properties:
      inTest:
        $ref: '#/definitions/InTestConfigurationStatusVo'
      deployed:
        $ref: '#/definitions/DeployedConfigurationStatusVo'

  InTestConfigurationStatusVo:
    type: "object"
    required:
    - state
    - version
    - lastChangeUser
    - lastChangeDate
    - configurationPageUrl
    - votingCardsLocation
    - protocolProgress
    properties:
      state:
        type: "string"
        enum:
        - TEST_SITE_IN_DEPLOYMENT
        - IN_ERROR
        - IN_VALIDATION
        - VALIDATED
        - INVALIDATED
        - DEPLOYMENT_REQUESTED
        - DEPLOYMENT_REFUSED
        - DEPLOYED
      comment:
        type: string
      version:
        type: integer
        format: int32
      lastChangeUser:
        type: string
      lastChangeDate:
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
      configurationPageUrl:
        type: string
        description: PACT frontend URL of the deployment page for the current configuration, relative to the PACT application context URL
        example: "configuration-deployments/9202"
      votingCardsLocation:
        type: string
        description: Path to the voting cards storage in the shared volume
      protocolProgress:
        type: array
        items:
          $ref: '#/definitions/ProtocolStageProgressVo'

  DeployedConfigurationStatusVo:
    type: object
    required:
    - version
    - lastChangeUser
    - lastChangeDate
    properties:
      version:
        type: integer
        format: int32
      user:
        type: string
      lastChangeDate:
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'

  UserVo:
    type: string

  VotingMaterialsConfigurationSubmissionVo:
    type: object
    required:
    - user
    - votingCardLabel
    - target
    - registerFilesCatalog
    - municipalities
    - virtualMunicipalities
    - printers
    - municipalitiesToPrintersLinks
    - swissAbroadWithoutMunicipalityPrinter
    properties:
      user:
        type: string
      votingCardLabel:
        type: string
      target:
        type: string
        description: Whether this configuration is for a simulation or a real site
        enum:
        - SIMULATION
        - REAL
      simulationName:
        type: string
        description: The name of the simulation if the target of this configuration is a simulation.
      registerFilesCatalog:
        type: array
        items:
          $ref: '#/definitions/RegisterFileEntryVo'
      municipalities:
        type: array
        items:
          $ref: '#/definitions/MunicipalityVo'
      virtualMunicipalities:
        type: array
        items:
          $ref: '#/definitions/MunicipalityVo'
      printers:
        type: array
        items:
          $ref: '#/definitions/PrinterConfigurationVo'
      municipalitiesToPrintersLinks:
        type: array
        items:
          $ref: '#/definitions/MunicipalityToPrinterLinkVo'
      swissAbroadWithoutMunicipalityPrinter:
        $ref: '#/definitions/PrinterConfigurationVo'

  VotingMaterialsStatusVo:
    type: object
    required:
    - state
    - lastChangeUser
    - lastChangeDate
    - configurationPageUrl
    - invalidationPageUrl
    - votingCardLocations
    - protocolProgress
    properties:
      state:
        type: string
        enum:
        - AVAILABLE_FOR_CREATION
        - CREATION_REQUESTED
        - CREATION_REJECTED
        - CREATION_IN_PROGRESS
        - CREATION_FAILED
        - CREATED
        - INVALIDATION_REQUESTED
        - INVALIDATION_REJECTED
        - INVALIDATED
      lastChangeUser:
        type: string
      lastChangeDate:
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
      rejectionReason:
        type: string
      configurationPageUrl:
        type: string
      invalidationPageUrl:
        type: string
      votingCardLocations:
        description: Map of printer id to voting card locaiton
        type: object
        additionalProperties:
          type: object
          properties:
            printerId:
              type: string
            votingCardLocation:
              type: string
              description: Path to the voting cards storage in the shared volume
      protocolProgress:
        type: array
        items:
          $ref: '#/definitions/ProtocolStageProgressVo'

  ProtocolStageProgressVo:
    type: object
    required:
    - stage
    - ccIndex
    - ratio
    properties:
      stage:
        type: string
        enum:
        - SEND_PUBLIC_PARAMETERS
        - PUBLISH_PUBLIC_KEY_PARTS
        - STORE_PUBLIC_KEY_PARTS
        - SEND_ELECTION_SET
        - SEND_VOTERS
        - PUBLISH_PRIMES
        - BUILD_PUBLIC_CREDENTIALS
        - REQUEST_PRIVATE_CREDENTIALS
        - SEND_ELECTION_OFFICE_KEY
        - INITIALIZE_CONTROL_COMPONENTS
        - PUBLISH_SHUFFLE
        - PUBLISH_PARTIAL_DECRYPTION
      ccIndex:
        type: integer
        description: The control component index or -1 if this stage does not keep track of individual control
      ratio:
        type: number
        description: The completition ratio of this stage
      startDate:
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
      lastModificationDate:
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'

  RegisterFileEntryVo:
    type: object
    required:
    - zipFileName
    - importDateTime
    properties:
      zipFileName:
        type: string
      importDateTime:
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'

  MunicipalityVo:
    type: object
    required:
    - ofsId
    - name
    properties:
      ofsId:
        type: integer
        minimum: 1
        maximum: 9999
      name:
        type: string

  PrinterConfigurationVo:
    type: object
    required:
    - id
    - name
    - publicKey
    properties:
      id:
        type: string
        description: The printer's identifier
      name:
        type: string
        description: The printer's name
      publicKey:
        type: string
        format: base64
        description: The serialized public key

  MunicipalityToPrinterLinkVo:
    type: object
    required:
    - municipalityOfsId
    - printerId
    properties:
      municipalityOfsId:
        type: integer
        minimum: 1
        maximum: 9999
      printerId:
        type: string

  VotersCreationStatisticsVo:
    type: object
    required:
    - printerName
    - countingCircleId
    - numberOfVoters
    properties:
      printerName:
        type: string
      countingCircleId:
        type: string
      countingCircleName:
        type: string
      numberOfVoters:
        type: integer
        format: int64
        minimum: 0


  VotingPeriodConfigurationSubmissionVo:
    type: object
    required:
    - user
    - electoralAuthorityKey
    - target
    - simulationName
    - siteOpeningDate
    - siteClosingDate
    - gracePeriod
    properties:
      user:
        type: string
      electoralAuthorityKey:
        type: string
      target:
        type: string
        description: Whether this configuration is for a simulation or a real site
        enum:
        - SIMULATION
        - REAL
      simulationName:
        type: string
        description: The name of the simulation if the target of this configuration is a simulation.
      siteOpeningDate:
        type: string
        description: override SITE_OPEN milestone if the target of this configuration is a simulation.
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
      siteClosingDate:
        type: string
        description: override SITE_CLOSE milestone if the target of this configuration is a simulation.
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
      gracePeriod:
        type: integer
        description: override grace period if the target of this configuration is a simulation.

  VotingPeriodStatusVo:
    type: object
    required:
    - state
    - lastChangeUser
    - lastChangeDate
    - configurationPageUrl
    - rejectionReason
    properties:
      state:
        type: string
        enum:
        - AVAILABLE_FOR_INITIALIZATION
        - INITIALIZATION_REQUESTED
        - INITIALIZATION_REJECTED
        - INITIALIZATION_IN_PROGRESS
        - INITIALIZATION_FAILED
        - INITIALIZED
        - CLOSED
      lastChangeUser:
        type: string
      lastChangeDate:
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
      configurationPageUrl:
        type: string
        description: PACT frontend URL of the deployment page for the current configuration, relative to the PACT application context URL
      rejectionReason:
        type: string

  TallyArchiveStatusVo:
    type: object
    required:
    - state
    - tallyArchiveLocation
    properties:
      state:
        type: string
        enum:
        - NOT_REQUESTED
        - NOT_ENOUGH_VOTES_CAST
        - CREATION_IN_PROGRESS
        - CREATION_FAILED
        - CREATED
      tallyArchiveLocation:
        type: string
        description: Path to the tally archive in the shared volume

  OperationVo:
    type: object
    required:
    - shortLabel
    - longLabel
    - canton
    - votingCardLabel
    - type
    - simulationName
    - defaultLang
    - votingPeriod
    - operationDate
    - votationRepositoryIds
    - electionRepositoryIds
    properties:
      shortLabel:
        type: string
        description: the short label of the operation
      longLabel:
        type: string
        description: the long label of the operation
      canton:
        type: string
        description: 2 letters code of the canton
      votingCardLabel:
        type: string
        description: the label that has been printed on the voting card.
      type:
        type: string
        enum:
        - TEST
        - SIMULATION
        - REAL
      simulationName:
        type: string
        description: The name of the simulation if the target of this configuration is a simulation.
      defaultLang:
        type: string
        enum:
        - FR
        - IT
        - DE
        - RM
      votingPeriod:
        $ref: '#/definitions/VotingPeriodVo'
      operationDate:
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
        description: the operation date
      votationRepositoryIds:
        type: array
        description: a list with all the votation repository identifiers belonging to this operation.
        items:
          type: integer
          description: the attchment identifier of a votation repository.
      electionRepositoryIds:
        type: array
        description: a list with all the election repository identifiers belonging to this operation.
        items:
          type: integer
            description: the attchment identifier of an election repository.

  VotingPeriodVo:
    type: object
    required:
    - openingDate
    - closingDate
    - gracePeriod
    properties:
      openingDate:
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
        description: the voting period opening date
      closingDate:
        type: string
        format: '^JSDate\[(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})\]$'
        description: the voting period closing date
      gracePeriod:
        type: integer
        description: the grace period in minutes

  DomainOfInfluenceVo:
    type: object
    required:
    - id
    - name
    properties:
      id:
        type: string
        description: the eCH-0155 localDomainOfInfluencetId
      name:
        type: string
        description: the eCH-0155 domainOfInfluenceName

  CountingCircle:
    type: object
    required:
    - id
    - name
    properties:
      id:
        type: string
        description: the eCH-0155 countingCircleId
      name:
        type: string
        description: the eCH-0155 countingCircleName

  VoterVo:
    type: object
    required:
    - domainOfInfluences
    - localPersonId
    - voterIndex
    - voterBirthYear
    - identificationCodeHash
    - countingCircle
    properties:
      domainOfInfluences:
        type: array
        items:
          $ref: '#/definitions/DomainOfInfluenceVo'
      localPersonId:
        type: string
        description: the eCH-0044 localPersonId
      voterIndex:
        type: string
        description: the protocol voter id
      voterBirthYear:
        type: integer
        description: the voter birth year
      voterBirthMonth:
        type: integer
        description: the voter birth month
      voterBirthDay:
        type: integer
        description: the voter birth day
      identificationCodeHash:
        type: string
        description: a hash of the identification code on the voting card
      countingCircle:
        $ref: '#/definitions/CountingCircle'

  VotingSiteConfigurationVo:
    type: object
